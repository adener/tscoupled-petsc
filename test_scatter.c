#include <unistd.h>
#include "fvns3d.h"

int main(int argc,char **argv) {

  DMDALocalInfo atmosInfo, oceanInfo;
  ProblemDefs *atmosDefs, *oceanDefs;
  FVNS *atmos, *ocean;
  Vec atmosQQ, oceanQQ, atmosWork, oceanWork;
  VecScatter atmosToOcean, oceanToAtmos;
  PetscScalar before, after, atmosError, oceanError;
  PetscInt nloc, nglob, i, j;
  PetscMPIInt rank;
  Solution **ocArr, **atArr;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PetscInitialize(&argc,&argv,0,0); if (ierr) return ierr;
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD, &rank);CHKERRQ(ierr);

  ierr = ProblemDefsCreate("ocean",NULL, PTYPE_WDF, UP, &oceanDefs);CHKERRQ(ierr);
  ierr = FVNSCreate(PETSC_COMM_WORLD, oceanDefs, &ocean);CHKERRQ(ierr);
  ierr = FVNSSetNumElements(ocean,32,32,32);CHKERRQ(ierr);
  ierr = FVNSSetFromOptions(ocean);CHKERRQ(ierr);
  ierr = FVNSSetUp(ocean);
  ierr = FVNSView(ocean, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);

  ierr = ProblemDefsCreate("atmos",NULL, PTYPE_WDF, DOWN, &atmosDefs);CHKERRQ(ierr);
  ierr = FVNSCreate(PETSC_COMM_WORLD,atmosDefs, &atmos);CHKERRQ(ierr);
  ierr = FVNSSetNumElements(atmos,32,32,32);CHKERRQ(ierr);
  ierr = FVNSSetFromOptions(atmos);CHKERRQ(ierr);
  ierr = FVNSSetUp(atmos);
  ierr = FVNSView(atmos, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);

  ierr = VecScatterCreate(atmos->couplingOwn, ocean->couplingIS,
                          ocean->couplingOther, ocean->couplingIS,
                          &atmosToOcean);CHKERRQ(ierr);
  ierr = VecScatterCreate(ocean->couplingOwn, atmos->couplingIS,
                          atmos->couplingOther, atmos->couplingIS,
                          &oceanToAtmos);CHKERRQ(ierr);

  ierr = VecZeroEntries(atmos->couplingOwn);CHKERRQ(ierr);
  ierr = VecGetSize(atmos->couplingOwn, &nglob);CHKERRQ(ierr);
  ierr = VecGetLocalSize(atmos->couplingOwn, &nloc);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_SELF, "[%d] size of atmos->couplingOwn: n=%d  N=%d\n", rank, nloc, nglob);CHKERRQ(ierr);
  
  ierr = DMCreateGlobalVector(atmos->dm, &atmosQQ);CHKERRQ(ierr);
  ierr = VecSetRandom(atmosQQ, NULL);CHKERRQ(ierr);
  ierr = FVNSExtractCouplingVars(atmos, atmosQQ);CHKERRQ(ierr);
  ierr = VecNorm(atmos->couplingOwn, NORM_2, &before);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "atmos->couplingOwn : norm = %.3e\n", before);
  
  ierr = VecZeroEntries(ocean->couplingOther);CHKERRQ(ierr);
  ierr = VecGetSize(ocean->couplingOther, &nglob);CHKERRQ(ierr);
  ierr = VecGetLocalSize(ocean->couplingOther, &nloc);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_SELF, "[%d] size of ocean->couplingOther: n=%d  N=%d\n", rank, nloc, nglob);CHKERRQ(ierr);
  
  ierr = VecScatterBegin(atmosToOcean, atmos->couplingOwn, ocean->couplingOther, INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
  ierr = VecScatterEnd(atmosToOcean, atmos->couplingOwn, ocean->couplingOther, INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
  ierr = VecNorm(ocean->couplingOther, NORM_2, &after);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "ocean->couplingOther : norm = %0.2e\n", after);CHKERRQ(ierr);
  
  ierr = VecZeroEntries(ocean->couplingOwn);CHKERRQ(ierr);
  ierr = VecGetSize(ocean->couplingOwn, &nglob);CHKERRQ(ierr);
  ierr = VecGetLocalSize(ocean->couplingOwn, &nloc);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_SELF, "[%d] size of ocean->couplingOwn: n=%d  N=%d\n", rank, nloc, nglob);CHKERRQ(ierr);
  
  ierr = DMCreateGlobalVector(ocean->dm, &oceanQQ);CHKERRQ(ierr);
  ierr = VecSetRandom(oceanQQ, NULL);CHKERRQ(ierr);
  ierr = FVNSExtractCouplingVars(ocean, oceanQQ);CHKERRQ(ierr);
  ierr = VecNorm(ocean->couplingOwn, NORM_2, &before);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "ocean->couplingOwn : norm = %.3e\n", before);
  
  ierr = VecZeroEntries(atmos->couplingOther);CHKERRQ(ierr);
  ierr = VecGetSize(atmos->couplingOther, &nglob);CHKERRQ(ierr);
  ierr = VecGetLocalSize(atmos->couplingOther, &nloc);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_SELF, "[%d] size of atmos->couplingOther: n=%d  N=%d\n", rank, nloc, nglob);CHKERRQ(ierr);
  
  ierr = VecScatterBegin(oceanToAtmos, ocean->couplingOwn, atmos->couplingOther, INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
  ierr = VecScatterEnd(oceanToAtmos, ocean->couplingOwn, atmos->couplingOther, INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
  ierr = VecNorm(atmos->couplingOther, NORM_2, &after);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "atmos->couplingOther : norm = %0.2e\n", after);CHKERRQ(ierr);

  ierr = VecDuplicate(atmos->couplingOwn, &atmosWork);CHKERRQ(ierr);
  ierr = VecDuplicate(ocean->couplingOwn, &oceanWork);CHKERRQ(ierr);
  ierr = VecWAXPY(atmosWork, -1.0, atmos->couplingOther, atmos->couplingOwn);CHKERRQ(ierr);
  ierr = VecWAXPY(oceanWork, -1.0, ocean->couplingOwn, ocean->couplingOther);CHKERRQ(ierr);
  ierr = VecNorm(atmosWork, NORM_2, &atmosError);CHKERRQ(ierr);
  ierr = VecNorm(oceanWork, NORM_2, &oceanError);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "communication error = %0.3e\n", PetscAbsReal(oceanError - atmosError));CHKERRQ(ierr);

  ierr = DMDAGetLocalInfo(ocean->dm, &oceanInfo);CHKERRQ(ierr);
  ierr = VecGetArray2d(ocean->couplingOther, ocean->jm, ocean->im*oceanInfo.dof, ocean->js, ocean->is*oceanInfo.dof, (PetscScalar***)&ocArr);CHKERRQ(ierr);
  if (ocean->defs->couplingSide == UP && oceanInfo.zs+oceanInfo.zm == oceanInfo.mz) {
    for (j=oceanInfo.ys; j<oceanInfo.ys+oceanInfo.ym; j++) {
      for (i=oceanInfo.xs; i<oceanInfo.xs+oceanInfo.xm; i++) {
        SolutionCheck(PETSC_FALSE, &ocArr[j][i]);
      }
    }
  }
  ierr = VecRestoreArray2d(ocean->couplingOther, ocean->jm, ocean->im*oceanInfo.dof, ocean->js, ocean->is*oceanInfo.dof, (PetscScalar***)&ocArr);CHKERRQ(ierr);

  ierr = DMDAGetLocalInfo(atmos->dm, &atmosInfo);CHKERRQ(ierr);
  ierr = VecGetArray2d(atmos->couplingOther, atmos->jm, atmos->im*atmosInfo.dof, atmos->js, atmos->is*atmosInfo.dof, (PetscScalar***)&atArr);CHKERRQ(ierr);
  if (atmos->defs->couplingSide == DOWN && atmosInfo.zs == 0) {
    for (j=atmosInfo.ys; j<atmosInfo.ys+atmosInfo.ym; j++) {
      for (i=atmosInfo.xs; i<atmosInfo.xs+atmosInfo.xm; i++) {
        SolutionCheck(PETSC_FALSE, &atArr[j][i]);
      }
    }
  }
  ierr = VecRestoreArray2d(atmos->couplingOther, atmos->jm, atmos->im*atmosInfo.dof, atmos->js, atmos->is*atmosInfo.dof, (PetscScalar***)&atArr);CHKERRQ(ierr);

  ierr = VecDestroy(&oceanQQ);CHKERRQ(ierr);
  ierr = VecDestroy(&oceanWork);CHKERRQ(ierr);
  ierr = VecDestroy(&atmosQQ);CHKERRQ(ierr);
  ierr = VecDestroy(&atmosWork);CHKERRQ(ierr);
  ierr = FVNSDestroy(&atmos);CHKERRQ(ierr);
  ierr = FVNSDestroy(&ocean);CHKERRQ(ierr);
  ierr = VecScatterDestroy(&atmosToOcean);CHKERRQ(ierr);
  ierr = VecScatterDestroy(&oceanToAtmos);CHKERRQ(ierr);
  
  ierr = PetscFinalize();
  PetscFunctionReturn(0);
}