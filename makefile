SRCDIR  = ./src
OBJDIR  = ./obj
INCDIR  = ./inc

include ${PETSC_DIR}/lib/petsc/conf/variables
include ${PETSC_DIR}/lib/petsc/conf/rules

SOURCES = $(wildcard $(SRCDIR)/*.c)
HEADERS = $(wildcard $(INCDIR)/*.h)
OBJECTS = $(patsubst $(SRCDIR)/%,$(OBJDIR)/%,$(SOURCES:.c=.o))
TARGETS = test_CNS3D test_coupledCNS3D test_scatter test_coupled_buffer
OUTPUTDIR = out out/3D out/3D/TGV out/3D/LDC out/3D/WDF out/3D/WDF_MULTI out/3D/STANDSTILL/ out/3D/TB/ out/3D/HS/

#EXTRAFLAGS = -DADD_RHS_EVENTS

all : $(TARGETS)

%.o : %.c $(HEADERS)
	@${MKDIR} -p $(OBJDIR)
	${PCC} -o $@ -c ${PCC_FLAGS} ${PFLAGS} ${CCPPFLAGS} -I$(INCDIR) ${EXTRAFLAGS} $<

$(OBJDIR)/%.o : $(SRCDIR)/%.c $(HEADERS)
	@${MKDIR} -p $(OBJDIR)
	${PCC} -o $@ -c ${PCC_FLAGS} ${PFLAGS} ${CCPPFLAGS} -I$(INCDIR) $<

$(TARGETS) : % : $(OBJECTS) %.o
	${CLINKER} -o $@ $^ $(PETSC_LIB)
	@${MKDIR} -p $(OUTPUTDIR)

clean ::
	@${RM} -r $(OBJDIR)
	@${RM} $(TARGETS)
	@${RM} *.o

.PHONY: cleanall
cleanall : clean
	@${RM} -r out
