#include "fvns3d.h"
#include <petscts.h>

#if defined(ADD_RHS_EVENTS)
PetscLogEvent FAST_RHS,SLOW_RHS,SLOWBUFFER_RHS, SLOWBUFFER_RHS_BOUNDARY_COMM, SLOWBUFFER_RHS_COMPUTE;
#endif

typedef struct {
  MPI_Comm          comm;
  FVNS*             atmos;
  FVNS*             ocean;
  DM                pack;
  VecScatter        atmosToOcean;
  VecScatter        oceanToAtmos;
  PetscViewer       viewer1,viewer2; 
} CoupledCtx;

static PetscErrorCode InitializeCoupledProblem(MPI_Comm comm, PetscInt nxe, PetscInt nye, PetscInt atmosNz, PetscInt oceanNz, 
            PetscReal dx, PetscReal dy, PetscReal atmosdz, PetscReal oceandz,CoupledCtx** coupled)
{ 
  CoupledCtx *newctx;
  ProblemDefs *atmosDefs, *oceanDefs;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  /* crate the coupled context*/
  ierr = PetscNew(&newctx);CHKERRQ(ierr);
  newctx->comm = comm;

  /* initialize the PDEs for each subdomain */
  ierr = ProblemDefsCreate("atmos","out/3D/WDF_MULTI", PTYPE_WDF, DOWN, &atmosDefs);CHKERRQ(ierr);
  ierr = FVNSCreate(newctx->comm, atmosDefs, &newctx->atmos);CHKERRQ(ierr);
  ierr = FVNSSetNumElements(newctx->atmos,nxe,nye,atmosNz);CHKERRQ(ierr);
  ierr = FVNSSetMeshSize(newctx->atmos,dx,dy,atmosdz);CHKERRQ(ierr);
  ierr = FVNSSetFromOptions(newctx->atmos);CHKERRQ(ierr);
  ierr = FVNSSetUp(newctx->atmos);CHKERRQ(ierr);
  ierr = FVNSOptionsView(newctx->atmos);CHKERRQ(ierr);
  ierr = ProblemDefsCreate("ocean","out/3D/WDF_MULTI", PTYPE_WDF, UP, &oceanDefs);CHKERRQ(ierr);
  ierr = FVNSCreate(newctx->comm, oceanDefs, &newctx->ocean);CHKERRQ(ierr);
  ierr = FVNSSetNumElements(newctx->ocean,nxe,nye,oceanNz);CHKERRQ(ierr);
  ierr = FVNSSetMeshSize(newctx->ocean,dx,dy,oceandz);CHKERRQ(ierr);
  ierr = FVNSSetFromOptions(newctx->ocean);CHKERRQ(ierr);
  ierr = FVNSSetUp(newctx->ocean);CHKERRQ(ierr);
  ierr = FVNSOptionsView(newctx->ocean);CHKERRQ(ierr);
  ierr = PetscViewerFlush(PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);

  ierr = FVNSViewExnerPressureProfile(newctx->ocean);CHKERRQ(ierr);
  ierr = FVNSViewExnerPressureProfile(newctx->atmos);CHKERRQ(ierr);

  /* create the composite packer */
  ierr = DMCompositeCreate(newctx->comm, &newctx->pack);CHKERRQ(ierr);
  ierr = DMSetOptionsPrefix(newctx->pack, "coupled_");CHKERRQ(ierr);
  ierr = DMCompositeAddDM(newctx->pack, newctx->atmos->dm);CHKERRQ(ierr);
  ierr = DMCompositeAddDM(newctx->pack, newctx->ocean->dm);CHKERRQ(ierr);
  ierr = DMSetFromOptions(newctx->pack);CHKERRQ(ierr);
  ierr = DMSetUp(newctx->pack);CHKERRQ(ierr);

  /* create scatters to communicate boundaries */
  ierr = VecScatterCreate(newctx->atmos->couplingOwn, newctx->ocean->couplingIS,
                          newctx->ocean->couplingOther, newctx->ocean->couplingIS,
                          &newctx->atmosToOcean);
  ierr = VecScatterCreate(newctx->ocean->couplingOwn, newctx->atmos->couplingIS,
                          newctx->atmos->couplingOther, newctx->atmos->couplingIS,
                          &newctx->oceanToAtmos);

  *coupled = newctx;
  PetscFunctionReturn(0);
}

static PetscErrorCode DestroyCoupledProblem(CoupledCtx** coupled)
{
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = FVNSDestroy(&(*coupled)->atmos);CHKERRQ(ierr);
  ierr = FVNSDestroy(&(*coupled)->ocean);CHKERRQ(ierr);
  ierr = DMDestroy(&((*coupled)->pack));CHKERRQ(ierr);
  ierr = VecScatterDestroy(&(*coupled)->atmosToOcean);CHKERRQ(ierr);
  ierr = VecScatterDestroy(&(*coupled)->oceanToAtmos);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&((*coupled)->viewer1));CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&((*coupled)->viewer2));CHKERRQ(ierr);
  ierr = PetscFree(*coupled);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode CommunicateCouplingBoundary(
  CoupledCtx* cpl, Vec atmosQQ, Vec oceanQQ)
{
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = FVNSExtractCouplingVars(cpl->atmos, atmosQQ);CHKERRQ(ierr);
  ierr = FVNSExtractCouplingVars(cpl->ocean, oceanQQ);CHKERRQ(ierr);
  ierr = VecScatterBegin(cpl->atmosToOcean, cpl->atmos->couplingOwn, cpl->ocean->couplingOther, INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
  ierr = VecScatterBegin(cpl->oceanToAtmos, cpl->ocean->couplingOwn, cpl->atmos->couplingOther, INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
  ierr = VecScatterEnd(cpl->atmosToOcean, cpl->atmos->couplingOwn, cpl->ocean->couplingOther, INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
  ierr = VecScatterEnd(cpl->oceanToAtmos, cpl->ocean->couplingOwn, cpl->atmos->couplingOther, INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/* gets coupling data for the ocean */ 
static PetscErrorCode CommunicateCouplingBoundary_Ocean(
  CoupledCtx* cpl, Vec atmosQQ, Vec oceanQQ)
{
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = FVNSExtractCouplingVars(cpl->atmos, atmosQQ);CHKERRQ(ierr);
  ierr = VecScatterBegin(cpl->atmosToOcean, cpl->atmos->couplingOwn, cpl->ocean->couplingOther, INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
  ierr = VecScatterEnd(cpl->atmosToOcean, cpl->atmos->couplingOwn, cpl->ocean->couplingOther, INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/* gets coupling data for the atmosphere */
static PetscErrorCode CommunicateCouplingBoundary_Atmos(
  CoupledCtx* cpl, Vec atmosQQ, Vec oceanQQ)
{
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = FVNSExtractCouplingVars(cpl->ocean, oceanQQ);CHKERRQ(ierr);
  ierr = VecScatterBegin(cpl->oceanToAtmos, cpl->ocean->couplingOwn, cpl->atmos->couplingOther, INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
  ierr = VecScatterEnd(cpl->oceanToAtmos, cpl->ocean->couplingOwn, cpl->atmos->couplingOther, INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


static PetscErrorCode FormCoupledRHS(TS ts, PetscReal t, Vec QQ, Vec LRHSQ, void *ctx)
{
  CoupledCtx* coupled = (CoupledCtx*)ctx;
  Vec atmosQQ, oceanQQ, atmosRHS, oceanRHS;
  PetscInt lock;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = VecLockGet(QQ, &lock);CHKERRQ(ierr);
  if (lock != 0) {
    ierr = VecLockReadPop(QQ);CHKERRQ(ierr);
  }
  ierr = DMCompositeGetAccess(coupled->pack, QQ, &atmosQQ, &oceanQQ);CHKERRQ(ierr);
  ierr = DMCompositeGetAccess(coupled->pack, LRHSQ, &atmosRHS, &oceanRHS);CHKERRQ(ierr);

  ierr = CommunicateCouplingBoundary(coupled, atmosQQ, oceanQQ);CHKERRQ(ierr);
  ierr = FVNSComputeRHS(coupled->atmos, atmosQQ, atmosRHS);CHKERRQ(ierr);
  ierr = FVNSComputeRHS(coupled->ocean, oceanQQ, oceanRHS);CHKERRQ(ierr);

  ierr = DMCompositeRestoreAccess(coupled->pack, QQ, &atmosQQ, &oceanQQ);CHKERRQ(ierr);
  ierr = DMCompositeRestoreAccess(coupled->pack, LRHSQ, &atmosRHS, &oceanRHS);CHKERRQ(ierr);
  if (lock != 0) {
    ierr = VecLockReadPush(QQ);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

/* QQ is full vector, oceanRHS is the subvector corresponding to the top of the ocean dm zsplit */
static PetscErrorCode FormCoupledRHS_Split_Ocean_Buffer(TS ts, PetscReal t, Vec QQ, Vec oceanRHS, void *ctx)
{
  CoupledCtx* coupled = (CoupledCtx*)ctx;
  Vec atmosQQ, oceanQQ;
  PetscInt lock;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
#if defined(ADD_RHS_EVENTS)
  PetscLogEventBegin(SLOWBUFFER_RHS,0,0,0,0);
#endif
  ierr = VecLockGet(QQ, &lock);CHKERRQ(ierr);
  if (lock != 0) {
    ierr = VecLockReadPop(QQ);CHKERRQ(ierr);
  }
  ierr = DMCompositeGetAccess(coupled->pack, QQ, &atmosQQ, &oceanQQ);CHKERRQ(ierr);
#if defined(ADD_RHS_EVENTS)
  PetscLogEventBegin(SLOWBUFFER_RHS_BOUNDARY_COMM,0,0,0,0);
#endif
  ierr = CommunicateCouplingBoundary_Ocean(coupled, atmosQQ, oceanQQ);CHKERRQ(ierr);
#if defined(ADD_RHS_EVENTS)
  PetscLogEventEnd(SLOWBUFFER_RHS_BOUNDARY_COMM,0,0,0,0);
  PetscLogEventBegin(SLOWBUFFER_RHS_COMPUTE,0,0,0,0);
#endif
  ierr = FVNSComputeRHS_TOP(coupled->ocean, oceanQQ, oceanRHS);CHKERRQ(ierr);
#if defined(ADD_RHS_EVENTS)
  PetscLogEventEnd(SLOWBUFFER_RHS_COMPUTE,0,0,0,0);
#endif
  ierr = DMCompositeRestoreAccess(coupled->pack, QQ, &atmosQQ, &oceanQQ);CHKERRQ(ierr);
  if (lock != 0) {
    ierr = VecLockReadPush(QQ);CHKERRQ(ierr);
  }
#if defined(ADD_RHS_EVENTS)
  PetscLogEventEnd(SLOWBUFFER_RHS,0,0,0,0);
#endif
  PetscFunctionReturn(0); 
}

/* QQ is full vector, oceanRHS is the subvector of the ocean dm corresponding to the buffer zsplit IS*/
static PetscErrorCode FormCoupledRHS_Split_Ocean_Slow(TS ts, PetscReal t, Vec QQ, Vec oceanRHS, void *ctx)
{
  CoupledCtx* coupled = (CoupledCtx*)ctx;
  Vec atmosQQ, oceanQQ;
  PetscInt lock;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
#if defined(ADD_RHS_EVENTS)
  PetscLogEventBegin(SLOW_RHS,0,0,0,0);
#endif
  ierr = VecLockGet(QQ, &lock);CHKERRQ(ierr);
  if (lock != 0) {
    ierr = VecLockReadPop(QQ);CHKERRQ(ierr);
  }
  ierr = DMCompositeGetAccess(coupled->pack, QQ, &atmosQQ, &oceanQQ);CHKERRQ(ierr);
  /* No need for bounadry communication */
  ierr = FVNSComputeRHS_BOTTOM(coupled->ocean, oceanQQ, oceanRHS);CHKERRQ(ierr);
  ierr = DMCompositeRestoreAccess(coupled->pack, QQ, &atmosQQ, &oceanQQ);CHKERRQ(ierr);
  if (lock != 0) {
    ierr = VecLockReadPush(QQ);CHKERRQ(ierr);
  }
#if defined(ADD_RHS_EVENTS)
  PetscLogEventEnd(SLOW_RHS,0,0,0,0);
#endif
  PetscFunctionReturn(0);
}

/* QQ is full vector, atmosRHS is the subvector corresponding to just the atmos dm */
static PetscErrorCode FormCoupledRHS_Split_Atmos(TS ts, PetscReal t, Vec QQ, Vec atmosRHS, void *ctx)
{
  CoupledCtx* coupled = (CoupledCtx*)ctx;
  Vec atmosQQ, oceanQQ;
  PetscInt lock;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
#if defined(ADD_RHS_EVENTS)
  PetscLogEventBegin(FAST_RHS,0,0,0,0);
#endif
  ierr = VecLockGet(QQ, &lock);CHKERRQ(ierr);
  if (lock != 0) {
    ierr = VecLockReadPop(QQ);CHKERRQ(ierr);
  }
  ierr = DMCompositeGetAccess(coupled->pack, QQ, &atmosQQ, &oceanQQ);CHKERRQ(ierr);
  ierr = CommunicateCouplingBoundary_Atmos(coupled, atmosQQ, oceanQQ);CHKERRQ(ierr);
  ierr = FVNSComputeRHS(coupled->atmos, atmosQQ, atmosRHS);CHKERRQ(ierr);
  ierr = DMCompositeRestoreAccess(coupled->pack, QQ, &atmosQQ, &oceanQQ);CHKERRQ(ierr);
  if (lock != 0) {
    ierr = VecLockReadPush(QQ);CHKERRQ(ierr);
  }
#if defined(ADD_RHS_EVENTS)
  PetscLogEventEnd(FAST_RHS,0,0,0,0);
#endif
  PetscFunctionReturn(0); 
}

/* overlaps the buffer rhs computation and the coupling boundary commination from ocean to atmos */
static PetscErrorCode FormCoupledRHS_Split_Ocean_Buffer_Overlapped(TS ts, PetscReal t, Vec QQ, Vec oceanRHS, void *ctx)
{
  CoupledCtx* coupled = (CoupledCtx*)ctx;
  Vec atmosQQ, oceanQQ;
  PetscInt lock;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
#if defined(ADD_RHS_EVENTS)
  PetscLogEventBegin(SLOWBUFFER_RHS,0,0,0,0);
#endif
  ierr = VecLockGet(QQ, &lock);CHKERRQ(ierr);
  if (lock != 0) {
    ierr = VecLockReadPop(QQ);CHKERRQ(ierr);
  }
  ierr = DMCompositeGetAccess(coupled->pack, QQ, &atmosQQ, &oceanQQ);CHKERRQ(ierr);
#if defined(ADD_RHS_EVENTS)
  PetscLogEventBegin(SLOWBUFFER_RHS_BOUNDARY_COMM,0,0,0,0);
#endif
  ierr = FVNSExtractCouplingVars(coupled->atmos, atmosQQ);CHKERRQ(ierr);
  ierr = VecScatterBegin(coupled->atmosToOcean, coupled->atmos->couplingOwn, coupled->ocean->couplingOther, INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
  ierr = FVNSExtractCouplingVars(coupled->ocean, oceanQQ);CHKERRQ(ierr);
  ierr = VecScatterEnd(coupled->atmosToOcean, coupled->atmos->couplingOwn, coupled->ocean->couplingOther, INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
#if defined(ADD_RHS_EVENTS)
  PetscLogEventEnd(SLOWBUFFER_RHS_BOUNDARY_COMM,0,0,0,0);
#endif
  ierr = VecScatterBegin(coupled->oceanToAtmos, coupled->ocean->couplingOwn, coupled->atmos->couplingOther, INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
#if defined(ADD_RHS_EVENTS)
  PetscLogEventBegin(SLOWBUFFER_RHS_COMPUTE,0,0,0,0);
#endif
  ierr = FVNSComputeRHS_TOP(coupled->ocean, oceanQQ, oceanRHS);CHKERRQ(ierr);
#if defined(ADD_RHS_EVENTS)
  PetscLogEventEnd(SLOWBUFFER_RHS_COMPUTE,0,0,0,0);
#endif
  ierr = VecScatterEnd(coupled->oceanToAtmos, coupled->ocean->couplingOwn, coupled->atmos->couplingOther, INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
  ierr = DMCompositeRestoreAccess(coupled->pack, QQ, &atmosQQ, &oceanQQ);CHKERRQ(ierr);
  if (lock != 0) {
    ierr = VecLockReadPush(QQ);CHKERRQ(ierr);
  }
#if defined(ADD_RHS_EVENTS)
  PetscLogEventEnd(SLOWBUFFER_RHS,0,0,0,0);
#endif
  PetscFunctionReturn(0); 
}
/* asssumes the coupling boundary has already been communicated */ 
static PetscErrorCode FormCoupledRHS_Split_Atmos_Overlapped(TS ts, PetscReal t, Vec QQ, Vec atmosRHS, void *ctx)
{
 CoupledCtx* coupled = (CoupledCtx*)ctx;
  Vec atmosQQ, oceanQQ;
  PetscInt lock;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
#if defined(ADD_RHS_EVENTS)
  PetscLogEventBegin(FAST_RHS,0,0,0,0);
#endif
  ierr = VecLockGet(QQ, &lock);CHKERRQ(ierr);
  if (lock != 0) {
    ierr = VecLockReadPop(QQ);CHKERRQ(ierr);
  }
  ierr = DMCompositeGetAccess(coupled->pack, QQ, &atmosQQ, &oceanQQ);CHKERRQ(ierr);
  ierr = FVNSComputeRHS(coupled->atmos, atmosQQ, atmosRHS);CHKERRQ(ierr);
  ierr = DMCompositeRestoreAccess(coupled->pack, QQ, &atmosQQ, &oceanQQ);CHKERRQ(ierr);
  if (lock != 0) {
    ierr = VecLockReadPush(QQ);CHKERRQ(ierr);
  }
#if defined(ADD_RHS_EVENTS)
  PetscLogEventEnd(FAST_RHS,0,0,0,0);
#endif
  PetscFunctionReturn(0); 
}

static PetscErrorCode ComputeZBuffer(CoupledCtx* coupled, PetscInt bufferwidth,PetscInt *bufferz)
{
  PetscErrorCode ierr;
  DMDALocalInfo  info; 

  PetscFunctionBeginUser;
  ierr =  DMDAGetLocalInfo(coupled->ocean->dm,&info);CHKERRQ(ierr);
  (*bufferz) = info.mz - bufferwidth-1;
  PetscFunctionReturn(0); 
}

static PetscErrorCode PlotSolution(CoupledCtx* coupled, PetscInt step, Vec QQ)
{
  Vec atmosQQ, oceanQQ;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = DMCompositeGetAccess(coupled->pack, QQ, &atmosQQ, &oceanQQ);CHKERRQ(ierr);
  ierr = FVNSWriteSolutionVTK(coupled->atmos, atmosQQ, "out/3D/WDF_MULTI", step);CHKERRQ(ierr);
  ierr = FVNSWriteSolutionVTK(coupled->ocean, oceanQQ, "out/3D/WDF_MULTI", step);CHKERRQ(ierr);
  ierr = DMCompositeRestoreAccess(coupled->pack, QQ, &atmosQQ, &oceanQQ);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode PostStep(TS ts)
{
  CoupledCtx *coupled;
  Vec QQ;
  PetscInt step;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = TSGetApplicationContext(ts, &coupled);CHKERRQ(ierr);
  ierr = TSGetStepNumber(ts, &step);CHKERRQ(ierr);
  ierr = TSGetSolution(ts, &QQ);CHKERRQ(ierr);
  ierr = PlotSolution(coupled, step, QQ);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode PostStepGlvis(TS ts)
{
  CoupledCtx *coupled;
  Vec QQ,atmosQQ,oceanQQ;
  PetscInt step;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = TSGetApplicationContext(ts, &coupled);CHKERRQ(ierr);
  ierr = TSGetStepNumber(ts, &step);CHKERRQ(ierr);
  ierr = TSGetSolution(ts, &QQ);CHKERRQ(ierr);
  ierr = DMCompositeGetAccess(coupled->pack, QQ, &atmosQQ, &oceanQQ);CHKERRQ(ierr);
  ierr = VecView(atmosQQ,coupled->viewer1);CHKERRQ(ierr); 
  ierr = VecView(oceanQQ,coupled->viewer2);CHKERRQ(ierr); 
  ierr = DMCompositeRestoreAccess(coupled->pack, QQ, &atmosQQ, &oceanQQ);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode CoupleMonitorView_Conservation_CSV(TS ts ,PetscInt step,PetscReal time,Vec u ,void *mctx)
{
  CoupledCtx     *coupled = (CoupledCtx*)mctx;
  Vec            atmosQQ, oceanQQ;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = DMCompositeGetAccess(coupled->pack, u, &atmosQQ, &oceanQQ);CHKERRQ(ierr);
  ierr = FVNSMonitorView_Conservation_CSV(ts,step,time,atmosQQ,(void*)coupled->atmos);CHKERRQ(ierr); 
  ierr = FVNSMonitorView_Conservation_CSV(ts,step,time,oceanQQ,(void*)coupled->ocean);CHKERRQ(ierr); 
  ierr = DMCompositeRestoreAccess(coupled->pack, u, &atmosQQ, &oceanQQ);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

 
int main(int argc,char **argv)
{
  CoupledCtx *coupled;
  Vec QQ, RHS, atmosQQ, oceanQQ;
  TS ts;
  PetscMPIInt rank;
  PetscInt  max_steps=50000, nxe=8, nye=8, atmosnze=8, oceannze=8,numdm,i,bufferwidth=6,is_size; 
  PetscInt  slow_size,buffer_size,fast_size; 
  PetscReal dt=0.025, max_time=10.0, dx=0.1, dy=0.1, atmosdz=0.1, oceandz=0.1;
  PetscBool plot=PETSC_FALSE, flg,plotglvis = PETSC_FALSE,monitor_conservation = PETSC_FALSE,monitor_tssolve = PETSC_TRUE; 
  PetscBool is_view_size = PETSC_FALSE,isascii, overlapcoupling = PETSC_TRUE; 
  IS        *compositeIS,SlowIS,BufferIS,SlowIS_ocean,BufferIS_ocean;
  Solution  ocean_initial, ocean_final, ocean_diff,atmos_initial,atmos_final,atmos_diff; 
  PetscLogDouble timestart,timeend;
  PetscViewer    viewer; 
  PetscViewerType  viewertype; 
  PetscErrorCode ierr;
  
  PetscFunctionBeginUser;
  ierr = PetscInitialize(&argc,&argv,0,0); if (ierr) return ierr;
  ierr = PetscOptionsGetBool(NULL,NULL,"-plot",&plot,&flg);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(NULL,NULL,"-plot_glvis",&plotglvis,&flg);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(NULL,NULL,"-monitor_conservation",&monitor_conservation,&flg);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(NULL,NULL,"-monitor_tstime",&monitor_tssolve,&flg);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(NULL,NULL,"-overlap",&overlapcoupling,&flg);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-coupled_nxe",&nxe,&flg);CHKERRQ(ierr); 
  ierr = PetscOptionsGetInt(NULL,NULL,"-coupled_nye",&nye,&flg);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-atmos_nze",&atmosnze,&flg);CHKERRQ(ierr); 
  ierr = PetscOptionsGetInt(NULL,NULL,"-ocean_nze",&oceannze,&flg);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-buffer_width",&bufferwidth,&flg);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-dx",&dx,&flg);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-dy",&dy,&flg);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-atmos_dz",&atmosdz,&flg);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,NULL,"-ocean_dz",&oceandz,&flg);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(NULL,NULL,"-is_view_size",&is_view_size,&flg);CHKERRQ(ierr);

#if defined(ADD_RHS_EVENTS)
  PetscLogEventRegister("FastRHS",TS_CLASSID,&FAST_RHS);
  PetscLogEventRegister("SlowRHS",TS_CLASSID,&SLOW_RHS);
  PetscLogEventRegister("SlowBRHS",TS_CLASSID,&SLOWBUFFER_RHS);
  PetscLogEventRegister("SlowBRHS_Comp",TS_CLASSID,&SLOWBUFFER_RHS_COMPUTE);
  PetscLogEventRegister("SlowBRHS_Comm",TS_CLASSID,&SLOWBUFFER_RHS_BOUNDARY_COMM);
#endif
  ierr = InitializeCoupledProblem(PETSC_COMM_WORLD,nxe,nye,atmosnze, oceannze,dx,dy,atmosdz,oceandz,&coupled);CHKERRQ(ierr);

  /* extract a global solution and set initial conditions */
  ierr = DMCreateGlobalVector(coupled->pack, &QQ);CHKERRQ(ierr);
  ierr = DMCompositeGetAccess(coupled->pack, QQ, &atmosQQ, &oceanQQ);CHKERRQ(ierr);
  ierr = FVNSComputeInitialCondition(coupled->atmos, atmosQQ);CHKERRQ(ierr);
  ierr = FVNSComputeInitialCondition(coupled->ocean, oceanQQ);CHKERRQ(ierr);
  if (monitor_conservation) {
    ierr = FVNSIntegrate(coupled->ocean,oceanQQ,&ocean_initial);CHKERRQ(ierr); 
    ierr = FVNSIntegrate(coupled->atmos,atmosQQ,&atmos_initial);CHKERRQ(ierr);
  }
  ierr = DMCompositeRestoreAccess(coupled->pack, QQ, &atmosQQ, &oceanQQ);CHKERRQ(ierr);
   
  /* Create and set the TS Partitions for MPRK */
  ierr = TSCreate(PETSC_COMM_WORLD, &ts);CHKERRQ(ierr);

  /* get the IS for the partitions directly from the DMComposite */
  ierr = DMCompositeGetNumberDM(coupled->pack,&numdm);CHKERRQ(ierr);
  ierr = DMCompositeGetGlobalISs(coupled->pack,&compositeIS);CHKERRQ(ierr);  
  ierr = TSRHSSplitSetIS(ts,"fast",compositeIS[0]);CHKERRQ(ierr); /* atmos is fast */

  /* Split ocean into buffer and slow zone */
  ierr = ComputeZBuffer(coupled,bufferwidth, &coupled->ocean->multictx.bufferz);CHKERRQ(ierr);
  ierr = CreateZSplitIS_DA(coupled->ocean->dm,coupled->ocean->multictx.bufferz,&SlowIS_ocean,&BufferIS_ocean);CHKERRQ(ierr);
  ierr = ISCreateSubIS(compositeIS[1],BufferIS_ocean,&BufferIS);CHKERRQ(ierr);
  ierr = ISCreateSubIS(compositeIS[1],SlowIS_ocean,&SlowIS);CHKERRQ(ierr);
  ierr = ISDestroy(&SlowIS_ocean);CHKERRQ(ierr);
  ierr = ISDestroy(&BufferIS_ocean);CHKERRQ(ierr);
  ierr = TSRHSSplitSetIS(ts,"slow",SlowIS);CHKERRQ(ierr);
  ierr = TSRHSSplitSetIS(ts,"slowbuffer",BufferIS);CHKERRQ(ierr);

  ierr = TSRHSSplitSetRHSFunction(ts,"slow",NULL,FormCoupledRHS_Split_Ocean_Slow,(void*)coupled);CHKERRQ(ierr);
  if (overlapcoupling) {
    ierr = TSRHSSplitSetRHSFunction(ts,"fast",NULL,FormCoupledRHS_Split_Atmos_Overlapped,(void*)coupled);CHKERRQ(ierr); 
    ierr = TSRHSSplitSetRHSFunction(ts,"slowbuffer",NULL,FormCoupledRHS_Split_Ocean_Buffer_Overlapped,(void*)coupled);CHKERRQ(ierr);
  } else {
    ierr = TSRHSSplitSetRHSFunction(ts,"fast",NULL,FormCoupledRHS_Split_Atmos,(void*)coupled);CHKERRQ(ierr); 
    ierr = TSRHSSplitSetRHSFunction(ts,"slowbuffer",NULL,FormCoupledRHS_Split_Ocean_Buffer,(void*)coupled);CHKERRQ(ierr);
  }
  
  /* create and setup the time stepper */
  ierr = TSSetDM(ts, coupled->pack);CHKERRQ(ierr);
  ierr = TSSetProblemType(ts, TS_LINEAR);CHKERRQ(ierr);
  ierr = TSSetType(ts, TSMPRK);CHKERRQ(ierr);
  ierr = TSSetTimeStep(ts, dt);CHKERRQ(ierr);
  ierr = TSSetMaxTime(ts, max_time);CHKERRQ(ierr);
  ierr = TSSetMaxSteps(ts, max_steps);CHKERRQ(ierr);
  ierr = TSSetExactFinalTime(ts, TS_EXACTFINALTIME_STEPOVER);CHKERRQ(ierr);
  ierr = TSSetUseSplitRHSFunction(ts,PETSC_TRUE);CHKERRQ(ierr);
  ierr = TSSetFromOptions(ts);CHKERRQ(ierr);
  ierr = TSSetUp(ts);CHKERRQ(ierr);

  /* solve the problem and then distribute the solution to the local vectors for plotting */
  ierr = DMCreateGlobalVector(coupled->pack, &RHS);CHKERRQ(ierr);
 /* print the is sizes to screen because -is_view doesn't have this option */
  flg = PETSC_FALSE;
  ierr = PetscOptionsGetViewer(coupled->comm,NULL,NULL,"-view_region_size",&viewer,NULL,&flg);CHKERRQ(ierr);
  if (flg) {
    ierr = PetscViewerGetType(viewer,&viewertype);CHKERRQ(ierr);
    ierr = PetscStrcmp(viewertype,PETSCVIEWERASCII,&isascii);CHKERRQ(ierr);
    if (isascii) {
      ierr = VecGetSize(RHS,&is_size);CHKERRQ(ierr);
      ierr = PetscViewerASCIIPrintf(viewer,"Total Number of Dof: %i \n",is_size);CHKERRQ(ierr);

      ierr = ISGetSize(compositeIS[0],&is_size);CHKERRQ(ierr);
      ierr = PetscViewerASCIIPrintf(viewer,"Fast Region Number of Dof: %i \n",is_size);CHKERRQ(ierr);
      ierr = ISGetSize(BufferIS,&is_size);CHKERRQ(ierr);
      ierr = PetscViewerASCIIPrintf(viewer,"Buffer Region Number of Dof: %i \n",is_size);CHKERRQ(ierr);
      ierr = ISGetSize(SlowIS,&is_size);CHKERRQ(ierr);
      ierr = PetscViewerASCIIPrintf(viewer,"Slow Region Number of Dof: %i \n",is_size);CHKERRQ(ierr);
    }
    ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);

  }
  flg = PETSC_FALSE;
  ierr = PetscOptionsGetViewer(coupled->comm,NULL,NULL,"-view_region_size_local",&viewer,NULL,&flg);CHKERRQ(ierr);
  if (flg) {
    ierr = MPI_Comm_rank(coupled->comm,&rank);CHKERRQ(ierr);
    ierr = ISGetLocalSize(compositeIS[0],&fast_size);CHKERRQ(ierr);
    ierr = ISGetLocalSize(BufferIS,&slow_size);CHKERRQ(ierr);
    ierr = ISGetLocalSize(SlowIS,&buffer_size);CHKERRQ(ierr);
    /* there are 5 dofs per element of the DMDA, so divide all IS outputs by 5 to get number of elements */
    fast_size /= 5; slow_size /= 5; buffer_size /= 5; 
    ierr = PetscViewerASCIIPrintf(viewer,"Rank, Slow, SlowBuffer, Fast, Total \n");CHKERRQ(ierr);
    ierr = PetscViewerASCIIPushSynchronized(viewer);CHKERRQ(ierr);
    ierr = PetscViewerASCIISynchronizedPrintf(viewer,"%i,     %i,      %i,     %i,   %i\n" ,rank,slow_size,buffer_size,fast_size,slow_size+buffer_size+fast_size);CHKERRQ(ierr);
    ierr = PetscViewerFlush(viewer);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPopSynchronized(viewer);CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  }

  ierr = TSSetRHSFunction(ts, RHS, FormCoupledRHS, (void*)coupled);CHKERRQ(ierr);
  if (plot || plotglvis) {
    ierr = TSSetApplicationContext(ts, (void*)coupled);CHKERRQ(ierr);
    if (plotglvis) {
      ierr = PetscViewerGLVisOpen(PETSC_COMM_WORLD,PETSC_VIEWER_GLVIS_SOCKET,"localhost",PETSC_DECIDE,&coupled->viewer1);CHKERRQ(ierr);
      ierr = PetscViewerGLVisOpen(PETSC_COMM_WORLD,PETSC_VIEWER_GLVIS_SOCKET,"localhost",PETSC_DECIDE,&coupled->viewer2);CHKERRQ(ierr);
      ierr = TSSetPostStep(ts, PostStepGlvis);CHKERRQ(ierr);
    } else {
    ierr = PlotSolution(coupled, 0, QQ);CHKERRQ(ierr);
    ierr = TSSetPostStep(ts,PostStep);CHKERRQ(ierr);
    }
  }
  if (monitor_conservation) ierr = TSMonitorSet(ts,CoupleMonitorView_Conservation_CSV,(void*)coupled,NULL);CHKERRQ(ierr);
 
 ierr = PetscTime(&timestart);CHKERRQ(ierr);
 ierr = TSSolve(ts, QQ);CHKERRQ(ierr);
 ierr = PetscTime(&timeend);CHKERRQ(ierr);

 if (monitor_tssolve) {ierr = PetscPrintf(PETSC_COMM_WORLD,"Time for TSSolve: \n %16e",timeend-timestart);CHKERRQ(ierr);}
  
  /* Conservative Variable Conservation Check */
  if (monitor_conservation) {
    ierr = DMCompositeGetAccess(coupled->pack, QQ, &atmosQQ, &oceanQQ);CHKERRQ(ierr);
    ierr = FVNSIntegrate(coupled->ocean,oceanQQ,&ocean_final);CHKERRQ(ierr); 
    ierr = FVNSIntegrate(coupled->atmos,atmosQQ,&atmos_final);CHKERRQ(ierr);
    ierr = DMCompositeRestoreAccess(coupled->pack, QQ, &atmosQQ, &oceanQQ);CHKERRQ(ierr);

    SolutionAXPBY(1.0 , &ocean_initial, -1.0, &ocean_final,&ocean_diff);
    SolutionAXPBY(1.0 , &atmos_initial, -1.0, &atmos_final,&atmos_diff);

    ierr = PetscViewerASCIIPrintf(PETSC_VIEWER_STDERR_WORLD,"Conservation Variable Integral Difference Ocean:\n");CHKERRQ(ierr);
    ierr = FVNSView_Solution_Point(coupled->ocean,PETSC_VIEWER_STDERR_WORLD,ocean_diff);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPrintf(PETSC_VIEWER_STDERR_WORLD,"Conservation Variable Integral Difference Atmos:\n");CHKERRQ(ierr);
    ierr = FVNSView_Solution_Point(coupled->ocean,PETSC_VIEWER_STDERR_WORLD,atmos_diff);CHKERRQ(ierr);
  }
   
  /* clean up data */
  ierr = VecDestroy(&QQ);CHKERRQ(ierr);
  ierr = VecDestroy(&RHS);CHKERRQ(ierr);
  ierr = DestroyCoupledProblem(&coupled);CHKERRQ(ierr);
  ierr = TSDestroy(&ts);CHKERRQ(ierr);
  for(i=0; i<numdm; i++) { ierr = ISDestroy(&compositeIS[i]);CHKERRQ(ierr);}
  ierr = PetscFree(compositeIS);CHKERRQ(ierr);
  ierr = ISDestroy(&BufferIS);CHKERRQ(ierr);
  ierr = ISDestroy(&SlowIS);CHKERRQ(ierr);
  ierr = PetscFinalize();
  return ierr;
}