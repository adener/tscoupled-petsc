## FVNS -- Finite Volume Navier-Stokes Solver (3D)

A finite-volume DNS code for the Navier-Stokes equations based on the
[`tscoupled`](https://gitlab.com/emconsta/tscoupled) solver written in Julia.

***Note:*** This branch (`denera/multiratewithbuffer`) is the 3D extension of the original 2D
implementation in `main` with multirate capabilities.

### Build Instructions

1. Configure and build PETSc/TAO 3.15 with MPI
2. Make sure the `$PETSC_DIR` environment variable points to the correct folder
for the PETSc install.
3. Run `make test_CNS3D` to compile the executable for the single-domain problem, 
`make test_coupledCNS3D` for the coupled problem, `make test_coupledCNS3D_Multirate` for the basic multirate coupled problem, `make test_coupled_buffer` for the fast/slowbuffer/slow multirate coupled problem,and `make all` for all.

### Command-line Options

* `-ptype` -- problem type; 
      `tgv` Taylor-Green Vortex 
      `ldc` Lid-Driven Cavity 
      `hs`  Hydrostatic 
      `tb`  ThermalBubble 
      `wdf` Wave Driven Flow 
* `-alpha` -- viscous dissippation
* `-gamma` -- specific weight
* `-pr` -- Prandtl number
* `-re` -- Reynolds number
* `-mu` -- dynamic viscosity
* `-g`  -- gravity (nondimensionalized)
* `-u0`, `-v0`, `-w0` -- initial velocity magnitudes in cardinal directions
* `-pres0` -- reference pressure
* `-temp0` -- reference temperature
* `-cv`    -- Specific Heat Capacity at constant volume 
* `-L_nondim` -- nondimensionalization length scale 
* `-temp_nondim` -- nondim temperature 
* `-dens_nondim` -- nondim density 
* `-pres_nondim` -- nondim pressure
* `-nxe`, `-nye`, `-nze` -- number of discrete cells in cardinal directions
* `-dx`, `-dy` , `-dz`   -- cell size in each direction 
* `-x_top, -y_top, -z_top` -- top coordinate of the mesh (only active with fixed geometry)
* `-x_bot, -y_bot, -z_bot` -- bottom coordinate of the mesh (defaults to (0,0,0))
* `-fixedgeom` -- fixes the geometry of the mesh. Deactivates the -d[xyz] options. 
* `-fvns_view` -- view the problem setup. takes in a petscviewer as a parameter 

***Note:*** Refer to the PETSc/TAO manual for [options related to the time stepper (TS)](https://petsc.org/release/docs/manualpages/TS/TSSetFromOptions.html#TSSetFromOptions).

### Installing GLVis 

Some multirate examples have the option to view the solution using GLVis. This requires installing GLVis 
and comfiguring PETSc to use it. Unfortunately the --download-glvis option is broken, so the recommended procedure to install GLVis is the following: 

1. Build GLVis locally: 
   Go here https://glvis.org/building/ and follow the instructions for building GLVis. This requires installing a serial version of MFEM as well. 
2. Configure PETSc using the --with-glvis-dir option using the path of your local install. 

### Usage Examples

**Taylor-Green Vortex**

`./test_CNS3D -ptype tgv -nxe 32 -nye 32 -nze 32 -ts_monitor`

**Lid-Driven Cavity**

`./test_CNS3D -ptype ldc -nxe 16 -nye 16 -nze 16 -ts_monitor -fixedgeom`

*Note:* Larger LDC problems are unstable with explicit time integrators.

**Hydro-Static Flow**

`./test_CNS3D -ptype hs -nxe 16 -nye 16 -nze 16 -ts_monitor -g 9.81 -ts_rk_type 2a -ts_dt 0.03 -L_nondim 100`

- `-exner_view`   views  exner pressure -1. (The perturbation from 1) Takes in commandline petscviewer option


**Thermal Bubble**

`./test_CNS3D -ptype tb -nxe 20 -nye 20 -nze 20 ts_monitor -g 9.81 -dx 0.1 -dy 0.1 -dz 0.1 -bubble_x 0 -bubble_y 0 -bubble_z 2.5 -bubble_r 1 -dtemp0 100 -ts_rk_type 4 -ts_dt 0.02 -L_nondim 100 -exner_view draw -draw_pause -fixedgeom -x_bot -5 -y_bot -5 -z_bot 0 -x_top 5 -y_top 5 -z_top 5`

Copied from the slack post by Shinhoo. 

- `-bubble_[xyz]` adjusts the center of the thermal bubble 
- `-bubble_r `    adjusts the radius of the thermal bubble 
- `-dtemp0`       scale the temperature difference of the bubble 
- `-exner_view`   views  exner pressure -1. (The perturbation from 1) Takes in commandline petscviewer option

**Coupled Wind-Driven Flow**

`./test_coupledCNS3D -coupled_nxe 32 -coupled_nye 32 -atmos_nze 16 -ocean_nze 32 -ts_monitor`

This test problem contains two instances of the FVNS solver with the `atmos_` and `ocean_`
prefixes. Properties for each can be controlled independently by adding the prefix to the
above command-line options; for example, `-atmos_alpha` will alter the viscous dissippation
for the atmosphere model only. Since the two solvers are coupled together at the bottom/top
boundaries, they must have the same discretization size across the x- and y-directions
controlled with `-coupled_nxe` and `-coupled_nye` flags.

**Coupled Wind-Driven Flow Multirate with Buffer**

`./test_coupled_buffer -coupled_nxe 16 -coupled_nye 16 -atmos_nze 16 -ocean_nze 8 -ts_monitor -ts_dt 0.05` 

**Requires and up to date version of petsc main or release**

This test problem is the simple fast/slowbuffer/slow version of the 
**Coupled Wind-Driven Flow** flowproblem above. This inherits the command-line options 
and setup of the **Coupled Wind-Driven Flow** problem. The integrals of the conserved variables per time
step are saved in out/WDF_MULTI in csv files. 

This tests multirate time integrators with fast and slow and slowbuffer partioning, compare against a singlerate method to see the improvement of multirate. The partitions are: 
- Fast       : Atmosphere 
- SlowBuffer : A "z split" of the ocean DMDA connecting the atmosphere (the top piece after cutting the
               DMDA along the z axis). It's width is given by the -bufferwidth commandline option. 
- Slow       : The part of the ocean away from the atmosphere. Specifically the complement of the
               slowbuffer in the ocean DMDA. 

 Note that the default rk solver with the internal adaptivity does not appear to be choosing a stable time-step, one can see some significant oscillations in the solution.

### Command-line Options for this example 

* `-bufferwidth` -- size of the buffer region
* `-plot`        -- Write the soltion to a vts file for later viewing 
* `-plot_glvis`  -- View the solution in real-time using a GLVis socket. PETSc must be compiled with GLvis 
                    for this to work. 
* `-monitor_conservation`  -- monitor integrals of conserved variables at every time step. Defaults to true.
* `-view_region_size` -- Print out the sizes (# of dofs) of the multirate regions.  Takes in an ascii viewer arg.
* `-view_region_size_local` -- Print out the sizes (# of elements) for the multirate regions for each processor.  Takes in an ascii viewer arg.
* `-overlap`     -- Overlap the computation of the bufferrhs and the ocean -> atmos boundary coupling (Default is TRUE) 


See the following for the ts command line options for mrpk https://petsc.org/release/docs/manualpages/TS/TSMPRKType.html#TSMPRKType


**Coupled Hydrostatic Thermal Bubble Multirate with Buffer**

We have a version where the number of elements and mesh size are chosen independently, but changing geometry. 

`./test_coupled_buffer -coupled_nxe 20 -coupled_nye 20 -atmos_nze 20 -ocean_nze 10 -ts_monitor -ts_dt 0.04 -ts_max_time 10 -atmos_ptype hs -ocean_ptype tb -dx 0.2 -dy 0.2 -ocean_dz 0.2 -atmos_dz 0.1 -ocean_bubble_x 2 -ocean_bubble_y 2 -ocean_dtemp0 100 -atmos_g 9.81 -ocean_g 9.81 -ocean_L_nondim 100 -atmos_L_nondim 100  -atmos_z_bot 2`

And a version with fixed geometry but only the number of elements can be changed. This has domain  [-5,5] x [-5,5] x[-5,0] for ocean and  [-5,5] x [-5,5] x [0,5] for atmosphere. 

`./test_coupled_buffer -coupled_nxe 20 -coupled_nye 20 -atmos_nze 20 -ocean_nze 20 -ts_monitor -ts_dt 0.02 -ts_max_time 10 -atmos_ptype hs -ocean_ptype tb -ocean_bubble_x 0 -ocean_bubble_y 0 -ocean_bubble_z -4 -ocean_dtemp0 100 -atmos_g 9.81 -ocean_g 9.81 -ocean_L_nondim 100 -atmos_L_nondim 100 -ocean_bubble_r 1 -atmos_fixedgeom -ocean_fixedgeom -atmos_x_top 5 -atmos_y_top 5 -atmos_x_bot -5 -atmos_y_bot -5 -atmos_z_bot 0 -atmos_z_top 5 -ocean_x_top 5 -ocean_y_top 5 -ocean_x_bot -5 -ocean_y_bot -5 -ocean_z_bot -5 -ocean_z_top 0`

We have a thermal bubble for the ocean and a hydrostatic flow for the atmosphere. Has the same basic options as the previous example. 

- `-bubble_[xyz]` adjusts the center of the thermal bubble 
- `-bubble_r `    adjusts the radius of the thermal bubble 
- `-dtemp0`       scale the temperature difference of the bubble 
- `-atmos_exner_view`   views  exner pressure -1. (The perturbation from 1) Takes in commandline petscviewer option
- `-ocean_exner_view`   views  exner pressure -1. (The perturbation from 1) Takes in commandline petscviewer option
