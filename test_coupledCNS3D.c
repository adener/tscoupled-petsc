#include "fvns3d.h"

typedef struct {
  MPI_Comm          comm;
  FVNS*             atmos;
  FVNS*             ocean;
  DM                pack;
  VecScatter        atmosToOcean;
  VecScatter        oceanToAtmos;
} CoupledCtx;

static PetscErrorCode InitializeCoupledProblem(MPI_Comm comm, PetscInt nxe, PetscInt nye, PetscInt atmosNz, PetscInt oceanNz, CoupledCtx** coupled)
{ 
  CoupledCtx *newctx;
  ProblemDefs *atmosDefs, *oceanDefs;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  /* crate the coupled context*/
  ierr = PetscNew(&newctx);CHKERRQ(ierr);
  newctx->comm = comm;

  /* initialize the PDEs for each subdomain */
  ierr = ProblemDefsCreate("atmos","out/3D/WDF", PTYPE_WDF, DOWN, &atmosDefs);CHKERRQ(ierr);
  ierr = FVNSCreate(newctx->comm, atmosDefs, &newctx->atmos);CHKERRQ(ierr);
  ierr = FVNSSetNumElements(newctx->atmos,nxe,nye,atmosNz);CHKERRQ(ierr);
  ierr = FVNSSetFromOptions(newctx->atmos);CHKERRQ(ierr); 
  ierr = FVNSSetUp(newctx->atmos);CHKERRQ(ierr);
  ierr = FVNSView(newctx->atmos, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  ierr = ProblemDefsCreate("ocean","out/3D/WDF", PTYPE_WDF, UP, &oceanDefs);CHKERRQ(ierr);
  ierr = FVNSCreate(newctx->comm, oceanDefs, &newctx->ocean);CHKERRQ(ierr);
  ierr = FVNSSetNumElements(newctx->ocean,nxe,nye,oceanNz);CHKERRQ(ierr);
  ierr = FVNSSetFromOptions(newctx->ocean);CHKERRQ(ierr); 
  ierr = FVNSSetUp(newctx->ocean);CHKERRQ(ierr);
  ierr = FVNSView(newctx->ocean, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  ierr = PetscViewerFlush(PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);

  /* create the composite packer */
  ierr = DMCompositeCreate(newctx->comm, &newctx->pack);CHKERRQ(ierr);
  ierr = DMSetOptionsPrefix(newctx->pack, "coupled_");CHKERRQ(ierr);
  ierr = DMCompositeAddDM(newctx->pack, newctx->atmos->dm);CHKERRQ(ierr);
  ierr = DMCompositeAddDM(newctx->pack, newctx->ocean->dm);CHKERRQ(ierr);
  ierr = DMSetFromOptions(newctx->pack);CHKERRQ(ierr);
  ierr = DMSetUp(newctx->pack);CHKERRQ(ierr);

  /* create scatters to communicate boundaries */
  ierr = VecScatterCreate(newctx->atmos->couplingOwn, newctx->ocean->couplingIS,
                          newctx->ocean->couplingOther, newctx->ocean->couplingIS,
                          &newctx->atmosToOcean);
  ierr = VecScatterCreate(newctx->ocean->couplingOwn, newctx->atmos->couplingIS,
                          newctx->atmos->couplingOther, newctx->atmos->couplingIS,
                          &newctx->oceanToAtmos);

  *coupled = newctx;
  PetscFunctionReturn(0);
}

static PetscErrorCode DestroyCoupledProblem(CoupledCtx** coupled)
{
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = FVNSDestroy(&(*coupled)->atmos);CHKERRQ(ierr);
  ierr = FVNSDestroy(&(*coupled)->ocean);CHKERRQ(ierr);
  ierr = DMDestroy(&((*coupled)->pack));CHKERRQ(ierr);
  ierr = VecScatterDestroy(&(*coupled)->atmosToOcean);CHKERRQ(ierr);
  ierr = VecScatterDestroy(&(*coupled)->oceanToAtmos);CHKERRQ(ierr);
  ierr = PetscFree(*coupled);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode CommunicateCouplingBoundary(
  CoupledCtx* cpl, Vec atmosQQ, Vec oceanQQ)
{
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = FVNSExtractCouplingVars(cpl->atmos, atmosQQ);CHKERRQ(ierr);
  ierr = FVNSExtractCouplingVars(cpl->ocean, oceanQQ);CHKERRQ(ierr);
  ierr = VecZeroEntries(cpl->atmos->couplingOther);CHKERRQ(ierr);
  ierr = VecZeroEntries(cpl->ocean->couplingOther);CHKERRQ(ierr);
  ierr = VecScatterBegin(cpl->atmosToOcean, cpl->atmos->couplingOwn, cpl->ocean->couplingOther, INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
  ierr = VecScatterBegin(cpl->oceanToAtmos, cpl->ocean->couplingOwn, cpl->atmos->couplingOther, INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
  ierr = VecScatterEnd(cpl->atmosToOcean, cpl->atmos->couplingOwn, cpl->ocean->couplingOther, INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
  ierr = VecScatterEnd(cpl->oceanToAtmos, cpl->ocean->couplingOwn, cpl->atmos->couplingOther, INSERT_VALUES, SCATTER_FORWARD);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode FormCoupledRHS(TS ts, PetscReal t, Vec QQ, Vec LRHSQ, void *ctx)
{
  CoupledCtx* coupled = (CoupledCtx*)ctx;
  Vec atmosQQ, oceanQQ, atmosRHS, oceanRHS;
  PetscInt lock;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = VecLockGet(QQ, &lock);CHKERRQ(ierr);
  if (lock != 0) {
    ierr = VecLockReadPop(QQ);CHKERRQ(ierr);
  }
  ierr = DMCompositeGetAccess(coupled->pack, QQ, &atmosQQ, &oceanQQ);CHKERRQ(ierr);
  ierr = DMCompositeGetAccess(coupled->pack, LRHSQ, &atmosRHS, &oceanRHS);CHKERRQ(ierr);

  ierr = CommunicateCouplingBoundary(coupled, atmosQQ, oceanQQ);CHKERRQ(ierr);
  ierr = FVNSComputeRHS(coupled->atmos, atmosQQ, atmosRHS);CHKERRQ(ierr);
  ierr = FVNSComputeRHS(coupled->ocean, oceanQQ, oceanRHS);CHKERRQ(ierr);

  ierr = DMCompositeRestoreAccess(coupled->pack, QQ, &atmosQQ, &oceanQQ);CHKERRQ(ierr);
  ierr = DMCompositeRestoreAccess(coupled->pack, LRHSQ, &atmosRHS, &oceanRHS);CHKERRQ(ierr);
  if (lock != 0) {
    ierr = VecLockReadPush(QQ);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

static PetscErrorCode PlotSolution(CoupledCtx* coupled, PetscInt step, Vec QQ)
{
  Vec atmosQQ, oceanQQ;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = DMCompositeGetAccess(coupled->pack, QQ, &atmosQQ, &oceanQQ);CHKERRQ(ierr);
  ierr = FVNSWriteSolutionVTK(coupled->atmos, atmosQQ, "out/3D/WDF", step);CHKERRQ(ierr);
  ierr = FVNSWriteSolutionVTK(coupled->ocean, oceanQQ, "out/3D/WDF", step);CHKERRQ(ierr);
  ierr = DMCompositeRestoreAccess(coupled->pack, QQ, &atmosQQ, &oceanQQ);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode PostStep(TS ts)
{
  CoupledCtx *coupled;
  Vec QQ;
  PetscInt step;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = TSGetApplicationContext(ts, &coupled);CHKERRQ(ierr);
  ierr = TSGetStepNumber(ts, &step);CHKERRQ(ierr);
  ierr = TSGetSolution(ts, &QQ);CHKERRQ(ierr);
  ierr = PlotSolution(coupled, step, QQ);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

int main(int argc,char **argv)
{
  CoupledCtx *coupled;
  Vec QQ, RHS, atmosQQ, oceanQQ;
  TS ts;
  PetscInt max_steps=50000, nxe=64, nye=64, nze=64;;
  PetscReal dt=1e-3, max_time=50.0;
  PetscBool plot, flg;
  PetscErrorCode ierr;
  
  PetscFunctionBeginUser;
  ierr = PetscInitialize(&argc,&argv,0,0); if (ierr) return ierr;
  ierr = PetscOptionsGetBool(NULL,NULL,"-plot",&plot,&flg);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(NULL,NULL,"-coupled_nxe",&nxe,&flg);CHKERRQ(ierr); 
  ierr = PetscOptionsGetInt(NULL,NULL,"-coupled_nye",&nye,&flg);CHKERRQ(ierr);
  ierr = InitializeCoupledProblem(PETSC_COMM_WORLD, nxe, nye, nze, nze, &coupled);CHKERRQ(ierr);

  /* extract a global solution and set initial conditions */
  ierr = DMCreateGlobalVector(coupled->pack, &QQ);CHKERRQ(ierr);
  ierr = DMCompositeGetAccess(coupled->pack, QQ, &atmosQQ, &oceanQQ);CHKERRQ(ierr);
  ierr = FVNSComputeInitialCondition(coupled->atmos, atmosQQ);CHKERRQ(ierr);
  ierr = FVNSComputeInitialCondition(coupled->ocean, oceanQQ);CHKERRQ(ierr);
  ierr = DMCompositeRestoreAccess(coupled->pack, QQ, &atmosQQ, &oceanQQ);CHKERRQ(ierr);

  /* create and setup the time stepper */
  ierr = TSCreate(PETSC_COMM_WORLD, &ts);CHKERRQ(ierr);
  ierr = TSSetDM(ts, coupled->pack);CHKERRQ(ierr);
  ierr = TSSetProblemType(ts, TS_LINEAR);CHKERRQ(ierr);
  ierr = TSSetType(ts, TSRK);CHKERRQ(ierr);
  ierr = TSSetTimeStep(ts, dt);CHKERRQ(ierr);
  ierr = TSSetMaxTime(ts, max_time);CHKERRQ(ierr);
  ierr = TSSetMaxSteps(ts, max_steps);CHKERRQ(ierr);
  ierr = TSSetExactFinalTime(ts, TS_EXACTFINALTIME_STEPOVER);CHKERRQ(ierr);
  ierr = TSSetFromOptions(ts);CHKERRQ(ierr);
  ierr = TSSetUp(ts);CHKERRQ(ierr);

  /* solve the problem and then distribute the solution to the local vectors for plotting */
  ierr = DMCreateGlobalVector(coupled->pack, &RHS);CHKERRQ(ierr);
  ierr = TSSetRHSFunction(ts, RHS, FormCoupledRHS, (void*)coupled);CHKERRQ(ierr);
  if (plot) {
    ierr = PlotSolution(coupled, 0, QQ);CHKERRQ(ierr);
    ierr = TSSetApplicationContext(ts, (void*)coupled);CHKERRQ(ierr);
    ierr = TSSetPostStep(ts, PostStep);CHKERRQ(ierr);
  }
  ierr = TSSolve(ts, QQ);CHKERRQ(ierr);
  
  /* clean up data */
  ierr = VecDestroy(&QQ);CHKERRQ(ierr);
  ierr = VecDestroy(&RHS);CHKERRQ(ierr);
  ierr = DestroyCoupledProblem(&coupled);CHKERRQ(ierr);
  ierr = TSDestroy(&ts);CHKERRQ(ierr);

  ierr = PetscFinalize();
  return ierr;
}
