#include <petsc.h>
#include <petscviewerhdf5.h>

typedef enum   _e_ProblemType           ProblemType;
typedef enum   _e_BoundaryType          BoundaryType;
typedef enum   _e_BoundaryLocation      BoundaryLocation;
typedef struct _s_Solution              Solution;
typedef struct _s_Primitives            Primitives;
typedef struct _s_ProblemDefs           ProblemDefs;
typedef struct _s_FVNS                  FVNS;
typedef struct _s_Multirate             MultirateCtx; 
typedef struct _s_FluidParameters       FluidParameters; 
typedef struct _s_NonDimensionalization NonDimensionalization; 
typedef struct _s_FluidReferenceStates  FluidReferenceStates;

extern const char* ProblemTypes[8];
enum _e_ProblemType {
  PTYPE_LDC,
  PTYPE_TGV,
  PTYPE_WDF,
  PTYPE_HYDROSTATIC,
  PTYPE_THERMALBUBBLE
};

extern const char* BoundaryTypes[6];
enum _e_BoundaryType {
  PERIODIC,
  DIRICHLET,
  COUPLING,
  DIRICHLET_RIEMANN_INVAR
};

extern const char* BoundaryLocations[8];
enum _e_BoundaryLocation {
  NONE, LEFT, RIGHT, DOWN, UP, FRONT, BACK
};

struct _s_Solution {
  PetscScalar       rho, rhoU, rhoV, rhoW, rhoE;
};

struct _s_Primitives {
  PetscScalar       r, u, v, w, p, e;
};

struct _s_FluidParameters {
  /* ideal gas parameters (seperate?) */
  PetscScalar gamma; 
  PetscScalar Rgas; 

  PetscScalar mu; /* Dimensionless, Mach Number */
  PetscScalar visc_diss_alpha; 
  PetscScalar pr; /* Dimensionless, Prandtl numbere */

  PetscScalar cp; /* [J/(kg K)] specific heat at constant pressure */
  PetscScalar cv; /* [J/(kg K)] specific heat at constant volume */
  PetscScalar R;  /* [J/(kg K)] cp - cv NOT SURE IF TRUE IN GENERAL ASK */ 
};

struct _s_FluidReferenceStates {
  PetscScalar temp;     /* [K] */
  PetscScalar pressure; /* [Pa] */
  PetscScalar density;  /* [kg/m^3] */
  PetscScalar u,v,w;    /* [m/s] */
};

struct _s_NonDimensionalization {
  PetscScalar L;        /* [m] */
  PetscScalar temp;     /* [K] */
  PetscScalar pressure; /* [Pa] */
  PetscScalar density;  /* [kg/m^3] */
}; 

struct _s_ProblemDefs {
  PetscReal         xd, yd, zd;
  PetscScalar       gamma, mu, pr, visc_diss_alpha, g, g_dim, cp ,cv,R; 
  PetscScalar       re;
  PetscScalar       td_left, td_right, td_down, td_up, td_front, td_back;
  PetscScalar       ud_left, ud_right, ud_down, ud_up, ud_front, ud_back;
  PetscScalar       vd_left, vd_right, vd_down, vd_up, vd_front, vd_back;
  PetscScalar       wd_left, wd_right, wd_down, wd_up, wd_front, wd_back;
  PetscBool         xPeriodic, yPeriodic, zPeriodic;
  FluidReferenceStates  fluidref; 
  NonDimensionalization nondim; 
  BoundaryType      leftBC, rightBC, downBC, upBC, frontBC, backBC;
  BoundaryLocation  couplingSide;
  ProblemType       ptype;
  void              (*initFunc)(ProblemDefs*,PetscReal,PetscReal,PetscReal,Solution*);
  char*             prefix; 
  char*             outdir;
  /* temporary additional parameters for the temperature bubble problem */
  PetscReal        bubble_x,bubble_y,bubble_z, bubble_r, dtemp0;
  /* for holding pressure profiles (exner pressure) */ 
  Vec              exnerpres; 
};

/* WIP structure for storing information needed for my multirate hack */

struct _s_Multirate {
  MatStencil  lowerTOP_stencil, upperTOP_stencil, lowerBOTTOM_stencil,upperBOTTOM_stencil; /* Needed for DMDACreatePatchIS */
  PetscInt    bufferz; /* We partition our buffers by depth only, this is the only variable 
                          we need to generate our multirate partition */     
};

struct _s_FVNS {
  PetscScalar       x_upcorner, y_upcorner, z_upcorner, x_bottomcorner, y_bottomcorner, z_bottomcorner; 
  PetscBool         fixedgeometry; 
  PetscInt          nxe, nye, nze;
  PetscReal         dx, dy, dz;
  ProblemDefs       *defs;
  DM                dm;
  Vec               QQloc, RHSloc, BCloc;
  Vec               QF_L, QF_R, QF_D, QF_U, QF_F, QF_B;
  Solution          ***qf_L, ***qf_R, ***qf_D, ***qf_U, ***qf_F, ***qf_B;
  DM                dmPrim;
  Vec               XG, YG, ZG, PF_L, PF_R, PF_D, PF_U, PF_F, PF_B;
  Primitives        ***xg, ***yg, ***zg, ***pf_L, ***pf_R, ***pf_D, ***pf_U, ***pf_F, ***pf_B;
  IS                couplingIS;
  Vec               couplingOwn, couplingOther;
  PetscInt          im, jm, is, js, gim, gjm, gis, gjs, mi, mj;
  MPI_Comm          comm;
  MultirateCtx      multictx; 
  PetscViewer       conservation_viewer; 
};

PetscBool SolutionCheck(PetscBool, Solution*);
PetscErrorCode SolutionView(MPI_Comm, Solution*);
void SolutionCopy(Solution*, Solution*);
void SolutionAXPBY(PetscScalar, Solution*, PetscScalar, Solution*, Solution*);
void SolutionAXPBYPCZ(PetscScalar, Solution*, PetscScalar, Solution*, PetscScalar, Solution*, Solution*);

PetscErrorCode PrimitivesView(MPI_Comm, Primitives*);
void PrimitivesCopy(Primitives*, Primitives*);
void PrimitivesAXPBY(PetscScalar, Primitives*, PetscScalar, Primitives*, Primitives*);
void PrimitivesAXPBYPCZ(PetscScalar, Primitives*, PetscScalar, Primitives*, PetscScalar, Primitives*, Primitives*);

PetscErrorCode ApplyPhysicalBoundaryConditions(FVNS*, Solution***);
PetscErrorCode ComputePrimitiveGradients(FVNS*, Solution***);
PetscErrorCode ReconstructSolutionOnFaces(FVNS*, Solution***);

void ComputePrimitives(PetscScalar, Solution*, Primitives*);
PetscErrorCode ComputeIFlux(FVNS*, PetscInt, PetscInt, PetscInt, Solution*, Solution*, Solution*);
PetscErrorCode ComputeVFlux(FVNS*, PetscInt, PetscInt, PetscInt, Solution*, Solution*, Solution*);

PetscErrorCode ProblemDefsCreate(const char*, const char*, ProblemType, BoundaryLocation, ProblemDefs**);
PetscErrorCode ProblemDefsDestroy(ProblemDefs**);
PetscErrorCode ProblemDefsView(ProblemDefs*, PetscViewer);

PetscErrorCode FVNSCreate(MPI_Comm, ProblemDefs*, FVNS**);
PetscErrorCode FVNSSetUp(FVNS*); 
PetscErrorCode FVNSSetFromOptions(FVNS*);
PetscErrorCode FVNSSetNumElements(FVNS*, PetscInt, PetscInt, PetscInt);
PetscErrorCode FVNSSetMeshSize(FVNS*, PetscReal, PetscReal, PetscReal);

PetscErrorCode FVNSDestroy(FVNS**);
PetscErrorCode FVNSComputeInitialCondition(FVNS*, Vec);
PetscErrorCode FVNSComputeRHS(FVNS*, Vec, Vec);
PetscErrorCode FVNSExtractCouplingVars(FVNS*, Vec QQ);
PetscErrorCode FVNSCheckSolution(FVNS*, PetscBool, PetscBool, Vec);
PetscErrorCode FVNSCheckBoundary(FVNS*, PetscBool, Vec);
PetscErrorCode FVNSView(FVNS*, PetscViewer);
PetscErrorCode FVNSOptionsView(FVNS*);
PetscErrorCode FVNSView_Solution_Point(FVNS*, PetscViewer, Solution);
PetscErrorCode FVNSView_Conservation(FVNS*, PetscViewer, Vec);
PetscErrorCode FVNSMonitorView_Conservation_CSV(TS,PetscInt,PetscReal,Vec,void*);
PetscErrorCode FVNSViewExnerPressureProfile(FVNS*);

PetscErrorCode FVNSWriteSolutionVTK(FVNS*, Vec, const char*, PetscInt);

PetscErrorCode FVNSIntegrate(FVNS*,Vec,Solution*);

/* Multirate Functions */
PetscErrorCode ApplyPhysicalBoundaryConditions_TOP(FVNS*, Solution***);
PetscErrorCode ApplyPhysicalBoundaryConditions_BOTTOM(FVNS*, Solution***);

PetscErrorCode ComputePrimitiveGradients_TOP(FVNS*, Solution***);
PetscErrorCode ComputePrimitiveGradients_BOTTOM(FVNS*, Solution***);

PetscErrorCode ReconstructSolutionOnFaces_TOP(FVNS*, Solution***);
PetscErrorCode ReconstructSolutionOnFaces_BOTTOM(FVNS*, Solution***);

PetscErrorCode FVNSComputeRHS_TOP(FVNS*, Vec, Vec);
PetscErrorCode FVNSComputeRHS_TOP_DEBUG(FVNS*, Vec, Vec);

PetscErrorCode FVNSComputeRHS_BOTTOM(FVNS*, Vec, Vec);

PetscErrorCode CreateZSplitIS(FVNS*,IS*,IS*);
PetscErrorCode CreateZSplitIS_DA(DM,PetscInt,IS*,IS*);