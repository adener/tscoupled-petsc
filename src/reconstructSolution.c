#include "fvns3d.h"

/* fix the coefficients for prev/next interpolation with macros */
#define InterpolateSolutionPrev(p,c,n,r) SolutionAXPBYPCZ(-0.25,p,1.0,c,0.25,n,r)
#define InterpolateSolutionNext(p,c,n,r) SolutionAXPBYPCZ(0.25,p,1.0,c,-0.25,n,r)
#define InterpolatePrimitivesPrev(p,c,n,r) PrimitivesAXPBYPCZ(-0.25,p,1.0,c,0.25,n,r)
#define InterpolatePrimitivesNext(p,c,n,r) PrimitivesAXPBYPCZ(0.25,p,1.0,c,-0.25,n,r)

/* IN:  pde, xi, yi, zi, qq */
/* OUT: qf, qp */
static PetscErrorCode ReconstructSolutionLocal(
  FVNS* pde, PetscInt xi, PetscInt yi, PetscInt zi, Solution*** qq,
  Solution* qf_L, Solution* qf_R, Solution* qf_F, Solution* qf_B, Solution* qf_D, Solution* qf_U,
  Primitives* pf_L, Primitives* pf_R, Primitives* pf_F, Primitives* pf_B, Primitives* pf_D, Primitives* pf_U)
{
  DMDALocalInfo info;
  ProblemDefs* defs = (ProblemDefs*)pde->defs;
  Primitives primPrev, primCenter, primNext;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = DMDAGetLocalInfo(pde->dm, &info);CHKERRQ(ierr);

  /*
    x-direction faces
                    l r
    --------|--------|--------|--------
      xi-2     xi-1  ^   xi      xi+1
  */
  if ((xi == 0) || (xi == info.mx)) { /* left or right physical boundary face */
    if (defs->xPeriodic || (defs->leftBC == DIRICHLET && xi == 0) || (defs->rightBC == DIRICHLET && xi == info.mx) ) {
      InterpolateSolutionPrev(&qq[zi][yi][xi-2], &qq[zi][yi][xi-1], &qq[zi][yi][xi], qf_L);
      ComputePrimitives(defs->gamma, &qq[zi][yi][xi-2], &primPrev);
      ComputePrimitives(defs->gamma, &qq[zi][yi][xi-1], &primCenter);
      ComputePrimitives(defs->gamma, &qq[zi][yi][xi],   &primNext);
      InterpolatePrimitivesPrev(&primPrev, &primCenter, &primNext, pf_L);
      /* compute right-side flux with (xi) as the "center" cell (same as interior) */
      InterpolateSolutionNext(&qq[zi][yi][xi-1], &qq[zi][yi][xi], &qq[zi][yi][xi+1], qf_R);
      PrimitivesCopy(&primCenter, &primPrev);
      PrimitivesCopy(&primNext, &primCenter);
      ComputePrimitives(defs->gamma, &qq[zi][yi][xi+1], &primNext);
      InterpolatePrimitivesNext(&primPrev, &primCenter, &primNext, pf_R);
    } else { /* coupling */
      /* left-side flux with boundary condition */
      SolutionCopy(&qq[zi][yi][xi-1], qf_L);
      ComputePrimitives(defs->gamma, &qq[zi][yi][xi-1], &primPrev);
      PrimitivesCopy(&primPrev, pf_L);
      /* right-side flux with boundary condition*/
      SolutionCopy(&qq[zi][yi][xi], qf_R);
      ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &primNext);
      PrimitivesCopy(&primNext, pf_R);
    }
  } else {  /* interior */
      /* compute left-side flux with (xi-1) as the "center" cell */
      InterpolateSolutionPrev(&qq[zi][yi][xi-2], &qq[zi][yi][xi-1], &qq[zi][yi][xi], qf_L);
      ComputePrimitives(defs->gamma, &qq[zi][yi][xi-2], &primPrev);
      ComputePrimitives(defs->gamma, &qq[zi][yi][xi-1], &primCenter);
      ComputePrimitives(defs->gamma, &qq[zi][yi][xi],   &primNext);
      InterpolatePrimitivesPrev(&primPrev, &primCenter, &primNext, pf_L);
      /* compute right-side flux with (xi) as the "center" cell */
      InterpolateSolutionNext(&qq[zi][yi][xi-1], &qq[zi][yi][xi], &qq[zi][yi][xi+1], qf_R);
      PrimitivesCopy(&primCenter, &primPrev);
      PrimitivesCopy(&primNext, &primCenter);
      ComputePrimitives(defs->gamma, &qq[zi][yi][xi+1], &primNext);
      InterpolatePrimitivesNext(&primPrev, &primCenter, &primNext, pf_R);
  }

  /*
    y-direction faces
                    f b
    --------|--------|--------|--------
      yi-2     yi-1  ^   yi      yi+1
  */
  if ((yi == 0) || (yi == info.my)) { /* front or back physical boundary face */
    if (defs->yPeriodic || (defs->frontBC == DIRICHLET && yi == 0) || (defs->backBC == DIRICHLET && yi == info.my)) {
      /* compute front-side flux with (yi-1) as the "center" cell (same as interior)*/
      InterpolateSolutionPrev(&qq[zi][yi-2][xi], &qq[zi][yi-1][xi], &qq[zi][yi][xi], qf_F);
      ComputePrimitives(defs->gamma, &qq[zi][yi-2][xi], &primPrev);
      ComputePrimitives(defs->gamma, &qq[zi][yi-1][xi], &primCenter);
      ComputePrimitives(defs->gamma, &qq[zi][yi][xi],   &primNext);
      InterpolatePrimitivesNext(&primPrev, &primCenter, &primNext, pf_F);
      /* compute back-side flux with (yi) as the "center" cell (same as interior) */
      InterpolateSolutionNext(&qq[zi][yi-1][xi], &qq[zi][yi][xi], &qq[zi][yi+1][xi], qf_B);
      PrimitivesCopy(&primCenter, &primPrev);
      PrimitivesCopy(&primNext, &primCenter);
      ComputePrimitives(defs->gamma, &qq[zi][yi+1][xi], &primNext);
      InterpolatePrimitivesNext(&primPrev, &primCenter, &primNext, pf_B);
    } else {
      /* front-side flux with boundary condition */
      SolutionCopy(&qq[zi][yi-1][xi], qf_F);
      ComputePrimitives(defs->gamma, &qq[zi][yi-1][xi], &primPrev);
      PrimitivesCopy(&primPrev, pf_F);
      /* back-side flux with boundary condition */
      SolutionCopy(&qq[zi][yi][xi], qf_B);
      ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &primNext);
      PrimitivesCopy(&primNext, pf_B);
    }
  } else {  /* interior */
    /* complete interior */
      /* compute front-side flux with (yi-1) as the "center" cell */
      InterpolateSolutionPrev(&qq[zi][yi-2][xi], &qq[zi][yi-1][xi], &qq[zi][yi][xi], qf_F);
      ComputePrimitives(defs->gamma, &qq[zi][yi-2][xi], &primPrev);
      ComputePrimitives(defs->gamma, &qq[zi][yi-1][xi], &primCenter);
      ComputePrimitives(defs->gamma, &qq[zi][yi][xi],   &primNext);
      InterpolatePrimitivesPrev(&primPrev, &primCenter, &primNext, pf_F);
      /* compute back-side flux with (yi) as the "center" cell */
      InterpolateSolutionNext(&qq[zi][yi-1][xi], &qq[zi][yi][xi], &qq[zi][yi+1][xi], qf_B);
      PrimitivesCopy(&primCenter, &primPrev);
      PrimitivesCopy(&primNext, &primCenter);
      ComputePrimitives(defs->gamma, &qq[zi][yi+1][xi], &primNext);
      InterpolatePrimitivesNext(&primPrev, &primCenter, &primNext, pf_B);
  }

  /*
    z-direction faces
                    d u
    --------|--------|--------|--------
      zi-2     zi-1  ^   zi      zi+1
  */
  if ((zi == 0) || (zi == info.mz)) { /* down/bottom or up/top physical boundary face */
    if (defs->zPeriodic || (defs->downBC == DIRICHLET && zi == 0) || (defs->upBC == DIRICHLET && zi == info.mz) ) {
      InterpolateSolutionPrev(&qq[zi-2][yi][xi], &qq[zi-1][yi][xi], &qq[zi][yi][xi], qf_D);
      ComputePrimitives(defs->gamma, &qq[zi-2][yi][xi], &primPrev);
      ComputePrimitives(defs->gamma, &qq[zi-1][yi][xi], &primCenter);
      ComputePrimitives(defs->gamma, &qq[zi][yi][xi],   &primNext);
      InterpolatePrimitivesPrev(&primPrev, &primCenter, &primNext, pf_D);
      /* compute up/top-side flux with (zi) as the "center" cell (same as interior) */
      InterpolateSolutionNext(&qq[zi-1][yi][xi], &qq[zi][yi][xi], &qq[zi+1][yi][xi], qf_U);
      PrimitivesCopy(&primCenter, &primPrev);
      PrimitivesCopy(&primNext, &primCenter);
      ComputePrimitives(defs->gamma, &qq[zi+1][yi][xi], &primNext);
      InterpolatePrimitivesNext(&primPrev, &primCenter, &primNext, pf_U);
    } else {
      /* down/bottom-side flux with boundary condition */
      SolutionCopy(&qq[zi-1][yi][xi], qf_D);
      ComputePrimitives(defs->gamma, &qq[zi-1][yi][xi], &primPrev);
      PrimitivesCopy(&primPrev, pf_D);
      /* up/top-side flux with boundary condition*/
      SolutionCopy(&qq[zi][yi][xi], qf_U);
      ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &primNext);
      PrimitivesCopy(&primNext, pf_U);
    }
  } else {  /* interior */
      /* compute down/bottom-side flux with (zi-1) as the "center" cell */
      InterpolateSolutionPrev(&qq[zi-2][yi][xi], &qq[zi-1][yi][xi], &qq[zi][yi][xi], qf_D);
      ComputePrimitives(defs->gamma, &qq[zi-2][yi][xi], &primPrev);
      ComputePrimitives(defs->gamma, &qq[zi-1][yi][xi], &primCenter);
      ComputePrimitives(defs->gamma, &qq[zi][yi][xi],   &primNext);
      InterpolatePrimitivesPrev(&primPrev, &primCenter, &primNext, pf_D);
      /* compute up/top-side flux with (zi) as the "center" cell */
      InterpolateSolutionNext(&qq[zi-1][yi][xi], &qq[zi][yi][xi], &qq[zi+1][yi][xi], qf_U);
      PrimitivesCopy(&primCenter, &primPrev);
      PrimitivesCopy(&primNext, &primCenter);
      ComputePrimitives(defs->gamma, &qq[zi+1][yi][xi], &primNext);
      InterpolatePrimitivesNext(&primPrev, &primCenter, &primNext, pf_U);
  }
  PetscFunctionReturn(0);
}

/* IN:  pde, qq */
/* OUT: qf, qp */
PetscErrorCode ReconstructSolutionOnFaces(FVNS* pde, Solution*** qq)
{
  DMDALocalInfo info;
  PetscInt xi, yi, zi, xstart, xend, ystart, yend, zstart, zend;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = DMDAGetLocalInfo(pde->dm, &info);CHKERRQ(ierr);
  xstart = info.xs;  xend = info.xs + info.xm;
  ystart = info.ys;  yend = info.ys + info.ym;
  zstart = info.zs;  zend = info.zs + info.zm;
  for (zi=zstart; zi<zend; zi++) {
    for (yi=ystart; yi<yend; yi++) {
      for (xi=xstart; xi<xend; xi++) {
        /* 
          fq and fp contains reconstructed Q for the left/right sides of the left-face 
          and the down/up sides of the front-face for each element we iterate
          */
        ierr = ReconstructSolutionLocal(
          pde, xi, yi, zi, qq, 
          &pde->qf_L[zi][yi][xi], &pde->qf_R[zi][yi][xi],
          &pde->qf_F[zi][yi][xi], &pde->qf_B[zi][yi][xi],
          &pde->qf_D[zi][yi][xi], &pde->qf_U[zi][yi][xi],
          &pde->pf_L[zi][yi][xi], &pde->pf_R[zi][yi][xi],
          &pde->pf_F[zi][yi][xi], &pde->pf_B[zi][yi][xi],
          &pde->pf_D[zi][yi][xi], &pde->pf_U[zi][yi][xi]);CHKERRQ(ierr);

        /* last element in the x-direction also has to do the first ghost cell on the right */
        if (xi == xend-1) {
          ierr = ReconstructSolutionLocal(
            pde, xi+1, yi, zi, qq, 
            &pde->qf_L[zi][yi][xi+1], &pde->qf_R[zi][yi][xi+1],
            &pde->qf_F[zi][yi][xi+1], &pde->qf_B[zi][yi][xi+1],
            &pde->qf_D[zi][yi][xi+1], &pde->qf_U[zi][yi][xi+1],
            &pde->pf_L[zi][yi][xi+1], &pde->pf_R[zi][yi][xi+1],
            &pde->pf_F[zi][yi][xi+1], &pde->pf_B[zi][yi][xi+1],
            &pde->pf_D[zi][yi][xi+1], &pde->pf_U[zi][yi][xi+1]);CHKERRQ(ierr);
        }

        /* last element in the y-direction also has to do the first ghost cell in the back */
        if (yi == yend-1) {
          ierr = ReconstructSolutionLocal(
            pde, xi, yi+1, zi, qq, 
            &pde->qf_L[zi][yi+1][xi], &pde->qf_R[zi][yi+1][xi],
            &pde->qf_F[zi][yi+1][xi], &pde->qf_B[zi][yi+1][xi],
            &pde->qf_D[zi][yi+1][xi], &pde->qf_U[zi][yi+1][xi],
            &pde->pf_L[zi][yi+1][xi], &pde->pf_R[zi][yi+1][xi],
            &pde->pf_F[zi][yi+1][xi], &pde->pf_B[zi][yi+1][xi],
            &pde->pf_D[zi][yi+1][xi], &pde->pf_U[zi][yi+1][xi]);CHKERRQ(ierr);
        }

        /* last element in the z-direction also has to do the first ghost cell on the top */
        if (zi == zend-1) {
          ierr = ReconstructSolutionLocal(
            pde, xi, yi, zi+1, qq, 
            &pde->qf_L[zi+1][yi][xi], &pde->qf_R[zi+1][yi][xi],
            &pde->qf_F[zi+1][yi][xi], &pde->qf_B[zi+1][yi][xi],
            &pde->qf_D[zi+1][yi][xi], &pde->qf_U[zi+1][yi][xi],
            &pde->pf_L[zi+1][yi][xi], &pde->pf_R[zi+1][yi][xi],
            &pde->pf_F[zi+1][yi][xi], &pde->pf_B[zi+1][yi][xi],
            &pde->pf_D[zi+1][yi][xi], &pde->pf_U[zi+1][yi][xi]);CHKERRQ(ierr);
        }
      }
    }
  }
  PetscFunctionReturn(0);
}

/* Only Computer for z>buffer */
/* IN:  pde, qq */
/* OUT: qf, qp */
PetscErrorCode ReconstructSolutionOnFaces_BOTTOM(FVNS* pde, Solution*** qq)
{
  DMDALocalInfo info;
  PetscInt xi, yi, zi, xstart, xend, ystart, yend, zstart, zend;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = DMDAGetLocalInfo(pde->dm, &info);CHKERRQ(ierr);
  xstart = info.xs;  xend = info.xs + info.xm;
  ystart = info.ys;  yend = info.ys + info.ym;
  zstart = info.zs; 
  zend   = (pde->multictx.bufferz+1 < info.zs+info.zm) ? pde->multictx.bufferz+1 : info.zs+info.zm;
  for (zi=zstart; zi<zend; zi++) {
    for (yi=ystart; yi<yend; yi++) {
      for (xi=xstart; xi<xend; xi++) {
        /* 
          fq and fp contains reconstructed Q for the left/right sides of the left-face 
          and the down/up sides of the front-face for each element we iterate
          */
        ierr = ReconstructSolutionLocal(
          pde, xi, yi, zi, qq, 
          &pde->qf_L[zi][yi][xi], &pde->qf_R[zi][yi][xi],
          &pde->qf_F[zi][yi][xi], &pde->qf_B[zi][yi][xi],
          &pde->qf_D[zi][yi][xi], &pde->qf_U[zi][yi][xi],
          &pde->pf_L[zi][yi][xi], &pde->pf_R[zi][yi][xi],
          &pde->pf_F[zi][yi][xi], &pde->pf_B[zi][yi][xi],
          &pde->pf_D[zi][yi][xi], &pde->pf_U[zi][yi][xi]);CHKERRQ(ierr);

        /* last element in the x-direction also has to do the first ghost cell on the right */
        if (xi == xend-1) {
          ierr = ReconstructSolutionLocal(
            pde, xi+1, yi, zi, qq, 
            &pde->qf_L[zi][yi][xi+1], &pde->qf_R[zi][yi][xi+1],
            &pde->qf_F[zi][yi][xi+1], &pde->qf_B[zi][yi][xi+1],
            &pde->qf_D[zi][yi][xi+1], &pde->qf_U[zi][yi][xi+1],
            &pde->pf_L[zi][yi][xi+1], &pde->pf_R[zi][yi][xi+1],
            &pde->pf_F[zi][yi][xi+1], &pde->pf_B[zi][yi][xi+1],
            &pde->pf_D[zi][yi][xi+1], &pde->pf_U[zi][yi][xi+1]);CHKERRQ(ierr);
        }

        /* last element in the y-direction also has to do the first ghost cell in the back */
        if (yi == yend-1) {
          ierr = ReconstructSolutionLocal(
            pde, xi, yi+1, zi, qq, 
            &pde->qf_L[zi][yi+1][xi], &pde->qf_R[zi][yi+1][xi],
            &pde->qf_F[zi][yi+1][xi], &pde->qf_B[zi][yi+1][xi],
            &pde->qf_D[zi][yi+1][xi], &pde->qf_U[zi][yi+1][xi],
            &pde->pf_L[zi][yi+1][xi], &pde->pf_R[zi][yi+1][xi],
            &pde->pf_F[zi][yi+1][xi], &pde->pf_B[zi][yi+1][xi],
            &pde->pf_D[zi][yi+1][xi], &pde->pf_U[zi][yi+1][xi]);CHKERRQ(ierr);
        }

        /* last element in the z-direction also has to do the first ghost cell on the top */
        if (zi == zend-1) {
          ierr = ReconstructSolutionLocal(
            pde, xi, yi, zi+1, qq, 
            &pde->qf_L[zi+1][yi][xi], &pde->qf_R[zi+1][yi][xi],
            &pde->qf_F[zi+1][yi][xi], &pde->qf_B[zi+1][yi][xi],
            &pde->qf_D[zi+1][yi][xi], &pde->qf_U[zi+1][yi][xi],
            &pde->pf_L[zi+1][yi][xi], &pde->pf_R[zi+1][yi][xi],
            &pde->pf_F[zi+1][yi][xi], &pde->pf_B[zi+1][yi][xi],
            &pde->pf_D[zi+1][yi][xi], &pde->pf_U[zi+1][yi][xi]);CHKERRQ(ierr);
        }
      }
    }
  }
  PetscFunctionReturn(0);
}

/* IN:  pde, qq */
/* OUT: qf, qp */
PetscErrorCode ReconstructSolutionOnFaces_TOP(FVNS* pde, Solution*** qq)
{
  DMDALocalInfo info;
  PetscInt xi, yi, zi, xstart, xend, ystart, yend, zstart, zend;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = DMDAGetLocalInfo(pde->dm, &info);CHKERRQ(ierr);
  xstart = info.xs;  xend = info.xs + info.xm;
  ystart = info.ys;  yend = info.ys + info.ym;
  zstart = (pde->multictx.bufferz >= info.zs) ? pde->multictx.bufferz+1 : info.zs;
  zend = info.zs + info.zm;
  for (zi=zstart; zi<zend; zi++) {
    for (yi=ystart; yi<yend; yi++) {
      for (xi=xstart; xi<xend; xi++) {
        /* 
          fq and fp contains reconstructed Q for the left/right sides of the left-face 
          and the down/up sides of the front-face for each element we iterate
          */
        ierr = ReconstructSolutionLocal(
          pde, xi, yi, zi, qq, 
          &pde->qf_L[zi][yi][xi], &pde->qf_R[zi][yi][xi],
          &pde->qf_F[zi][yi][xi], &pde->qf_B[zi][yi][xi],
          &pde->qf_D[zi][yi][xi], &pde->qf_U[zi][yi][xi],
          &pde->pf_L[zi][yi][xi], &pde->pf_R[zi][yi][xi],
          &pde->pf_F[zi][yi][xi], &pde->pf_B[zi][yi][xi],
          &pde->pf_D[zi][yi][xi], &pde->pf_U[zi][yi][xi]);CHKERRQ(ierr);

        /* last element in the x-direction also has to do the first ghost cell on the right */
        if (xi == xend-1) {
          ierr = ReconstructSolutionLocal(
            pde, xi+1, yi, zi, qq, 
            &pde->qf_L[zi][yi][xi+1], &pde->qf_R[zi][yi][xi+1],
            &pde->qf_F[zi][yi][xi+1], &pde->qf_B[zi][yi][xi+1],
            &pde->qf_D[zi][yi][xi+1], &pde->qf_U[zi][yi][xi+1],
            &pde->pf_L[zi][yi][xi+1], &pde->pf_R[zi][yi][xi+1],
            &pde->pf_F[zi][yi][xi+1], &pde->pf_B[zi][yi][xi+1],
            &pde->pf_D[zi][yi][xi+1], &pde->pf_U[zi][yi][xi+1]);CHKERRQ(ierr);
        }

        /* last element in the y-direction also has to do the first ghost cell in the back */
        if (yi == yend-1) {
          ierr = ReconstructSolutionLocal(
            pde, xi, yi+1, zi, qq, 
            &pde->qf_L[zi][yi+1][xi], &pde->qf_R[zi][yi+1][xi],
            &pde->qf_F[zi][yi+1][xi], &pde->qf_B[zi][yi+1][xi],
            &pde->qf_D[zi][yi+1][xi], &pde->qf_U[zi][yi+1][xi],
            &pde->pf_L[zi][yi+1][xi], &pde->pf_R[zi][yi+1][xi],
            &pde->pf_F[zi][yi+1][xi], &pde->pf_B[zi][yi+1][xi],
            &pde->pf_D[zi][yi+1][xi], &pde->pf_U[zi][yi+1][xi]);CHKERRQ(ierr);
        }

        /* last element in the z-direction also has to do the first ghost cell on the top */
        if (zi == zend-1) {
          ierr = ReconstructSolutionLocal(
            pde, xi, yi, zi+1, qq, 
            &pde->qf_L[zi+1][yi][xi], &pde->qf_R[zi+1][yi][xi],
            &pde->qf_F[zi+1][yi][xi], &pde->qf_B[zi+1][yi][xi],
            &pde->qf_D[zi+1][yi][xi], &pde->qf_U[zi+1][yi][xi],
            &pde->pf_L[zi+1][yi][xi], &pde->pf_R[zi+1][yi][xi],
            &pde->pf_F[zi+1][yi][xi], &pde->pf_B[zi+1][yi][xi],
            &pde->pf_D[zi+1][yi][xi], &pde->pf_U[zi+1][yi][xi]);CHKERRQ(ierr);
        }
      }
    }
  }
  PetscFunctionReturn(0);
}