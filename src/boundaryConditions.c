#include "fvns3d.h"

static void NoSlipAdiabaticKernel(
  PetscScalar rho, PetscScalar rhoE, PetscScalar u, PetscScalar v, PetscScalar w,
  PetscScalar uw, PetscScalar vw, PetscScalar ww, Solution* bc)
{
  bc->rho = rho;
  bc->rhoU = rho*uw;
  bc->rhoV = rho*vw;
  bc->rhoW = rho*ww;
  bc->rhoE = rhoE-.5*rho*(u*u+v*v+w*w)+0.5*rho*(vw*vw+uw*uw+ww*ww);
}

static void NoSlipEnergyAdjustment(PetscScalar rho,
   PetscScalar u, PetscScalar v, PetscScalar w, Solution* bc)
{
  bc->rhoE = bc->rhoE-.5*rho*(u*u+v*v+w*w)+0.5*(PetscSqr(bc->rhoW)+PetscSqr(bc->rhoU)+PetscSqr(bc->rhoV))/bc->rho;
}

static void NoSlipAdiabaticKernel_better( PetscScalar rho, PetscScalar rhoE, PetscScalar u, PetscScalar v, PetscScalar w,
  PetscScalar uw, PetscScalar vw, PetscScalar ww, PetscReal nx, PetscReal ny, PetscReal nz, PetscScalar gamma, Solution* bc)
{
  PetscScalar un, pres, a, ri1, ri2, ri3, a_star, p_star; 
  
  un = nx*u+ ny*v+nz*w; 
  pres = (gamma-1.) * (rhoE - 0.5*rho*(u*u+v*v+w+w));
  a = PetscSqrtScalar(gamma*pres/rho);

  ri1 = -un -2*a/(gamma-1); 
  ri2 = pres/PetscPowScalar(rho,gamma);
  ri3 = un+ 2*a/(gamma-1);

  a_star = (gamma-1)/4*(ri3-ri1); 
  bc->rho = PetscPowScalar(PetscSqr(a_star)/(gamma*ri2),(1./(gamma-1.)));
  p_star = ri2*PetscPowScalar(bc->rho,gamma);

  bc->rhoU = bc->rho*uw; 
  bc->rhoV = bc->rho*vw; 
  bc->rhoW = bc->rho*ww;

  bc->rhoE = p_star/(gamma-1) + 0.5*bc->rho*(uw*uw+vw*vw+ww*ww);
}

static PetscErrorCode ApplyPhysicalBCKernel(
  FVNS* pde, BoundaryType bcType, PetscScalar uD, PetscScalar vD, PetscScalar wD, PetscReal nx, PetscReal ny,
   PetscReal nz, Solution* qq1, Solution* qq2, Solution* cbc, Solution* bc1, Solution* bc2)
{
  ProblemDefs *defs = pde->defs;
  PetscScalar u, v, w, uw, vw, ww;

  PetscFunctionBeginUser;
  if (bcType == PERIODIC) PetscFunctionReturn(0);
  u = qq1->rhoU / qq1->rho;
  v = qq1->rhoV / qq1->rho;
  w = qq1->rhoW / qq1->rho;
  uw = vw = ww = 0.0;
  switch (bcType) {
    case DIRICHLET:
      /* Fill in ghost cells, these are chosen such that the interpolation techniques fix u,v,w = uw,vw,ww, 
      the wall velocities. This allows for the same exact reconstruction/flux evaluation at the boundary 
      as in the interior */

      /* -1 ghost cell. */

      bc1->rho  = qq1->rho; 
      bc1->rhoE = qq1->rhoE; 

      bc1->rhoV = 4*vw*qq1->rho - 4*qq1->rhoV + qq2->rhoV;
      bc1->rhoU = 4*uw*qq1->rho - 4*qq1->rhoU + qq2->rhoU;
      bc1->rhoW = 4*ww*qq1->rho - 4*qq1->rhoW + qq2->rhoW;

      NoSlipEnergyAdjustment(qq1->rho,u,v,w,bc1);

      /* -2 ghost cell */
      u = qq2->rhoU / qq2->rho;
      v = qq2->rhoV / qq2->rho;
      w = qq2->rhoW / qq2->rho;
      bc2->rho  = qq2->rho; 
      bc2->rhoE = qq2->rhoE; 

      bc2->rhoV = qq1->rhoV - 4*vw*qq2->rho+4*bc1->rhoV;
      bc2->rhoU = qq1->rhoU - 4*uw*qq2->rho+4*bc1->rhoU;
      bc2->rhoW = qq1->rhoW - 4*ww*qq2->rho+4*bc1->rhoW;
      NoSlipEnergyAdjustment(qq1->rho,u,v,w,bc2);
      break;

    case COUPLING:
      if (defs->couplingSide == LEFT || defs->couplingSide == RIGHT) {
        /* left/right coupling will connect only the y- and z-velocities */
        uw = -u;
        vw = cbc->rhoV / cbc->rho;
        ww = cbc->rhoW / cbc->rho;
      } else if (defs->couplingSide == FRONT || defs->couplingSide == BACK) {
        /* front/back coupling will connect only the x- and z-velocities */
        uw = cbc->rhoU / cbc->rho;
        vw = -v;
        ww = cbc->rhoW / cbc->rho;
      } else if (defs->couplingSide == DOWN || defs->couplingSide == UP) {
        /* top/bottom coupling will connect only the x- and y- terms */
        uw = cbc->rhoU / cbc->rho;
        vw = cbc->rhoV / cbc->rho;
        ww = -w;
      }
      NoSlipAdiabaticKernel(qq1->rho, qq1->rhoE, u, v, w, uw, vw, ww, bc1);
      break;
    
    default:
      SETERRQ(pde->comm, PETSC_ERR_ARG_INCOMP, "Wrong/unknown boundary type detected");
      break;
  }
  PetscFunctionReturn(0);
}

/* IN:  pde, qq */
/* OUT: qq (modified in-place) */
PetscErrorCode ApplyPhysicalBoundaryConditions(FVNS* pde, Solution*** qq)
{
  DMDALocalInfo info;
  ProblemDefs* defs = (ProblemDefs*)pde->defs;
  Solution **cbc, *cbc_loc;
  PetscInt xi, yi, zi, xstart, ystart, zstart, xend, yend, zend;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = DMDAGetLocalInfo(pde->dm, &info);CHKERRQ(ierr);
  xstart = info.xs;  xend = info.xs + info.xm;
  ystart = info.ys;  yend = info.ys + info.ym;
  zstart = info.zs;  zend = info.zs + info.zm;
  if (defs->couplingSide != NONE) {
    ierr = VecGetArray2d(
      pde->couplingOther, pde->jm, pde->im*info.dof, pde->js, pde->is*info.dof,
      (PetscScalar***)&cbc);CHKERRQ(ierr);
  }
  if (info.xs == 0) {  /* process owns left physical boundary */
    for (zi=zstart; zi<zend; zi++) {
      for (yi=ystart; yi<yend; yi++) {
        if (defs->couplingSide == LEFT) cbc_loc = &cbc[zi][yi];
        else cbc_loc = NULL;
        ierr = ApplyPhysicalBCKernel(
          pde, defs->leftBC, defs->ud_left, defs->vd_left, defs->wd_left, -1, 0, 0,
          &qq[zi][yi][0],&qq[zi][yi][1], cbc_loc, &qq[zi][yi][-1],&qq[zi][yi][-2]);CHKERRQ(ierr);
      }
    }
  }
  if (info.xs+info.xm == info.mx) {  /* process owns right physical boundary */
    for (zi=zstart; zi<zend; zi++) {
      for (yi=ystart; yi<yend; yi++) {
        if (defs->couplingSide == RIGHT) cbc_loc = &cbc[zi][yi];
        else cbc_loc = NULL;
        ierr = ApplyPhysicalBCKernel(
          pde, defs->rightBC, defs->ud_right, defs->vd_right, defs->wd_right, 1, 0, 0,
          &qq[zi][yi][info.mx-1],&qq[zi][yi][info.mx-2], cbc_loc, &qq[zi][yi][info.mx],&qq[zi][yi][info.mx+1]);CHKERRQ(ierr);
      }
    }
  }
  if (info.ys == 0) {  /* process owns front physical boundary */
    for (zi=zstart; zi<zend; zi++) {  
      for (xi=xstart; xi<xend; xi++) {
        if (defs->couplingSide == FRONT) cbc_loc = &cbc[zi][xi];
        else cbc_loc = NULL;
        ierr = ApplyPhysicalBCKernel(
          pde, defs->frontBC, defs->ud_front, defs->vd_front, defs->wd_front, 0, -1, 0, 
          &qq[zi][0][xi],&qq[zi][1][xi],cbc_loc, &qq[zi][-1][xi],&qq[zi][-2][xi]);CHKERRQ(ierr);
      }
    }
  }
  if (info.ys+info.ym == info.my) {  /* process owns back physical boundary */
    for (zi=zstart; zi<zend; zi++) {
      for (xi=xstart; xi<xend; xi++) {
        if (defs->couplingSide == BACK) cbc_loc = &cbc[zi][xi];
        else cbc_loc = NULL;
        ierr = ApplyPhysicalBCKernel(
          pde, defs->backBC, defs->ud_back, defs->vd_back, defs->wd_back, 0, 1, 0,
          &qq[zi][info.my-1][xi],&qq[zi][info.my-2][xi], cbc_loc, &qq[zi][info.my][xi],&qq[zi][info.my+1][xi]);CHKERRQ(ierr);
      }
    }
  }
  if (info.zs == 0) {  /* process owns down/bottom physical boundary */
    for (yi=ystart; yi<yend; yi++) {
      for (xi=xstart; xi<xend; xi++) {
        if (defs->couplingSide == DOWN) cbc_loc = &cbc[yi][xi];
        else cbc_loc = NULL;
        ierr = ApplyPhysicalBCKernel(
          pde, defs->downBC, defs->ud_down, defs->vd_down, defs->wd_down, 0, 0, -1,
          &qq[0][yi][xi],&qq[1][yi][xi], cbc_loc, &qq[-1][yi][xi],&qq[-2][yi][xi]);CHKERRQ(ierr);
      }
    }
  }
  if (info.zs+info.zm == info.mz) {  /* process owns up/top physical boundary */
    for (yi=ystart; yi<yend; yi++) {
      for (xi=xstart; xi<xend; xi++) {
        if (defs->couplingSide == UP) cbc_loc = &cbc[yi][xi];
        else cbc_loc = NULL;
        ierr = ApplyPhysicalBCKernel(
          pde, defs->upBC, defs->ud_up, defs->vd_up, defs->wd_up, 0, 0, 1,
          &qq[info.mz-1][yi][xi],&qq[info.mz-2][yi][xi], cbc_loc, &qq[info.mz][yi][xi],&qq[info.mz+1][yi][xi]);CHKERRQ(ierr);
      }
    }
  }
  if (defs->couplingSide != NONE) {
    ierr = VecRestoreArray2d(
      pde->couplingOther, pde->jm, pde->im*info.dof, pde->js, pde->is*info.dof,
      (PetscScalar***)&cbc);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

/* Only compute for z<=bufferz */
/* IN:  pde, qq */
/* OUT: qq (modified in-place) */
PetscErrorCode ApplyPhysicalBoundaryConditions_BOTTOM(FVNS* pde, Solution*** qq)
{
  DMDALocalInfo info;
  ProblemDefs* defs = (ProblemDefs*)pde->defs;
  Solution **cbc, *cbc_loc;
  PetscInt xi, yi, zi, xstart, ystart, zstart, xend, yend, zend;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = DMDAGetLocalInfo(pde->dm, &info);CHKERRQ(ierr);
  xstart = info.xs;  xend = info.xs + info.xm;
  ystart = info.ys;  yend = info.ys + info.ym;
  zstart = info.zs; 
  zend   = (pde->multictx.bufferz+1 < info.zs+info.zm) ? pde->multictx.bufferz+1 : info.zs+info.zm;
   
   if (defs->couplingSide != NONE) {
    ierr = VecGetArray2d(
      pde->couplingOther, pde->jm, pde->im*info.dof, pde->js, pde->is*info.dof,
      (PetscScalar***)&cbc);CHKERRQ(ierr);
  }
  if (info.xs == 0) {  /* process owns left physical boundary */
    for (zi=zstart; zi<zend; zi++) {
      for (yi=ystart; yi<yend; yi++) {
        if (defs->couplingSide == LEFT) cbc_loc = &cbc[zi][yi];
        else cbc_loc = NULL;
        ierr = ApplyPhysicalBCKernel(
          pde, defs->leftBC, defs->ud_left, defs->vd_left, defs->wd_left, -1, 0, 0,
          &qq[zi][yi][0],&qq[zi][yi][1], cbc_loc, &qq[zi][yi][-1],&qq[zi][yi][-2]);CHKERRQ(ierr);
      }
    }
  }
  if (info.xs+info.xm == info.mx) {  /* process owns right physical boundary */
    for (zi=zstart; zi<zend; zi++) {
      for (yi=ystart; yi<yend; yi++) {
        if (defs->couplingSide == RIGHT) cbc_loc = &cbc[zi][yi];
        else cbc_loc = NULL;
        ierr = ApplyPhysicalBCKernel(
          pde, defs->rightBC, defs->ud_right, defs->vd_right, defs->wd_right, 1, 0, 0,
          &qq[zi][yi][info.mx-1],&qq[zi][yi][info.mx-2], cbc_loc, &qq[zi][yi][info.mx],&qq[zi][yi][info.mx+1]);CHKERRQ(ierr);
      }
    }
  }
  if (info.ys == 0) {  /* process owns front physical boundary */
    for (zi=zstart; zi<zend; zi++) {  
      for (xi=xstart; xi<xend; xi++) {
        if (defs->couplingSide == FRONT) cbc_loc = &cbc[zi][xi];
        else cbc_loc = NULL;
        ierr = ApplyPhysicalBCKernel(
          pde, defs->frontBC, defs->ud_front, defs->vd_front, defs->wd_front, 0, -1, 0, 
          &qq[zi][0][xi],&qq[zi][1][xi],cbc_loc, &qq[zi][-1][xi],&qq[zi][-2][xi]);CHKERRQ(ierr);
      }
    }
  }
  if (info.ys+info.ym == info.my) {  /* process owns back physical boundary */
    for (zi=zstart; zi<zend; zi++) {
      for (xi=xstart; xi<xend; xi++) {
        if (defs->couplingSide == BACK) cbc_loc = &cbc[zi][xi];
        else cbc_loc = NULL;
        ierr = ApplyPhysicalBCKernel(
          pde, defs->backBC, defs->ud_back, defs->vd_back, defs->wd_back, 0, 1, 0,
          &qq[zi][info.my-1][xi],&qq[zi][info.my-2][xi], cbc_loc, &qq[zi][info.my][xi],&qq[zi][info.my+1][xi]);CHKERRQ(ierr);
      }
    }
  }
  if (info.zs == 0) {  /* process owns down/bottom physical boundary */
    for (yi=ystart; yi<yend; yi++) {
      for (xi=xstart; xi<xend; xi++) {
        if (defs->couplingSide == DOWN) cbc_loc = &cbc[yi][xi];
        else cbc_loc = NULL;
        ierr = ApplyPhysicalBCKernel(
          pde, defs->downBC, defs->ud_down, defs->vd_down, defs->wd_down, 0, 0, -1,
          &qq[0][yi][xi],&qq[1][yi][xi], cbc_loc, &qq[-1][yi][xi],&qq[-2][yi][xi]);CHKERRQ(ierr);
      }
    }
  }
  if (info.zs+info.zm == info.mz) {  /* process owns up/top physical boundary */
    for (yi=ystart; yi<yend; yi++) {
      for (xi=xstart; xi<xend; xi++) {
        if (defs->couplingSide == UP) cbc_loc = &cbc[yi][xi];
        else cbc_loc = NULL;
        ierr = ApplyPhysicalBCKernel(
          pde, defs->upBC, defs->ud_up, defs->vd_up, defs->wd_up, 0, 0, 1,
          &qq[info.mz-1][yi][xi],&qq[info.mz-2][yi][xi], cbc_loc, &qq[info.mz][yi][xi],&qq[info.mz+1][yi][xi]);CHKERRQ(ierr);
      }
    }
  }
  if (defs->couplingSide != NONE) {
    ierr = VecRestoreArray2d(
      pde->couplingOther, pde->jm, pde->im*info.dof, pde->js, pde->is*info.dof,
      (PetscScalar***)&cbc);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

/* Only compute for z>bufferz */
/* IN:  pde, qq */
/* OUT: qq (modified in-place) */
PetscErrorCode ApplyPhysicalBoundaryConditions_TOP(FVNS* pde, Solution*** qq)
{
  DMDALocalInfo info;
  ProblemDefs* defs = (ProblemDefs*)pde->defs;
  Solution **cbc, *cbc_loc;
  PetscInt xi, yi, zi, xstart, ystart, zstart, xend, yend, zend;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = DMDAGetLocalInfo(pde->dm, &info);CHKERRQ(ierr);
  xstart = info.xs;  xend = info.xs + info.xm;
  ystart = info.ys;  yend = info.ys + info.ym;
  zstart = (pde->multictx.bufferz >= info.zs) ? pde->multictx.bufferz+1 : info.zs;
  zend   = info.zs+info.zm;
 
  if (defs->couplingSide != NONE) {
    ierr = VecGetArray2d(
      pde->couplingOther, pde->jm, pde->im*info.dof, pde->js, pde->is*info.dof,
      (PetscScalar***)&cbc);CHKERRQ(ierr);
  }
  if (info.xs == 0) {  /* process owns left physical boundary */
    for (zi=zstart; zi<zend; zi++) {
      for (yi=ystart; yi<yend; yi++) {
        if (defs->couplingSide == LEFT) cbc_loc = &cbc[zi][yi];
        else cbc_loc = NULL;
        ierr = ApplyPhysicalBCKernel(
          pde, defs->leftBC, defs->ud_left, defs->vd_left, defs->wd_left, -1, 0, 0,
          &qq[zi][yi][0],&qq[zi][yi][1], cbc_loc, &qq[zi][yi][-1],&qq[zi][yi][-2]);CHKERRQ(ierr);
      }
    }
  }
  if (info.xs+info.xm == info.mx) {  /* process owns right physical boundary */
    for (zi=zstart; zi<zend; zi++) {
      for (yi=ystart; yi<yend; yi++) {
        if (defs->couplingSide == RIGHT) cbc_loc = &cbc[zi][yi];
        else cbc_loc = NULL;
        ierr = ApplyPhysicalBCKernel(
          pde, defs->rightBC, defs->ud_right, defs->vd_right, defs->wd_right, 1, 0, 0,
          &qq[zi][yi][info.mx-1],&qq[zi][yi][info.mx-2], cbc_loc, &qq[zi][yi][info.mx],&qq[zi][yi][info.mx+1]);CHKERRQ(ierr);
      }
    }
  }
  if (info.ys == 0) {  /* process owns front physical boundary */
    for (zi=zstart; zi<zend; zi++) {  
      for (xi=xstart; xi<xend; xi++) {
        if (defs->couplingSide == FRONT) cbc_loc = &cbc[zi][xi];
        else cbc_loc = NULL;
        ierr = ApplyPhysicalBCKernel(
          pde, defs->frontBC, defs->ud_front, defs->vd_front, defs->wd_front, 0, -1, 0, 
          &qq[zi][0][xi],&qq[zi][1][xi],cbc_loc, &qq[zi][-1][xi],&qq[zi][-2][xi]);CHKERRQ(ierr);
      }
    }
  }
  if (info.ys+info.ym == info.my) {  /* process owns back physical boundary */
    for (zi=zstart; zi<zend; zi++) {
      for (xi=xstart; xi<xend; xi++) {
        if (defs->couplingSide == BACK) cbc_loc = &cbc[zi][xi];
        else cbc_loc = NULL;
        ierr = ApplyPhysicalBCKernel(
          pde, defs->backBC, defs->ud_back, defs->vd_back, defs->wd_back, 0, 1, 0,
          &qq[zi][info.my-1][xi],&qq[zi][info.my-2][xi], cbc_loc, &qq[zi][info.my][xi],&qq[zi][info.my+1][xi]);CHKERRQ(ierr);
      }
    }
  }
  if (info.zs == 0) {  /* process owns down/bottom physical boundary */
    for (yi=ystart; yi<yend; yi++) {
      for (xi=xstart; xi<xend; xi++) {
        if (defs->couplingSide == DOWN) cbc_loc = &cbc[yi][xi];
        else cbc_loc = NULL;
        ierr = ApplyPhysicalBCKernel(
          pde, defs->downBC, defs->ud_down, defs->vd_down, defs->wd_down, 0, 0, -1,
          &qq[0][yi][xi],&qq[1][yi][xi], cbc_loc, &qq[-1][yi][xi],&qq[-2][yi][xi]);CHKERRQ(ierr);
      }
    }
  }
  if (info.zs+info.zm == info.mz) {  /* process owns up/top physical boundary */
    for (yi=ystart; yi<yend; yi++) {
      for (xi=xstart; xi<xend; xi++) {
        if (defs->couplingSide == UP) cbc_loc = &cbc[yi][xi];
        else cbc_loc = NULL;
        ierr = ApplyPhysicalBCKernel(
          pde, defs->upBC, defs->ud_up, defs->vd_up, defs->wd_up, 0, 0, 1,
          &qq[info.mz-1][yi][xi],&qq[info.mz-2][yi][xi], cbc_loc, &qq[info.mz][yi][xi],&qq[info.mz+1][yi][xi]);CHKERRQ(ierr);
      }
    }
  }
  if (defs->couplingSide != NONE) {
    ierr = VecRestoreArray2d(
      pde->couplingOther, pde->jm, pde->im*info.dof, pde->js, pde->is*info.dof,
      (PetscScalar***)&cbc);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}