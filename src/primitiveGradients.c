#include "fvns3d.h"

#define ComputeGradient_Local(d,p,n,g) PrimitivesAXPBY(0.5/d,n,-0.5/d,p,g)

/* IN: pde, qq */
/* OUT: xgp, ygp */
PetscErrorCode ComputePrimitiveGradients(FVNS* pde, Solution*** qq)
{
  DMDALocalInfo info;
  ProblemDefs* defs = (ProblemDefs*)pde->defs;
  Primitives left, right, front, back, down, up;
  PetscInt xi, yi, zi, xstart, ystart, zstart, xend, yend, zend;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  /* loop over the interior of the domain and use central difference for gradients */
  ierr = DMDAGetLocalInfo(pde->dm, &info);CHKERRQ(ierr);
  xstart = info.xs;  xend = info.xs + info.xm;
  ystart = info.ys;  yend = info.ys + info.ym;
  zstart = info.zs;  zend = info.zs + info.zm;
  for (zi=zstart; zi<zend; zi++) {
    for (yi=ystart; yi<yend; yi++) {
      for (xi=xstart; xi<xend; xi++) {
        /* do x-direction gradients first for left and right faces */
        ComputePrimitives(defs->gamma, &qq[zi][yi][xi-1], &left);
        ComputePrimitives(defs->gamma, &qq[zi][yi][xi+1], &right);
        ComputeGradient_Local(pde->dx, &left, &right, &pde->xg[zi][yi][xi]);

        /* do the y-direction for front and back faces */
        ComputePrimitives(defs->gamma, &qq[zi][yi-1][xi], &front);
        ComputePrimitives(defs->gamma, &qq[zi][yi+1][xi], &back);
        ComputeGradient_Local(pde->dy, &front, &back, &pde->yg[zi][yi][xi]);

        /* now do the z-direction for down/bottom and up/top faces */
        ComputePrimitives(defs->gamma, &qq[zi-1][yi][xi], &down);
        ComputePrimitives(defs->gamma, &qq[zi+1][yi][xi], &up);
        ComputeGradient_Local(pde->dz, &down, &up, &pde->zg[zi][yi][xi]);
        
        /* x-direction ghost cells */
        if (xi == 0) {  /* left physical boundary */
          if (defs->leftBC != COUPLING) {
            /* periodic ghost is computed using the 2nd ghost */
            ComputePrimitives(defs->gamma, &qq[zi][yi][xi-2], &left);
            ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &right);
            ComputeGradient_Local(pde->dx, &left, &right, &pde->xg[zi][yi][xi-1]);
          } else {
            /* non-periodic ghost has a constant extrapolation */
            PrimitivesCopy(&pde->xg[zi][yi][xi], &pde->xg[zi][yi][xi-1]);
          }
        } else if (xi == info.mx-1) {  /* right physical boundary */
          if (defs->rightBC != COUPLING) {
            /* periodic ghost is computed using the 2nd ghost */
            ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &left);
            ComputePrimitives(defs->gamma, &qq[zi][yi][xi+2], &right);
            ComputeGradient_Local(pde->dx, &left, &right, &pde->xg[zi][yi][xi+1]);
          } else {
            /* non-periodic ghost has a constant extrapolation */
            PrimitivesCopy(&pde->xg[zi][yi][xi], &pde->xg[zi][yi][xi+1]);
          }
        } else if (xi == xstart) {  /* parallel boundary (left) */
          ComputePrimitives(defs->gamma, &qq[zi][yi][xi-2], &left);
          ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &right);
          ComputeGradient_Local(pde->dx, &left, &right, &pde->xg[zi][yi][xi-1]);
          PrimitivesCopy(&pde->yg[zi][yi][xi],   &pde->yg[zi][yi][xi-1]);
          PrimitivesCopy(&pde->zg[zi][yi][xi],   &pde->zg[zi][yi][xi-1]);
        } else if (xi == xend-1) {  /* parallel boundary (right) */
          ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &left);
          ComputePrimitives(defs->gamma, &qq[zi][yi][xi+2], &right);
          ComputeGradient_Local(pde->dx, &left, &right, &pde->xg[zi][yi][xi+1]);
          PrimitivesCopy(&pde->yg[zi][yi][xi],   &pde->yg[zi][yi][xi+1]);
          PrimitivesCopy(&pde->zg[zi][yi][xi],   &pde->zg[zi][yi][xi+1]);
        }
        /* y-direction ghost cells */
        if (yi == 0) {  /* front physical boundary */
          if (defs->frontBC != COUPLING) {
            /* periodic ghost is computed using the 2nd ghost */
            ComputePrimitives(defs->gamma, &qq[zi][yi-2][xi], &front);
            ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &back);
            ComputeGradient_Local(pde->dy, &front, &back, &pde->yg[zi][yi-1][xi]);
          } else {
            /* non-periodic ghost has a constant extrapolation */
            PrimitivesCopy(&pde->yg[zi][yi][xi], &pde->yg[zi][yi-1][xi]);
          }
        } else if (yi == info.my-1) {  /* back physical boundary */
          if (defs->backBC != COUPLING) {
            /* periodic ghost is computed using the 2nd ghost */
            ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &front);
            ComputePrimitives(defs->gamma, &qq[zi][yi+2][xi], &back);
            ComputeGradient_Local(pde->dy, &front, &back, &pde->yg[zi][yi+1][xi]);
          } else {
            /* non-periodic ghost has a constant extrapolation */
            PrimitivesCopy(&pde->yg[zi][yi][xi], &pde->yg[zi][yi+1][xi]);
          }
        } else if (yi == ystart) {  /* parallel boundary (front) */
          ComputePrimitives(defs->gamma, &qq[zi][yi-2][xi], &front);
          ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &back);
          ComputeGradient_Local(pde->dy, &front, &back, &pde->yg[zi][yi-1][xi]);
          PrimitivesCopy(&pde->xg[zi][yi][xi], &pde->xg[zi][yi-1][xi]);
          PrimitivesCopy(&pde->zg[zi][yi][xi], &pde->zg[zi][yi-1][xi]);
        } else if (yi == yend-1) {  /* parallel boundary (back) */
          ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &front);
          ComputePrimitives(defs->gamma, &qq[zi][yi+2][xi], &back);
          ComputeGradient_Local(pde->dy, &front, &back, &pde->yg[zi][yi+1][xi]);
          PrimitivesCopy(&pde->xg[zi][yi][xi], &pde->xg[zi][yi+1][xi]);
          PrimitivesCopy(&pde->zg[zi][yi][xi], &pde->zg[zi][yi+1][xi]);
        }

        /* z-direction ghost cells */
        if (zi == 0) {  /* down/bottom physical boundary */
          if (defs->downBC != COUPLING) {
            /* periodic ghost is computed using the 2nd ghost */
            ComputePrimitives(defs->gamma, &qq[zi-2][yi][xi], &down);
            ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &up);
            ComputeGradient_Local(pde->dz, &down, &up, &pde->zg[zi-1][yi][xi]);
          } else {
            /* non-periodic ghost has a constant extrapolation */
            PrimitivesCopy(&pde->zg[zi][yi][xi], &pde->zg[zi-1][yi][xi]);
          }
        } else if (zi == info.mz-1) {  /* up physical boundary */
          if (defs->upBC != COUPLING) {
            /* periodic ghost is computed using the 2nd ghost */
            ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &down);
            ComputePrimitives(defs->gamma, &qq[zi+2][yi][xi], &up);
            ComputeGradient_Local(pde->dz, &down, &up, &pde->zg[zi+1][yi][xi]);
          } else {
            /* non-periodic ghost has a constant extrapolation */
            PrimitivesCopy(&pde->zg[zi][yi][xi], &pde->zg[zi+1][yi][xi]);
          }
        } else if (zi == zstart) {  /* parallel boundary (down) */
          ComputePrimitives(defs->gamma, &qq[zi-2][yi][xi], &down);
          ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &up);
          ComputeGradient_Local(pde->dz, &down, &up, &pde->zg[zi-1][yi][xi]);
          PrimitivesCopy(&pde->xg[zi][yi][xi], &pde->xg[zi-1][yi][xi]);
          PrimitivesCopy(&pde->yg[zi][yi][xi], &pde->yg[zi-1][yi][xi]);
        } else if (zi == zend-1) {  /* parallel boundary (up) */
          ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &down);
          ComputePrimitives(defs->gamma, &qq[zi+2][yi][xi], &up);
          ComputeGradient_Local(pde->dz, &down, &up, &pde->zg[zi+1][yi][xi]);
          PrimitivesCopy(&pde->xg[zi][yi][xi], &pde->xg[zi+1][yi][xi]);
          PrimitivesCopy(&pde->yg[zi][yi][xi], &pde->yg[zi+1][yi][xi]);
        }
      }
    }
  }
  PetscFunctionReturn(0);
}

/* Only compute for z<=bufferz */
/* IN: pde, qq */
/* OUT: xgp, ygp */
PetscErrorCode ComputePrimitiveGradients_BOTTOM(FVNS* pde, Solution*** qq)
{
  DMDALocalInfo info;
  ProblemDefs* defs = (ProblemDefs*)pde->defs;
  Primitives left, right, front, back, down, up;
  PetscInt xi, yi, zi, xstart, ystart, zstart, xend, yend, zend;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  /* loop over the interior of the domain and use central difference for gradients */
  ierr = DMDAGetLocalInfo(pde->dm, &info);CHKERRQ(ierr);
  xstart = info.xs;  xend = info.xs + info.xm;
  ystart = info.ys;  yend = info.ys + info.ym;
  zstart = info.zs;  
  zend   = (pde->multictx.bufferz+1 < info.zs+info.zm) ? pde->multictx.bufferz+1 : info.zs+info.zm;
  for (zi=zstart; zi<zend; zi++) {
    for (yi=ystart; yi<yend; yi++) {
      for (xi=xstart; xi<xend; xi++) {
        /* do x-direction gradients first for left and right faces */
        ComputePrimitives(defs->gamma, &qq[zi][yi][xi-1], &left);
        ComputePrimitives(defs->gamma, &qq[zi][yi][xi+1], &right);
        ComputeGradient_Local(pde->dx, &left, &right, &pde->xg[zi][yi][xi]);

        /* do the y-direction for front and back faces */
        ComputePrimitives(defs->gamma, &qq[zi][yi-1][xi], &front);
        ComputePrimitives(defs->gamma, &qq[zi][yi+1][xi], &back);
        ComputeGradient_Local(pde->dy, &front, &back, &pde->yg[zi][yi][xi]);

        /* now do the z-direction for down/bottom and up/top faces */
        ComputePrimitives(defs->gamma, &qq[zi-1][yi][xi], &down);
        ComputePrimitives(defs->gamma, &qq[zi+1][yi][xi], &up);
        ComputeGradient_Local(pde->dz, &down, &up, &pde->zg[zi][yi][xi]);
        
        /* x-direction ghost cells */
        if (xi == 0) {  /* left physical boundary */
          if (defs->leftBC != COUPLING) {
            /* periodic ghost is computed using the 2nd ghost */
            ComputePrimitives(defs->gamma, &qq[zi][yi][xi-2], &left);
            ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &right);
            ComputeGradient_Local(pde->dx, &left, &right, &pde->xg[zi][yi][xi-1]);
          } else {
            /* non-periodic ghost has a constant extrapolation */
            PrimitivesCopy(&pde->xg[zi][yi][xi], &pde->xg[zi][yi][xi-1]);
          }
        } else if (xi == info.mx-1) {  /* right physical boundary */
          if (defs->rightBC != COUPLING) {
            /* periodic ghost is computed using the 2nd ghost */
            ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &left);
            ComputePrimitives(defs->gamma, &qq[zi][yi][xi+2], &right);
            ComputeGradient_Local(pde->dx, &left, &right, &pde->xg[zi][yi][xi+1]);
          } else {
            /* non-periodic ghost has a constant extrapolation */
            PrimitivesCopy(&pde->xg[zi][yi][xi], &pde->xg[zi][yi][xi+1]);
          }
        } else if (xi == xstart) {  /* parallel boundary (left) */
          ComputePrimitives(defs->gamma, &qq[zi][yi][xi-2], &left);
          ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &right);
          ComputeGradient_Local(pde->dx, &left, &right, &pde->xg[zi][yi][xi-1]);
          PrimitivesCopy(&pde->yg[zi][yi][xi],   &pde->yg[zi][yi][xi-1]);
          PrimitivesCopy(&pde->zg[zi][yi][xi],   &pde->zg[zi][yi][xi-1]);
        } else if (xi == xend-1) {  /* parallel boundary (right) */
          ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &left);
          ComputePrimitives(defs->gamma, &qq[zi][yi][xi+2], &right);
          ComputeGradient_Local(pde->dx, &left, &right, &pde->xg[zi][yi][xi+1]);
          PrimitivesCopy(&pde->yg[zi][yi][xi],   &pde->yg[zi][yi][xi+1]);
          PrimitivesCopy(&pde->zg[zi][yi][xi],   &pde->zg[zi][yi][xi+1]);
        }
        /* y-direction ghost cells */
        if (yi == 0) {  /* front physical boundary */
          if (defs->frontBC != COUPLING) {
            /* periodic ghost is computed using the 2nd ghost */
            ComputePrimitives(defs->gamma, &qq[zi][yi-2][xi], &front);
            ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &back);
            ComputeGradient_Local(pde->dy, &front, &back, &pde->yg[zi][yi-1][xi]);
          } else {
            /* non-periodic ghost has a constant extrapolation */
            PrimitivesCopy(&pde->yg[zi][yi][xi], &pde->yg[zi][yi-1][xi]);
          }
        } else if (yi == info.my-1) {  /* back physical boundary */
          if (defs->backBC != COUPLING) {
            /* periodic ghost is computed using the 2nd ghost */
            ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &front);
            ComputePrimitives(defs->gamma, &qq[zi][yi+2][xi], &back);
            ComputeGradient_Local(pde->dy, &front, &back, &pde->yg[zi][yi+1][xi]);
          } else {
            /* non-periodic ghost has a constant extrapolation */
            PrimitivesCopy(&pde->yg[zi][yi][xi], &pde->yg[zi][yi+1][xi]);
          }
        } else if (yi == ystart) {  /* parallel boundary (front) */
          ComputePrimitives(defs->gamma, &qq[zi][yi-2][xi], &front);
          ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &back);
          ComputeGradient_Local(pde->dy, &front, &back, &pde->yg[zi][yi-1][xi]);
          PrimitivesCopy(&pde->xg[zi][yi][xi], &pde->xg[zi][yi-1][xi]);
          PrimitivesCopy(&pde->zg[zi][yi][xi], &pde->zg[zi][yi-1][xi]);
        } else if (yi == yend-1) {  /* parallel boundary (back) */
          ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &front);
          ComputePrimitives(defs->gamma, &qq[zi][yi+2][xi], &back);
          ComputeGradient_Local(pde->dy, &front, &back, &pde->yg[zi][yi+1][xi]);
          PrimitivesCopy(&pde->xg[zi][yi][xi], &pde->xg[zi][yi+1][xi]);
          PrimitivesCopy(&pde->zg[zi][yi][xi], &pde->zg[zi][yi+1][xi]);
        }

        /* z-direction ghost cells */
        if (zi == 0) {  /* down/bottom physical boundary */
          if (defs->downBC != COUPLING) {
            /* periodic ghost is computed using the 2nd ghost */
            ComputePrimitives(defs->gamma, &qq[zi-2][yi][xi], &down);
            ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &up);
            ComputeGradient_Local(pde->dz, &down, &up, &pde->zg[zi-1][yi][xi]);
          } else {
            /* non-periodic ghost has a constant extrapolation */
            PrimitivesCopy(&pde->zg[zi][yi][xi], &pde->zg[zi-1][yi][xi]);
          }
        } else if (zi == info.mz-1) {  /* up physical boundary */
          if (defs->upBC != COUPLING) {
            /* periodic ghost is computed using the 2nd ghost */
            ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &down);
            ComputePrimitives(defs->gamma, &qq[zi+2][yi][xi], &up);
            ComputeGradient_Local(pde->dz, &down, &up, &pde->zg[zi+1][yi][xi]);
          } else {
            /* non-periodic ghost has a constant extrapolation */
            PrimitivesCopy(&pde->zg[zi][yi][xi], &pde->zg[zi+1][yi][xi]);
          }
        } else if (zi == zstart) {  /* parallel boundary (down) */
          ComputePrimitives(defs->gamma, &qq[zi-2][yi][xi], &down);
          ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &up);
          ComputeGradient_Local(pde->dz, &down, &up, &pde->zg[zi-1][yi][xi]);
          PrimitivesCopy(&pde->xg[zi][yi][xi], &pde->xg[zi-1][yi][xi]);
          PrimitivesCopy(&pde->yg[zi][yi][xi], &pde->yg[zi-1][yi][xi]);
        } else if (zi == zend-1) {  /* parallel boundary (up) */
          ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &down);
          ComputePrimitives(defs->gamma, &qq[zi+2][yi][xi], &up);
          ComputeGradient_Local(pde->dz, &down, &up, &pde->zg[zi+1][yi][xi]);
          PrimitivesCopy(&pde->xg[zi][yi][xi], &pde->xg[zi+1][yi][xi]);
          PrimitivesCopy(&pde->yg[zi][yi][xi], &pde->yg[zi+1][yi][xi]);
        }
      }
    }
  }
  PetscFunctionReturn(0);
}

/* Only compute for z>bufferz */
/* IN: pde, qq */
/* OUT: xgp, ygp */
PetscErrorCode ComputePrimitiveGradients_TOP(FVNS* pde, Solution*** qq)
{
  DMDALocalInfo info;
  ProblemDefs* defs = (ProblemDefs*)pde->defs;
  Primitives left, right, front, back, down, up;
  PetscInt xi, yi, zi, xstart, ystart, zstart, xend, yend, zend;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  /* loop over the interior of the domain and use central difference for gradients */
  ierr = DMDAGetLocalInfo(pde->dm, &info);CHKERRQ(ierr);
  xstart = info.xs;  xend = info.xs + info.xm;
  ystart = info.ys;  yend = info.ys + info.ym;
  zstart = (pde->multictx.bufferz >= info.zs) ? pde->multictx.bufferz+1 : info.zs;
  zend   = info.zs+info.zm;

   for (zi=zstart; zi<zend; zi++) {
    for (yi=ystart; yi<yend; yi++) {
      for (xi=xstart; xi<xend; xi++) {
        /* do x-direction gradients first for left and right faces */
        ComputePrimitives(defs->gamma, &qq[zi][yi][xi-1], &left);
        ComputePrimitives(defs->gamma, &qq[zi][yi][xi+1], &right);
        ComputeGradient_Local(pde->dx, &left, &right, &pde->xg[zi][yi][xi]);

        /* do the y-direction for front and back faces */
        ComputePrimitives(defs->gamma, &qq[zi][yi-1][xi], &front);
        ComputePrimitives(defs->gamma, &qq[zi][yi+1][xi], &back);
        ComputeGradient_Local(pde->dy, &front, &back, &pde->yg[zi][yi][xi]);

        /* now do the z-direction for down/bottom and up/top faces */
        ComputePrimitives(defs->gamma, &qq[zi-1][yi][xi], &down);
        ComputePrimitives(defs->gamma, &qq[zi+1][yi][xi], &up);
        ComputeGradient_Local(pde->dz, &down, &up, &pde->zg[zi][yi][xi]);
        
        /* x-direction ghost cells */
        if (xi == 0) {  /* left physical boundary */
          if (defs->leftBC != COUPLING) {
            /* periodic ghost is computed using the 2nd ghost */
            ComputePrimitives(defs->gamma, &qq[zi][yi][xi-2], &left);
            ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &right);
            ComputeGradient_Local(pde->dx, &left, &right, &pde->xg[zi][yi][xi-1]);
          } else {
            /* non-periodic ghost has a constant extrapolation */
            PrimitivesCopy(&pde->xg[zi][yi][xi], &pde->xg[zi][yi][xi-1]);
          }
        } else if (xi == info.mx-1) {  /* right physical boundary */
          if (defs->rightBC != COUPLING) {
            /* periodic ghost is computed using the 2nd ghost */
            ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &left);
            ComputePrimitives(defs->gamma, &qq[zi][yi][xi+2], &right);
            ComputeGradient_Local(pde->dx, &left, &right, &pde->xg[zi][yi][xi+1]);
          } else {
            /* non-periodic ghost has a constant extrapolation */
            PrimitivesCopy(&pde->xg[zi][yi][xi], &pde->xg[zi][yi][xi+1]);
          }
        } else if (xi == xstart) {  /* parallel boundary (left) */
          ComputePrimitives(defs->gamma, &qq[zi][yi][xi-2], &left);
          ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &right);
          ComputeGradient_Local(pde->dx, &left, &right, &pde->xg[zi][yi][xi-1]);
          PrimitivesCopy(&pde->yg[zi][yi][xi],   &pde->yg[zi][yi][xi-1]);
          PrimitivesCopy(&pde->zg[zi][yi][xi],   &pde->zg[zi][yi][xi-1]);
        } else if (xi == xend-1) {  /* parallel boundary (right) */
          ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &left);
          ComputePrimitives(defs->gamma, &qq[zi][yi][xi+2], &right);
          ComputeGradient_Local(pde->dx, &left, &right, &pde->xg[zi][yi][xi+1]);
          PrimitivesCopy(&pde->yg[zi][yi][xi],   &pde->yg[zi][yi][xi+1]);
          PrimitivesCopy(&pde->zg[zi][yi][xi],   &pde->zg[zi][yi][xi+1]);
        }
        /* y-direction ghost cells */
        if (yi == 0) {  /* front physical boundary */
          if (defs->frontBC != COUPLING) {
            /* periodic ghost is computed using the 2nd ghost */
            ComputePrimitives(defs->gamma, &qq[zi][yi-2][xi], &front);
            ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &back);
            ComputeGradient_Local(pde->dy, &front, &back, &pde->yg[zi][yi-1][xi]);
          } else {
            /* non-periodic ghost has a constant extrapolation */
            PrimitivesCopy(&pde->yg[zi][yi][xi], &pde->yg[zi][yi-1][xi]);
          }
        } else if (yi == info.my-1) {  /* back physical boundary */
          if (defs->backBC != COUPLING) {
            /* periodic ghost is computed using the 2nd ghost */
            ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &front);
            ComputePrimitives(defs->gamma, &qq[zi][yi+2][xi], &back);
            ComputeGradient_Local(pde->dy, &front, &back, &pde->yg[zi][yi+1][xi]);
          } else {
            /* non-periodic ghost has a constant extrapolation */
            PrimitivesCopy(&pde->yg[zi][yi][xi], &pde->yg[zi][yi+1][xi]);
          }
        } else if (yi == ystart) {  /* parallel boundary (front) */
          ComputePrimitives(defs->gamma, &qq[zi][yi-2][xi], &front);
          ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &back);
          ComputeGradient_Local(pde->dy, &front, &back, &pde->yg[zi][yi-1][xi]);
          PrimitivesCopy(&pde->xg[zi][yi][xi], &pde->xg[zi][yi-1][xi]);
          PrimitivesCopy(&pde->zg[zi][yi][xi], &pde->zg[zi][yi-1][xi]);
        } else if (yi == yend-1) {  /* parallel boundary (back) */
          ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &front);
          ComputePrimitives(defs->gamma, &qq[zi][yi+2][xi], &back);
          ComputeGradient_Local(pde->dy, &front, &back, &pde->yg[zi][yi+1][xi]);
          PrimitivesCopy(&pde->xg[zi][yi][xi], &pde->xg[zi][yi+1][xi]);
          PrimitivesCopy(&pde->zg[zi][yi][xi], &pde->zg[zi][yi+1][xi]);
        }

        /* z-direction ghost cells */
        if (zi == 0) {  /* down/bottom physical boundary */
          if (defs->downBC != COUPLING) {
            /* periodic ghost is computed using the 2nd ghost */
            ComputePrimitives(defs->gamma, &qq[zi-2][yi][xi], &down);
            ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &up);
            ComputeGradient_Local(pde->dz, &down, &up, &pde->zg[zi-1][yi][xi]);
          } else {
            /* non-periodic ghost has a constant extrapolation */
            PrimitivesCopy(&pde->zg[zi][yi][xi], &pde->zg[zi-1][yi][xi]);
          }
        } else if (zi == info.mz-1) {  /* up physical boundary */
          if (defs->upBC != COUPLING) {
            /* periodic ghost is computed using the 2nd ghost */
            ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &down);
            ComputePrimitives(defs->gamma, &qq[zi+2][yi][xi], &up);
            ComputeGradient_Local(pde->dz, &down, &up, &pde->zg[zi+1][yi][xi]);
          } else {
            /* non-periodic ghost has a constant extrapolation */
            PrimitivesCopy(&pde->zg[zi][yi][xi], &pde->zg[zi+1][yi][xi]);
          }
        } else if (zi == zstart) {  /* parallel boundary (down) */
          ComputePrimitives(defs->gamma, &qq[zi-2][yi][xi], &down);
          ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &up);
          ComputeGradient_Local(pde->dz, &down, &up, &pde->zg[zi-1][yi][xi]);
          PrimitivesCopy(&pde->xg[zi][yi][xi], &pde->xg[zi-1][yi][xi]);
          PrimitivesCopy(&pde->yg[zi][yi][xi], &pde->yg[zi-1][yi][xi]);
        } else if (zi == zend-1) {  /* parallel boundary (up) */
          ComputePrimitives(defs->gamma, &qq[zi][yi][xi], &down);
          ComputePrimitives(defs->gamma, &qq[zi+2][yi][xi], &up);
          ComputeGradient_Local(pde->dz, &down, &up, &pde->zg[zi+1][yi][xi]);
          PrimitivesCopy(&pde->xg[zi][yi][xi], &pde->xg[zi+1][yi][xi]);
          PrimitivesCopy(&pde->yg[zi][yi][xi], &pde->yg[zi+1][yi][xi]);
        }
      }
    }
  }
  PetscFunctionReturn(0);
}