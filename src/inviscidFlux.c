#include "fvns3d.h"

/* IN:  gamma, qq */
/* OUT: u, v, w, pres */
static void ComputeIFlux_Kernel(
  PetscScalar gamma, Solution* qq,
  PetscScalar *u, PetscScalar *v, PetscScalar *w, PetscScalar* pres)
{
  PetscScalar uu, vv, ww, pp, vel2;

  uu = qq->rhoU/qq->rho;
  vv = qq->rhoV/qq->rho;
  ww = qq->rhoW/qq->rho;
  vel2 = uu*uu + vv*vv + ww*ww;
  pp = (gamma - 1.0)*(qq->rhoE - 0.5*qq->rho*vel2);

  *u = uu;  *v = vv;  *w = ww;  *pres = pp;
}

/* IN:  gamma, faces, qq */
/* OUT: fluxI */
static PetscErrorCode ComputeIFlux_Each(
  PetscScalar gamma, DMDirection faces, Solution* qq,
  Solution *fluxI)
{
  PetscScalar nx, ny, nz;
  PetscScalar u, v, w, pres;
  Solution xFluxI, yFluxI, zFluxI;

  PetscFunctionBeginUser;
  switch (faces) {
    case DM_X:
      nx = 1.0;  ny = 0.0;  nz = 0.0;
      break;
    case DM_Y:
      nx = 0.0;  ny = 1.0;  nz = 0.0;
      break;
    case DM_Z:
      nx = 0.0;  ny = 0.0;  nz = 1.0;
      break;
    default:
      SETERRQ(PETSC_COMM_WORLD, PETSC_ERR_ARG_WRONG,
              "ComputeIFlux_Each: Invalid face group");
  }

  ComputeIFlux_Kernel(gamma, qq, &u, &v, &w, &pres);

  xFluxI.rho  = qq->rhoU;
  xFluxI.rhoU = qq->rhoU*u + pres;
  xFluxI.rhoV = qq->rhoU*v;
  xFluxI.rhoW = qq->rhoU*w;
  xFluxI.rhoE = u*(qq->rhoE + pres);

  yFluxI.rho  = qq->rhoV;
  yFluxI.rhoU = qq->rhoV*u;
  yFluxI.rhoV = qq->rhoV*v + pres;
  yFluxI.rhoW = qq->rhoV*w;
  yFluxI.rhoE = v*(qq->rhoE + pres);

  zFluxI.rho = qq->rhoW;
  zFluxI.rhoU = qq->rhoW*u;
  zFluxI.rhoV = qq->rhoW*v;
  zFluxI.rhoW = qq->rhoW*w + pres;
  zFluxI.rhoE = w*(qq->rhoE + pres);

  SolutionAXPBYPCZ(nx, &xFluxI, ny, &yFluxI, nz, &zFluxI, fluxI);
  PetscFunctionReturn(0);
}

/* IN:  gamma, faces, prev, next */
/* OUT: penalty */
static PetscErrorCode ComputeIFlux_Penalty(
  PetscScalar gamma, DMDirection faces, Solution* prev, Solution* next,
  Solution* penalty)
{
  PetscScalar nx, ny, nz, vel2, pres, a, un;
  PetscScalar uPrev, uNext, vPrev, vNext, wPrev, wNext, Hprev, Hnext, tauPrev, tauNext;
  PetscScalar rsPrev, rsNext, rHat, uHat, vHat, wHat, Hhat, lam;

  PetscFunctionBeginUser;
  switch (faces) {
    case DM_X:
      nx = 1.0;  ny = 0.0;  nz = 0.0;
      break;
    case DM_Y:
      nx = 0.0;  ny = 1.0;  nz = 0.0;
      break;
    case DM_Z:
      nx = 0.0;  ny = 0.0;  nz = 1.0;
      break;
    default:
      SETERRQ(PETSC_COMM_WORLD, PETSC_ERR_ARG_WRONG,
              "ComputeIFlux_Penalty: Invalid face group");
  }

  ComputeIFlux_Kernel(gamma, prev, &uPrev, &vPrev, &wPrev, &pres);
  Hprev = (prev->rhoE + pres)/prev->rho;
  un = uPrev*nx + vPrev*ny + wPrev*nz;
  a = PetscSqrtScalar(gamma*pres/prev->rho);
  tauPrev = PetscAbs(un) + a;

  ComputeIFlux_Kernel(gamma, next, &uNext, &vNext, &wNext, &pres);
  Hnext = (next->rhoE + pres)/next->rho;
  un = uNext*nx + vNext*ny + wNext*nz;
  a = PetscSqrtScalar(gamma*pres/next->rho);
  tauNext = PetscAbsScalar(un) + a;

  rsPrev = PetscSqrtScalar(prev->rho);
  rsNext = PetscSqrtScalar(next->rho);
  rHat = rsPrev + rsNext;
  uHat = (rsPrev*uPrev + rsNext*uNext)/rHat;
  vHat = (rsPrev*vPrev + rsNext*vNext)/rHat;
  wHat = (rsPrev*wPrev + rsNext*wNext)/rHat;
  Hhat = (rsPrev*Hprev + rsNext*Hnext)/rHat;
  vel2 = uHat*uHat + vHat*vHat + wHat*wHat;
  a = PetscSqrtScalar((gamma - 1.0)*(Hhat - 0.5*vel2));
  un = uHat*nx + vHat*ny + wHat*nz;
  lam = PetscMax(PetscMax(tauPrev, tauNext), PetscAbsScalar(un) + a);

  SolutionAXPBY(lam, next, -lam, prev, penalty);
  PetscFunctionReturn(0);
}

/* IN:  defs, qf */
/* OUT: xFluxI, yFluxI, zFluxI */
PetscErrorCode ComputeIFlux(
  FVNS* pde, PetscInt xi, PetscInt yi, PetscInt zi, Solution* xFluxI, Solution* yFluxI, Solution* zFluxI)
{
  Solution xFluxI_L, xFluxI_R, yFluxI_F, yFluxI_B, zFluxI_D, zFluxI_U;
  Solution xPenalty, yPenalty, zPenalty;
  PetscScalar gamma=pde->defs->gamma;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  /*
    do "prev" and "next" sides separately for each direction
    x-dir = (1.0, 0.0, 0.0) & y-dir = (0.0, 1.0, 0.0) & z-dir = (0.0, 0.0, 1.0)
  */
  ierr = ComputeIFlux_Each(gamma, DM_X, &pde->qf_L[zi][yi][xi], &xFluxI_L);CHKERRQ(ierr);
  ierr = ComputeIFlux_Each(gamma, DM_X, &pde->qf_R[zi][yi][xi], &xFluxI_R);CHKERRQ(ierr);
  ierr = ComputeIFlux_Each(gamma, DM_Y, &pde->qf_F[zi][yi][xi], &yFluxI_F);CHKERRQ(ierr);
  ierr = ComputeIFlux_Each(gamma, DM_Y, &pde->qf_B[zi][yi][xi], &yFluxI_B);CHKERRQ(ierr);
  ierr = ComputeIFlux_Each(gamma, DM_Z, &pde->qf_D[zi][yi][xi], &zFluxI_D);CHKERRQ(ierr);
  ierr = ComputeIFlux_Each(gamma, DM_Z, &pde->qf_U[zi][yi][xi], &zFluxI_U);CHKERRQ(ierr);

  /* now compute the penalty terms for each face group */
  ierr = ComputeIFlux_Penalty(
    gamma, DM_X, &pde->qf_L[zi][yi][xi], &pde->qf_R[zi][yi][xi], &xPenalty);CHKERRQ(ierr);
  ierr = ComputeIFlux_Penalty(
    gamma, DM_Y, &pde->qf_F[zi][yi][xi], &pde->qf_B[zi][yi][xi], &yPenalty);CHKERRQ(ierr);
  ierr = ComputeIFlux_Penalty(
    gamma, DM_Z, &pde->qf_D[zi][yi][xi], &pde->qf_U[zi][yi][xi], &zPenalty);CHKERRQ(ierr);

  /* write out the final LF flux */
  SolutionAXPBYPCZ(0.5, &xFluxI_R, 0.5, &xFluxI_L, -0.5, &xPenalty, xFluxI);
  SolutionAXPBYPCZ(0.5, &yFluxI_B, 0.5, &yFluxI_F, -0.5, &yPenalty, yFluxI);
  SolutionAXPBYPCZ(0.5, &zFluxI_U, 0.5, &zFluxI_D, -0.5, &zPenalty, zFluxI);
  PetscFunctionReturn(0);
}