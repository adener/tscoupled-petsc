#include "fvns3d.h"

/* IS generations functions for multirate */


PetscErrorCode CreateZSplitIS(FVNS* pde,IS* bottomIS,IS* topIS)
{
  PetscInt xstart, ystart, zstart, nxe, nye, nze,dof,bufferz = pde->multictx.bufferz;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = DMDAGetCorners(pde->dm, &xstart, &ystart, &zstart, &nxe, &nye, &nze);CHKERRQ(ierr);
  ierr = DMDAGetDof(pde->dm,&dof);CHKERRQ(ierr);

  pde->multictx.lowerBOTTOM_stencil.i = xstart; 
  pde->multictx.lowerBOTTOM_stencil.j = ystart; 
  pde->multictx.lowerBOTTOM_stencil.k = zstart; 

  pde->multictx.upperBOTTOM_stencil.i = xstart+nxe; 
  pde->multictx.upperBOTTOM_stencil.j = ystart+nye; 
  pde->multictx.upperBOTTOM_stencil.k = (zstart+nze > bufferz+1) ? bufferz+1 : zstart+nze;
  /* odd fix - maybe bug in DMDACreatePatchIS */ 
  if(pde->multictx.upperBOTTOM_stencil.k <= zstart) {pde->multictx.upperBOTTOM_stencil = pde->multictx.lowerBOTTOM_stencil;}

  pde->multictx.upperTOP_stencil.i = xstart+nxe; 
  pde->multictx.upperTOP_stencil.j = ystart+nye; 
  pde->multictx.upperTOP_stencil.k = zstart+nze;
 
  pde->multictx.lowerTOP_stencil.i = xstart; 
  pde->multictx.lowerTOP_stencil.j = ystart; 
  pde->multictx.lowerTOP_stencil.k = pde->multictx.upperBOTTOM_stencil.k;
  /* odd fix - maybe bug in DMDACreatePatchIS */ 
  if(pde->multictx.upperBOTTOM_stencil.k == zstart+nze) {pde->multictx.upperTOP_stencil = pde->multictx.lowerTOP_stencil;}

  ierr = DMDACreatePatchIS(pde->dm,&pde->multictx.lowerBOTTOM_stencil,&pde->multictx.upperBOTTOM_stencil,bottomIS,PETSC_FALSE);CHKERRQ(ierr);
  ierr = DMDACreatePatchIS(pde->dm,&pde->multictx.lowerTOP_stencil,&pde->multictx.upperTOP_stencil,topIS,PETSC_FALSE);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


PetscErrorCode CreateZSplitIS_DA(DM dmda,PetscInt zsplit,IS* bottomIS,IS* topIS)
{
  PetscInt       xstart, ystart, zstart, nxe, nye, nze,dof; 
  DMType         dmtype; 
  MatStencil     BOTTOM_lower, BOTTOM_upper,TOP_lower,TOP_upper;
  PetscBool      dmflg; 
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = DMGetType(dmda,&dmtype);CHKERRQ(ierr);
  ierr = PetscStrcmp(dmtype,DMDA,&dmflg);CHKERRQ(ierr);
  if(!dmflg) {SETERRQ1(PetscObjectComm((PetscObject)dmda),PETSC_ERR_ARG_WRONG,"Must input a DMDA, you inputted a %s",dmtype);}
  ierr = DMDAGetCorners(dmda, &xstart, &ystart, &zstart, &nxe, &nye, &nze);CHKERRQ(ierr);
  ierr = DMDAGetDof(dmda,&dof);CHKERRQ(ierr);

  BOTTOM_lower.i = xstart; 
  BOTTOM_lower.j = ystart; 
  BOTTOM_lower.k = zstart; 

  BOTTOM_upper.i = xstart+nxe; 
  BOTTOM_upper.j = ystart+nye; 
  BOTTOM_upper.k = (zstart+nze > zsplit+1) ? zsplit+1 : zstart+nze;
  /* 
    odd fix - maybe bug in DMDACreatePatchIS
    If the upper corner is at the same z level or lower than the lower corner, set them equal so 
    DMDACreatePatchIS returns an empty IS, which is the desired output. 
  */ 
  if(BOTTOM_upper.k <= zstart) {BOTTOM_upper  = BOTTOM_lower ;}

  TOP_upper.i = xstart+nxe; 
  TOP_upper.j = ystart+nye; 
  TOP_upper.k = zstart+nze;
 
  TOP_lower.i = xstart; 
  TOP_lower.j = ystart; 
  TOP_lower.k = BOTTOM_upper.k;
  /* 
    odd fix - maybe bug in DMDACreatePatchIS 
    If the z level of lower and upper corners are off by one, specify them as the same to 
    force DMDACreatePatchIS to return an empty IS, the desired output
  */ 
  if(TOP_lower.k == zstart+nze) {TOP_upper = TOP_lower;}

  ierr = DMDACreatePatchIS(dmda,&BOTTOM_lower,&BOTTOM_upper,bottomIS,PETSC_FALSE);CHKERRQ(ierr);
  ierr = DMDACreatePatchIS(dmda,&TOP_lower,&TOP_upper,topIS,PETSC_FALSE);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}