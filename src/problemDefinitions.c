#include "fvns3d.h"

const char* ProblemTypes[8]={"LDC","TGV","WDF","HS","TB","ProblemType","PTYPE_",NULL};
const char* BoundaryTypes[6]={"PERIODIC","DIRICHLET","COUPLING","BoundaryType","",NULL};
const char* BoundaryLocations[8]={"NONE","LEFT","RIGHT","BOTTOM","TOP","BoundaryLocation","",NULL};

static void InitialCondition_TaylorGreenVortex(ProblemDefs* defs, PetscReal xc, PetscReal yc, PetscReal zc, Solution* qq)
{
  PetscScalar u, v, w, pres;
  PetscScalar u0, rho, gamma;
  FluidReferenceStates  ref    = defs->fluidref; 
  NonDimensionalization nondim = defs->nondim; 

  u0  = ref.u;
  rho = ref.density/nondim.density;
  gamma = defs->gamma;

  u =  u0*PetscSinScalar(2.0*PETSC_PI*xc)*PetscCosScalar(2.0*PETSC_PI*yc)*PetscCosScalar(2.0*PETSC_PI*zc);
  v = -u0*PetscCosScalar(2.0*PETSC_PI*xc)*PetscSinScalar(2.0*PETSC_PI*yc)*PetscCosScalar(2.0*PETSC_PI*zc);
  w = 0.0;

  pres = ref.pressure/nondim.pressure + (1.0/16.0)*rho*u0*u0*(PetscCosScalar(4.0*PETSC_PI*xc) + PetscCosScalar(4.0*PETSC_PI*yc))*(PetscCosScalar(4.0*PETSC_PI*zc) + 2.0);

  qq->rho = rho;
  qq->rhoU = rho*u;
  qq->rhoV = rho*v;
  qq->rhoW = rho*w;
  qq->rhoE = pres/(gamma-1) + 0.5*rho*(u*u + v*v + w*w);
}

static void InitialCondition_LidDrivenCavity(ProblemDefs* defs, PetscReal xc, PetscReal yc, PetscReal zc, Solution* qq)
{
  PetscScalar u, v, w, pres;
  PetscScalar u0, w0, rho, gamma;
  FluidReferenceStates  ref    = defs->fluidref; 
  NonDimensionalization nondim = defs->nondim; 

  u0 = ref.u;
  w0 = ref.w;
  rho = ref.density/nondim.density;

  gamma = defs->gamma;

  u = u0*(zc - 0.5);
  v = 0.0;
  w = w0*(0.5 - xc);
  pres = ref.pressure/nondim.pressure;

  qq->rho = rho;
  qq->rhoU = rho*u;
  qq->rhoV = rho*v;
  qq->rhoW = rho*w;
  qq->rhoE = pres/(gamma-1) + 0.5*rho*(u*u + v*v + w*w);
}

static void InitialCondition_HydroStatic(ProblemDefs* defs, PetscReal xc, PetscReal yc, PetscReal zc, Solution* qq)
{
  PetscScalar u, v, w, pressure,rho;
  PetscScalar gamma,exnerp,cp;
  FluidReferenceStates  ref    = defs->fluidref; 
  NonDimensionalization nondim = defs->nondim; 

  gamma = defs->gamma;
  cp    = defs->cp;

  /* perturbed velocity */
  u = ref.u; 
  v = ref.v; 
  w = ref.w;

  /* Exner pressure */
  exnerp = 1. -  defs->g*(zc) / (1/(gamma-1.));

  /* compute normalized density/pressure */
  pressure = ref.pressure * PetscPowScalar(exnerp,gamma/(gamma-1.0))/nondim.pressure;
  rho      = ref.density  * PetscPowScalar(exnerp,1./(gamma-1.0))/nondim.density;

  /* convert to conservative variables */
  qq->rho  = rho;
  qq->rhoU = rho*u;
  qq->rhoV = rho*v;
  qq->rhoW = rho*w;

  qq->rhoE = pressure/(gamma-1) + 0.5*rho*(u*u + v*v + w*w); /* kinetic + internal energy */
}

static void InitialCondition_HydroStatic2(ProblemDefs* defs, PetscReal xc, PetscReal yc, PetscReal zc, Solution* qq)
{
  PetscScalar u, v, w, pressure,rho,E;
  PetscScalar gamma,exnerp,cp;
  FluidReferenceStates  ref    = defs->fluidref; 
  NonDimensionalization nondim = defs->nondim; 

  gamma = defs->gamma;
  cp    = defs->cp;

  /* perturbed velocity */
  u = ref.u; 
  v = ref.v; 
  w = ref.w;
  
  /* Intenal Energy */ 

  E = 1.0; 

  /* compute normalized density/pressure */

  rho      = ref.density  * PetscExpReal(-defs->g*zc/(E*(gamma-1)))/nondim.density;
  pressure = rho*E*(gamma-1);
  /* convert to conservative variables */
  qq->rho  = rho;
  qq->rhoU = rho*u;
  qq->rhoV = rho*v;
  qq->rhoW = rho*w;

  qq->rhoE = pressure/(gamma-1) + 0.5*rho*(u*u + v*v + w*w); /* kinetic + internal energy */
}

static void InitialCondition_RisingThermalBubble(ProblemDefs* defs, PetscReal xc, PetscReal yc, PetscReal zc, Solution* qq)
{
  PetscScalar           r,u, v, w, pressure,rho,dtemp; 
  PetscScalar           gamma,exnerp,cp;
  FluidReferenceStates  ref    = defs->fluidref; 
  NonDimensionalization nondim = defs->nondim; 

  /* to be added directly to the problem def later */ 
  /* these are parameters specific to this problem definition */
  PetscReal             bubbleradius = defs->bubble_r,bubble_x = defs->bubble_x,bubble_y = defs->bubble_y, bubble_z = defs->bubble_z, dtemp0 = defs->dtemp0; 

  /* renaming */
  gamma = defs->gamma;
  cp    = defs->cp;

  /* perturbed velocity */
  u = ref.u; 
  v = ref.v; 
  w = ref.w; 

  /* Exner pressure */
  exnerp = 1. - defs->g*(zc) / (1/(gamma-1.));

 /* perturbation of potential temperature */
  r = PetscSqrtReal(PetscSqr(bubble_x-xc) + PetscSqr(bubble_y-yc) + PetscSqr(bubble_z-zc)); 
  if (r < bubbleradius){
    dtemp = dtemp0 * 0.5 * (1.0 + PetscCosReal(PETSC_PI*r/bubbleradius));
    rho = ref.density*ref.temp/(ref.temp+dtemp); /* ref.density has already been scaled by R*ref.temp */
  } else {
    rho = ref.density; 
  }

  /* compute normalized density/pressure */
  pressure = ref.pressure * PetscPowScalar(exnerp,gamma/(gamma-1))/nondim.pressure;
  rho      *=  PetscPowScalar(exnerp,1./(gamma-1))/nondim.density;

  /* convert to conservative variables */
  qq->rho  = rho;
  qq->rhoU = rho*u;
  qq->rhoV = rho*v;
  qq->rhoW = rho*w;

  qq->rhoE = pressure/(gamma-1.) + 0.5*rho*(u*u + v*v + w*w); /* kinetic + internal energy */
}

static void InitialCondition_RisingThermalBubble2(ProblemDefs* defs, PetscReal xc, PetscReal yc, PetscReal zc, Solution* qq)
{
  PetscScalar           r,u, v, w, pressure,rho,dtemp,E; 
  PetscScalar           gamma,exnerp,cp;
  FluidReferenceStates  ref    = defs->fluidref; 
  NonDimensionalization nondim = defs->nondim; 

  /* to be added directly to the problem def later */ 
  /* these are parameters specific to this problem definition */
  PetscReal             bubbleradius = defs->bubble_r,bubble_x = defs->bubble_x,bubble_y = defs->bubble_y, bubble_z = defs->bubble_z, dtemp0 = defs->dtemp0; 

  gamma = defs->gamma;
  cp    = defs->cp;

  /* perturbed velocity */
  u = ref.u; 
  v = ref.v; 
  w = ref.w;
  
  /* Intenal Energy */ 
  E = 1.0; 

  /* compute normalized density/pressure */

  rho      = ref.density  * PetscExpReal(-defs->g*zc/(E*(gamma-1)))/nondim.density;


 /* perturbation of potential temperature */
  r = PetscSqrtReal(PetscSqr(bubble_x-xc) + PetscSqr(bubble_y-yc) + PetscSqr(bubble_z-zc)); 
  if (r < bubbleradius){
    dtemp = dtemp0 * 0.5 * (1.0 + PetscCosReal(PETSC_PI*r/bubbleradius));
    rho = ref.density*ref.temp/(ref.temp+dtemp); /* ref.density has already been scaled by R*ref.temp */
  } else {
    rho = ref.density; 
  }
  pressure = rho*E*(gamma-1);

  /* convert to conservative variables */
  qq->rho  = rho;
  qq->rhoU = rho*u;
  qq->rhoV = rho*v;
  qq->rhoW = rho*w;

  qq->rhoE = pressure/(gamma-1.) + 0.5*rho*(u*u + v*v + w*w); /* kinetic + internal energy */
}

static void InitialCondition_WindDrivenFlow(ProblemDefs* defs, PetscReal xc, PetscReal yc, PetscReal zc, Solution* qq)
{
  PetscScalar rho, u, v, w, pres;
  PetscScalar gamma;
  FluidReferenceStates  ref    = defs->fluidref; 
  NonDimensionalization nondim = defs->nondim; 

  gamma = defs->gamma;
  /* perturbed initial data */
  u = ref.u; 
  v = ref.v; 
  w = ref.w; 
  pres = ref.pressure/nondim.pressure;
  rho  = ref.density/nondim.density;

  qq->rho = rho;
  qq->rhoU = rho*u;
  qq->rhoV = rho*v;
  qq->rhoW = rho*w;
  qq->rhoE = pres/(gamma-1.) + 0.5*rho*(u*u + v*v + w*w);
}

PetscErrorCode ProblemDefsCreate(
  const char* prefix, const char* outputdir,ProblemType ptype, BoundaryLocation coupling, ProblemDefs** defs)
{
  ProblemDefs *newdefs;
  size_t len;
  PetscBool flg, hasUnderscore;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PetscNew(&newdefs);CHKERRQ(ierr);
  
  /* Create prefix */
  ierr = PetscStrlen(prefix, &len);CHKERRQ(ierr);
  if (len) {
    ierr = PetscStrendswith(prefix, "_", &hasUnderscore);CHKERRQ(ierr);
    if (!hasUnderscore) {
      ierr = PetscMalloc1(len+2, &newdefs->prefix);CHKERRQ(ierr);
      ierr = PetscSNPrintf(newdefs->prefix, len+2, "%s_", prefix);CHKERRQ(ierr);
    } else {
      ierr = PetscMalloc1(len+1, &newdefs->prefix);CHKERRQ(ierr);
      ierr = PetscStrncpy(newdefs->prefix, prefix, len+1);CHKERRQ(ierr);
    }
  } else newdefs->prefix = NULL;

  /* create outputdir */ 

  ierr = PetscStrlen(outputdir,&len);CHKERRQ(ierr);
  if(len){
    ierr = PetscMalloc1(len+1,&newdefs->outdir);CHKERRQ(ierr);
    ierr = PetscStrncpy(newdefs->outdir,outputdir,len+1);CHKERRQ(ierr);
  } else {
    ierr = PetscMalloc1(len+1,&newdefs->outdir);CHKERRQ(ierr);
    newdefs->outdir[0] = '.'; 
    newdefs->outdir[1] = 0; 
  }

  newdefs->ptype = ptype;
  ierr = PetscOptionsGetEnum(NULL,newdefs->prefix,"-ptype",ProblemTypes,(PetscEnum*)&newdefs->ptype,&flg);CHKERRQ(ierr);
  newdefs->couplingSide = coupling;

  newdefs->visc_diss_alpha = 0.0;
  ierr = PetscOptionsGetScalar(NULL,newdefs->prefix,"-alpha",&newdefs->visc_diss_alpha,&flg);CHKERRQ(ierr);
  newdefs->gamma = 1.4;
  ierr = PetscOptionsGetScalar(NULL,newdefs->prefix,"-gamma",&newdefs->gamma,&flg);CHKERRQ(ierr);
  newdefs->pr = 0.72;
  ierr = PetscOptionsGetScalar(NULL,newdefs->prefix,"-pr",&newdefs->pr,&flg);CHKERRQ(ierr);
  newdefs->re = 100.0;
  ierr = PetscOptionsGetScalar(NULL,newdefs->prefix,"-re",&newdefs->re,&flg);CHKERRQ(ierr);
  newdefs->mu = 1.0/newdefs->re;

  newdefs->td_left   = 0.0;  newdefs->td_right  = 0.0;
  newdefs->td_front  = 0.0;  newdefs->td_back   = 0.0;
  newdefs->td_down   = 0.0;  newdefs->td_up     = 0.0;
  
  /* seem unused, what are these for ? */
  newdefs->ud_left   = 0.0;  newdefs->vd_left   = 0.0;  newdefs->wd_left   = 0.0;
  newdefs->ud_right  = 0.0;  newdefs->vd_right  = 0.0;  newdefs->wd_right  = 0.0;
  newdefs->ud_front  = 0.0;  newdefs->vd_front  = 0.0;  newdefs->wd_front  = 0.0;
  newdefs->ud_back   = 0.0;  newdefs->vd_back   = 0.0;  newdefs->wd_back   = 0.0;
  newdefs->ud_down   = 0.0;  newdefs->vd_down   = 0.0;  newdefs->wd_down   = 0.0;
  newdefs->ud_up     = 0.0;  newdefs->vd_up     = 0.0;  newdefs->wd_up     = 0.0;
  
  newdefs->fluidref.u = 0.0;
  ierr = PetscOptionsGetScalar(NULL,newdefs->prefix,"-u0",&newdefs->fluidref.u,&flg);CHKERRQ(ierr);
  newdefs->fluidref.v = 0.0;
  ierr = PetscOptionsGetScalar(NULL,newdefs->prefix,"-v0",&newdefs->fluidref.v,&flg);CHKERRQ(ierr);
  newdefs->fluidref.w = 0.0;
  ierr = PetscOptionsGetScalar(NULL,newdefs->prefix,"-w0",&newdefs->fluidref.w,&flg);CHKERRQ(ierr);

  newdefs->fluidref.pressure = 1e5;
  ierr = PetscOptionsGetScalar(NULL,newdefs->prefix,"-pres0",&newdefs->fluidref.pressure,&flg);CHKERRQ(ierr);
  newdefs->fluidref.temp     = 300; 
  ierr = PetscOptionsGetScalar(NULL,newdefs->prefix,"-temp0",&newdefs->fluidref.temp,&flg);CHKERRQ(ierr);
  newdefs->cv = 717.0; 
  ierr = PetscOptionsGetScalar(NULL,newdefs->prefix,"-cv",&newdefs->cv,&flg);CHKERRQ(ierr);
  newdefs->cp = newdefs->cv*newdefs->gamma;
  newdefs->R = newdefs->cp - newdefs->cv; 
  newdefs->fluidref.density  =  newdefs->fluidref.pressure/(newdefs->R * newdefs->fluidref.temp);

  newdefs->nondim.density  = newdefs->fluidref.density; 
  ierr = PetscOptionsGetScalar(NULL,newdefs->prefix,"-dens_nondim",&newdefs->nondim.density,&flg);CHKERRQ(ierr);
  newdefs->nondim.pressure = newdefs->gamma*newdefs->fluidref.pressure; 
  ierr = PetscOptionsGetScalar(NULL,newdefs->prefix,"-pres_nondim",&newdefs->nondim.pressure,&flg);CHKERRQ(ierr);
  newdefs->nondim.temp     = newdefs->fluidref.temp; 
  ierr = PetscOptionsGetScalar(NULL,newdefs->prefix,"-temp_nondim",&newdefs->nondim.temp,&flg);CHKERRQ(ierr);
  newdefs->nondim.L        = 1.0; 
  ierr = PetscOptionsGetScalar(NULL,newdefs->prefix,"-L_nondim",&newdefs->nondim.L,&flg);CHKERRQ(ierr);

  newdefs->g_dim     = 0.0;
  ierr = PetscOptionsGetScalar(NULL,newdefs->prefix,"-g",&newdefs->g_dim,&flg);CHKERRQ(ierr);
  /* nondimensionalization */ 
  newdefs->g = newdefs->g_dim/newdefs->gamma/newdefs->fluidref.temp/newdefs->R*newdefs->nondim.L;


  /* tempraray for the bubble problem */
  newdefs->bubble_r = 1.0;
  ierr = PetscOptionsGetReal(NULL,newdefs->prefix,"-bubble_r",&newdefs->bubble_r,&flg);CHKERRQ(ierr);
  newdefs->bubble_x = 0.5;
  ierr = PetscOptionsGetReal(NULL,newdefs->prefix,"-bubble_x",&newdefs->bubble_x,&flg);CHKERRQ(ierr);
  newdefs->bubble_y = 0.5;
  ierr = PetscOptionsGetReal(NULL,newdefs->prefix,"-bubble_y",&newdefs->bubble_y,&flg);CHKERRQ(ierr);
  newdefs->bubble_z = 0.5;
  ierr = PetscOptionsGetReal(NULL,newdefs->prefix,"-bubble_z",&newdefs->bubble_z,&flg);CHKERRQ(ierr);
  newdefs->dtemp0  = 1.0;
  ierr = PetscOptionsGetReal(NULL,newdefs->prefix,"-dtemp0",&newdefs->dtemp0,&flg);CHKERRQ(ierr);

  switch (newdefs->ptype) {
    case PTYPE_LDC:
      newdefs->fluidref.u = 1;
      newdefs->fluidref.w = 1;
      newdefs->initFunc = InitialCondition_LidDrivenCavity;
      newdefs->leftBC  = DIRICHLET;  newdefs->rightBC = DIRICHLET;  newdefs->xPeriodic = PETSC_FALSE;
      newdefs->frontBC = PERIODIC;   newdefs->backBC  = PERIODIC;   newdefs->yPeriodic = PETSC_TRUE;
      newdefs->downBC  = DIRICHLET;  newdefs->upBC    = DIRICHLET;  newdefs->zPeriodic = PETSC_FALSE;
      break;

    case PTYPE_TGV:
      newdefs->fluidref.u = 1;
      newdefs->initFunc = InitialCondition_TaylorGreenVortex;
      newdefs->leftBC  = PERIODIC;  newdefs->rightBC = PERIODIC;  newdefs->xPeriodic = PETSC_TRUE;
      newdefs->frontBC = PERIODIC;  newdefs->backBC  = PERIODIC;  newdefs->yPeriodic = PETSC_TRUE;
      newdefs->downBC  = PERIODIC;  newdefs->upBC    = PERIODIC;  newdefs->zPeriodic = PETSC_TRUE;
      break;

    case PTYPE_WDF:
      newdefs->initFunc = InitialCondition_WindDrivenFlow;
      switch (newdefs->couplingSide) {
        case DOWN:
          newdefs->fluidref.u = 0.1; 
          newdefs->xPeriodic = PETSC_TRUE;   newdefs->leftBC  = PERIODIC;  newdefs->rightBC = PERIODIC;
          newdefs->yPeriodic = PETSC_TRUE;   newdefs->frontBC = PERIODIC;  newdefs->backBC  = PERIODIC;
          newdefs->zPeriodic = PETSC_FALSE;  newdefs->downBC  = COUPLING;  newdefs->upBC    = DIRICHLET;
          break;
          
        case UP:
          newdefs->xPeriodic = PETSC_FALSE;  newdefs->leftBC  = DIRICHLET;  newdefs->rightBC = DIRICHLET;
          newdefs->yPeriodic = PETSC_TRUE;   newdefs->frontBC = PERIODIC;   newdefs->backBC  = PERIODIC;
          newdefs->zPeriodic = PETSC_FALSE;  newdefs->downBC  = DIRICHLET;  newdefs->upBC    = COUPLING;
          break;
        default:
          SETERRQ(PETSC_COMM_WORLD, PETSC_ERR_ARG_INCOMP, "Coupling supported only at the down/bottom or up/top boundaries");
      }
      break;
    case PTYPE_HYDROSTATIC: 
      newdefs->initFunc = InitialCondition_HydroStatic;
      switch (newdefs->couplingSide) {
        case DOWN:
          newdefs->xPeriodic = PETSC_FALSE; newdefs->leftBC  = DIRICHLET; newdefs->rightBC = DIRICHLET;
          newdefs->yPeriodic = PETSC_FALSE; newdefs->frontBC = DIRICHLET; newdefs->backBC  = DIRICHLET;
          newdefs->zPeriodic = PETSC_FALSE; newdefs->downBC  = COUPLING;  newdefs->upBC     = DIRICHLET;
          break;
        case UP:
          newdefs->xPeriodic = PETSC_FALSE;   newdefs->leftBC  = DIRICHLET;  newdefs->rightBC = DIRICHLET;
          newdefs->yPeriodic = PETSC_FALSE;   newdefs->frontBC = DIRICHLET;  newdefs->backBC  = DIRICHLET;
          newdefs->zPeriodic = PETSC_FALSE;   newdefs->downBC  = DIRICHLET;  newdefs->upBC    = COUPLING;
          break;
        case LEFT: 
        case RIGHT:
        case FRONT:
        case BACK:
          SETERRQ(PETSC_COMM_WORLD, PETSC_ERR_ARG_INCOMP, "Coupling supported only at the down/bottom or up/top boundaries");
          break;
        default: /* no coupling at all */
          newdefs->xPeriodic = PETSC_FALSE;   newdefs->leftBC  = DIRICHLET;  newdefs->rightBC = DIRICHLET;
          newdefs->yPeriodic = PETSC_FALSE;   newdefs->frontBC = DIRICHLET;  newdefs->backBC  = DIRICHLET;
          newdefs->zPeriodic = PETSC_FALSE;   newdefs->downBC  = DIRICHLET;  newdefs->upBC    = DIRICHLET;
          break;
      }
      break; 
    case PTYPE_THERMALBUBBLE:
      newdefs->initFunc = InitialCondition_RisingThermalBubble; 
      switch (newdefs->couplingSide) {
        case DOWN:
          newdefs->xPeriodic = PETSC_FALSE;   newdefs->leftBC  = DIRICHLET;  newdefs->rightBC = DIRICHLET;
          newdefs->yPeriodic = PETSC_FALSE;   newdefs->frontBC = DIRICHLET;  newdefs->backBC  = DIRICHLET;
          newdefs->zPeriodic = PETSC_FALSE;   newdefs->downBC  = COUPLING;  newdefs->upBC    = DIRICHLET;
          break;
        case UP:
          newdefs->xPeriodic = PETSC_FALSE;   newdefs->leftBC  = DIRICHLET;  newdefs->rightBC = DIRICHLET;
          newdefs->yPeriodic = PETSC_FALSE;   newdefs->frontBC = DIRICHLET;  newdefs->backBC  = DIRICHLET;
          newdefs->zPeriodic = PETSC_FALSE;   newdefs->downBC  = DIRICHLET;  newdefs->upBC    = COUPLING;
          break;
        case LEFT: 
        case RIGHT:
        case FRONT:
        case BACK:
          SETERRQ(PETSC_COMM_WORLD, PETSC_ERR_ARG_INCOMP, "Coupling supported only at the down/bottom or up/top boundaries");
          break;
        default: /* no coupling at all */
          newdefs->xPeriodic = PETSC_FALSE;   newdefs->leftBC  = DIRICHLET;  newdefs->rightBC = DIRICHLET;
          newdefs->yPeriodic = PETSC_FALSE;   newdefs->frontBC = DIRICHLET;  newdefs->backBC  = DIRICHLET;
          newdefs->zPeriodic = PETSC_FALSE;   newdefs->downBC  = DIRICHLET;  newdefs->upBC    = DIRICHLET;
          break;
      }
      break; 
    default:
      SETERRQ(PETSC_COMM_WORLD, PETSC_ERR_ARG_UNKNOWN_TYPE, "Invalid problem type");
      break;
  }
  *defs = newdefs;
  PetscFunctionReturn(0);
}

PetscErrorCode ProblemDefsDestroy(ProblemDefs** defs)
{
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PetscFree((*defs)->outdir);CHKERRQ(ierr);
  ierr = PetscFree((*defs)->prefix);CHKERRQ(ierr);
  ierr = VecDestroy(&(*defs)->exnerpres);CHKERRQ(ierr);
  ierr = PetscFree(*defs);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode ProblemDefsView(ProblemDefs* defs, PetscViewer viewer)
{
  PetscBool isASCII;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PetscObjectTypeCompare((PetscObject)viewer, PETSCVIEWERASCII, &isASCII);CHKERRQ(ierr);
  if (!isASCII) PetscFunctionReturn(0);

  ierr = PetscViewerASCIIPrintf(viewer, "ProblemDefs object: %s\n", defs->prefix);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPushTab(viewer);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "type: %s\n", ProblemTypes[defs->ptype]);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "viscous dissippation:  alpha = %.4f\n", defs->visc_diss_alpha);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "viscosity:             mu    = %.4f\n", defs->mu);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "specific gravity:      gamma = %.4f\n", defs->gamma);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "Prandtl number:        pr    = %.4f\n", defs->pr);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "Reynolds number:       re    = %.4f\n", defs->re);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "Gravity (dim):         g     = %.4f\n", defs->g_dim);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "Gravity (nondim):      g     = %.4f\n", defs->g);CHKERRQ(ierr);

  ierr = PetscViewerASCIIPrintf(viewer, "Reference States \n");CHKERRQ(ierr);
  ierr = PetscViewerASCIIPushTab(viewer);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "velocity (nondim): u0    = %.4e\n", defs->fluidref.u);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "                   v0    = %.4e\n", defs->fluidref.v);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "                   w0    = %.4e\n", defs->fluidref.w);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "density:           rho0  = %.4e\n", defs->fluidref.density);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "pressure:          pres0 = %.4e\n", defs->fluidref.pressure);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "temperature:       temp0 = %.4e\n", defs->fluidref.temp);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPopTab(viewer);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "Non-Dimensionalization:\n");CHKERRQ(ierr);
  ierr = PetscViewerASCIIPushTab(viewer);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "density:      %.4e\n", defs->nondim.density);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "pressure:     %.4e\n", defs->nondim.pressure);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "temperature:  %.4e\n", defs->nondim.temp);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "length:       %.4e\n", defs->nondim.L);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPopTab(viewer);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "boundary conditions:\n");CHKERRQ(ierr);
  ierr = PetscViewerASCIIPushTab(viewer);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "left  (x-):  %s\n", BoundaryTypes[defs->leftBC]);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "right (x+):  %s\n", BoundaryTypes[defs->rightBC]);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "front (y-):  %s\n", BoundaryTypes[defs->frontBC]);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "back  (y+):  %s\n", BoundaryTypes[defs->backBC]);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "down  (z-):  %s\n", BoundaryTypes[defs->downBC]);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer, "up    (z+):  %s\n", BoundaryTypes[defs->upBC]);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPopTab(viewer);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPopTab(viewer);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}