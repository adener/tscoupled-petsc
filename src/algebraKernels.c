#include "fvns3d.h"

PetscErrorCode SolutionView(MPI_Comm comm, Solution* X)
{
  PetscMPIInt rank;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = MPI_Comm_rank(comm, &rank);
  ierr = PetscPrintf(PETSC_COMM_SELF, "[%D] rho  = %0.2e\n", rank, X->rho);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_SELF, "[%D] rhoU = %0.2e\n", rank, X->rhoU);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_SELF, "[%D] rhoV = %0.2e\n", rank, X->rhoV);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_SELF, "[%D] rhoW = %0.2e\n", rank, X->rhoW);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_SELF, "[%D] rhoE = %0.2e\n", rank, X->rhoE);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscBool SolutionCheck(PetscBool checkZero, Solution* X)
{
  PetscBool badVal = PETSC_FALSE;
  if (PetscIsInfOrNanScalar((X->rho))) badVal = PETSC_TRUE;
  if (checkZero && X->rho == 0.0) badVal = PETSC_TRUE;
  if (PetscIsInfOrNanScalar((X->rhoU))) badVal = PETSC_TRUE;
  if (PetscIsInfOrNanScalar((X->rhoV))) badVal = PETSC_TRUE;
  if (PetscIsInfOrNanScalar((X->rhoW))) badVal = PETSC_TRUE;
  if (PetscIsInfOrNanScalar((X->rhoE))) badVal = PETSC_TRUE;
  return badVal;
}

/* Y = X */
void SolutionCopy(Solution* X, Solution* Y)
{
  Y->rho  = X->rho;
  Y->rhoU = X->rhoU;
  Y->rhoV = X->rhoV;
  Y->rhoW = X->rhoW;
  Y->rhoE = X->rhoE;
}

/* Z = a*X + b*Y */
void SolutionAXPBY(PetscScalar a, Solution* X, PetscScalar b, Solution* Y, Solution* Z)
{
  Z->rho  = a*X->rho  + b*Y->rho;
  Z->rhoU = a*X->rhoU + b*Y->rhoU;
  Z->rhoV = a*X->rhoV + b*Y->rhoV;
  Z->rhoW = a*X->rhoW + b*Y->rhoW;
  Z->rhoE = a*X->rhoE + b*Y->rhoE;
}

/* W = a*X + b*Y + c*Z */
void SolutionAXPBYPCZ(PetscScalar a, Solution* X, PetscScalar b, Solution* Y, PetscScalar c, Solution* Z, Solution* W)
{
  SolutionAXPBY(a, X, b, Y, W);
  SolutionAXPBY(c, Z, 1.0, W, W);
}

PetscErrorCode PrimitivesView(MPI_Comm comm, Primitives* X)
{
  PetscMPIInt rank;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = MPI_Comm_rank(comm, &rank);
  ierr = PetscPrintf(PETSC_COMM_SELF, "[%D] r = ", rank);
  ierr = PetscPrintf(PETSC_COMM_SELF, "%0.2e\n", X->r);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_SELF, "[%D] u = ", rank);
  ierr = PetscPrintf(PETSC_COMM_SELF, "%0.2e\n", X->u);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_SELF, "[%D] v = ", rank);
  ierr = PetscPrintf(PETSC_COMM_SELF, "%0.2e\n", X->v);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_SELF, "[%D] w = ", rank);
  ierr = PetscPrintf(PETSC_COMM_SELF, "%0.2e\n", X->w);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_SELF, "[%D] p = ", rank);
  ierr = PetscPrintf(PETSC_COMM_SELF, "%0.2e\n", X->p);CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_SELF, "[%D] e = ", rank);
  ierr = PetscPrintf(PETSC_COMM_SELF, "%0.2e\n", X->e);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/* Y = X */
void PrimitivesCopy(Primitives* X, Primitives* Y)
{
  Y->r = X->r;
  Y->u = X->u;
  Y->v = X->v;
  Y->w = X->w;
  Y->p = X->p;
  Y->e = X->e;
}

/* Z = a*X + b*Y */
void PrimitivesAXPBY(PetscScalar a, Primitives* X, PetscScalar b, Primitives* Y, Primitives* Z)
{
  Z->r = a*X->r + b*Y->r;
  Z->u = a*X->u + b*Y->u;
  Z->v = a*X->v + b*Y->v;
  Z->w = a*X->w + b*Y->w;
  Z->p = a*X->p + b*Y->p;
  Z->e = a*X->e + b*Y->e;
}

/* W = a*X + b*Y + c*Z */
void PrimitivesAXPBYPCZ(PetscScalar a, Primitives* X, PetscScalar b, Primitives* Y, PetscScalar c, Primitives* Z, Primitives* W)
{
  PrimitivesAXPBY(a, X, b, Y, W);
  PrimitivesAXPBY(c, Z, 1.0, W, W);
}

/* IN: gamma, qq */
/* OUT: prims */
void ComputePrimitives(PetscScalar gamma, Solution* qq, Primitives* prims)
{
  prims->r = qq->rho;
  prims->u = qq->rhoU/qq->rho;
  prims->v = qq->rhoV/qq->rho;
  prims->w = qq->rhoW/qq->rho;
  prims->e = qq->rhoE/qq->rho - 0.5*(prims->u*prims->u + prims->v*prims->v + prims->w*prims->w);
  prims->p = (gamma - 1.0)*prims->r*prims->e;
}