#include "fvns3d.h"

/* IN:  xGradP, xGradN, yGradP, yGradN, zGradP, zGradN, fluxP, fluxN, gamma, tauV */
/* OUT: cPrim, cPrimX, cPrimY, cPrimZ */
static void ComputeCommonState(
  Primitives* xGradP, Primitives* xGradN,
  Primitives* yGradP, Primitives* yGradN,
  Primitives* zGradP, Primitives* zGradN,
  Primitives* fluxP, Primitives* fluxN, PetscScalar gamma, PetscScalar tauV,
  Primitives* cPrim, Primitives* cPrimX, Primitives* cPrimY, Primitives* cPrimZ)
{
  PetscScalar a, aP, aN;
  Primitives work1, work2;

  aP = PetscSqrtScalar(gamma*fluxP->p/fluxP->r);
  aN = PetscSqrtScalar(gamma*fluxN->p/fluxN->r);
  a = 0.5*(aP + aN);

  PrimitivesAXPBY(0.5, fluxP, 0.5, fluxN, cPrim);
  cPrim->e = a*a/(gamma*(gamma - 1.0));

  PrimitivesAXPBY(1.0, fluxN, -1.0, fluxP, &work1);
  PrimitivesAXPBY(1.0, xGradN, 1.0, xGradP, &work2);
  PrimitivesAXPBY(0.5, &work2, tauV, &work1, cPrimX);
  cPrimX->e = (gamma*cPrimX->p - a*a*cPrimX->r)/(gamma*(gamma - 1.0)*cPrim->r);

  PrimitivesAXPBY(1.0, yGradN, 1.0, yGradP, &work1);
  PrimitivesAXPBY(0.5, &work1, tauV, &work2, cPrimY);
  cPrimY->e = (gamma*cPrimY->p - a*a*cPrimY->r)/(gamma*(gamma - 1.0)*cPrim->r);

  PrimitivesAXPBY(1.0, zGradN, 1.0, zGradP, &work1);
  PrimitivesAXPBY(0.5, &work1, tauV, &work2, cPrimZ);
  cPrimZ->e = (gamma*cPrimZ->p - a*a*cPrimZ->r)/(gamma*(gamma - 1.0)*cPrim->r);
}

/* IN:  gamma, mu, pr, faces, cPrim, cPrimX, cPrimY, cPrimZ */
/* OUT: fluxV */
static PetscErrorCode ComputeVFlux_Each(
  PetscScalar gamma, PetscScalar mu, PetscScalar pr, DMDirection faces, 
  Primitives* cPrim, Primitives* cPrimX, Primitives* cPrimY, Primitives* cPrimZ,
  Solution* fluxV)
{
  PetscScalar tau_xx, tau_yy, tau_zz;
  PetscScalar tau_xy, tau_xz, tau_yz;
  PetscScalar q_x, q_y, q_z;
  Solution flux1, flux2, flux3;
  PetscReal nx, ny, nz;

  PetscFunctionBeginUser;
  switch (faces) {
    case DM_X:
      nx = 1.0;  ny = 0.0;  nz = 0.0;
      break;
    case DM_Y:
      nx = 0.0;  ny = 1.0;  nz = 0.0;
      break;
    case DM_Z:
      nx = 0.0;  ny = 0.0;  nz = 1.0;
      break;
    default:
      SETERRQ(PETSC_COMM_WORLD, PETSC_ERR_ARG_WRONG,
              "ComputeIFlux_Each: Invalid face group");
  }
  
  /*
          [xx xy xz]
    tau = [yx yy yz]
          [zx zy zz]
   */
  tau_xx = mu*(2.0/3.0)*(2.0*cPrimX->u - cPrimY->v - cPrimZ->w);
  tau_yy = mu*(2.0/3.0)*(2.0*cPrimY->v - cPrimX->u - cPrimZ->w);
  tau_zz = mu*(2.0/3.0)*(2.0*cPrimZ->w - cPrimX->u - cPrimY->v);

  tau_xy = mu*(cPrimX->v + cPrimY->u);
  tau_xz = mu*(cPrimX->w + cPrimZ->u);
  tau_yz = mu*(cPrimY->w + cPrimZ->v);

  q_x = -gamma*mu*cPrimX->e/pr;
  q_y = -gamma*mu*cPrimY->e/pr;
  q_z = -gamma*mu*cPrimZ->e/pr;

  flux1.rho  = 0.0;
  flux1.rhoU = tau_xx;
  flux1.rhoV = tau_xy;
  flux1.rhoW = tau_xz;
  flux1.rhoE = tau_xx*cPrim->u + tau_xy*cPrim->v + tau_xz*cPrim->w - q_x;

  flux2.rho  = 0.0;
  flux2.rhoU = tau_xy;
  flux2.rhoV = tau_yy;
  flux2.rhoW = tau_yz;
  flux2.rhoE = tau_xy*cPrim->u + tau_yy*cPrim->v + tau_yz*cPrim->w - q_y;

  flux3.rho = 0.0;
  flux3.rhoU = tau_xz;
  flux3.rhoV = tau_yz;
  flux3.rhoW = tau_zz;
  flux3.rhoE = tau_xz*cPrim->u + tau_yz*cPrim->v + tau_zz*cPrim->w - q_z;

  SolutionAXPBYPCZ(nx, &flux1, ny, &flux2, nz, &flux3, fluxV);
  PetscFunctionReturn(0);
}

/* IN:  defs, qp, xGrad, yGrad, zGrad */
/* OUT: xFluxV, yFluxV, zFluxV */
PetscErrorCode ComputeVFlux(
  FVNS* pde, PetscInt xi, PetscInt yi, PetscInt zi,
  Solution* xFluxV, Solution* yFluxV, Solution* zFluxV)
{
  PetscScalar gamma=pde->defs->gamma;
  PetscScalar alpha=pde->defs->visc_diss_alpha;
  PetscScalar mu=pde->defs->mu, pr=pde->defs->pr;
  PetscReal dx=pde->dx, dy=pde->dy, dz=pde->dz;
  Primitives cPrim, cPrimX, cPrimY, cPrimZ;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  /* flux contribution to the left/right elements */
  ComputeCommonState(
    &pde->xg[zi][yi][xi-1], &pde->xg[zi][yi][xi],
    &pde->yg[zi][yi][xi-1], &pde->yg[zi][yi][xi],
    &pde->zg[zi][yi][xi-1], &pde->zg[zi][yi][xi],
    &pde->pf_L[zi][yi][xi], &pde->pf_R[zi][yi][xi], gamma, alpha/dx,
    &cPrim, &cPrimX, &cPrimY, &cPrimZ);
  ierr = ComputeVFlux_Each(
    gamma, mu, pr, DM_X, &cPrim, &cPrimX, &cPrimY, &cPrimZ, xFluxV);CHKERRQ(ierr);

  /* flux contribution to the front/back elements */
  ComputeCommonState(
    &pde->xg[zi][yi-1][xi], &pde->xg[zi][yi][xi],
    &pde->yg[zi][yi-1][xi], &pde->yg[zi][yi][xi],
    &pde->zg[zi][yi-1][xi], &pde->zg[zi][yi][xi],
    &pde->pf_F[zi][yi][xi], &pde->pf_B[zi][yi][xi], gamma, alpha/dy,
    &cPrim, &cPrimX, &cPrimY, &cPrimZ);
  ierr = ComputeVFlux_Each(
    gamma, mu, pr, DM_Y, &cPrim, &cPrimX, &cPrimY, &cPrimZ, yFluxV);CHKERRQ(ierr);

  /* flux contribution to the down/up elements */
  ComputeCommonState(
    &pde->xg[zi-1][yi][xi], &pde->xg[zi][yi][xi],
    &pde->yg[zi-1][yi][xi], &pde->yg[zi][yi][xi],
    &pde->zg[zi-1][yi][xi], &pde->zg[zi][yi][xi],
    &pde->pf_D[zi][yi][xi], &pde->pf_U[zi][yi][xi], gamma, alpha/dz,
    &cPrim, &cPrimX, &cPrimY, &cPrimZ);
  ierr = ComputeVFlux_Each(
    gamma, mu, pr, DM_Z, &cPrim, &cPrimX, &cPrimY, &cPrimZ, zFluxV);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}