#include "fvns3d.h"

PetscErrorCode FVNSComputeInitialCondition(FVNS* pde, Vec QQ)
{
  ProblemDefs *defs=pde->defs;
  DMDACoor3d ***coors;
  Solution ***qq;
  PetscInt xi, yi, zi, xstart, ystart, zstart, nxe, nye, nze;
  PetscReal xc, yc, zc;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = VecZeroEntries(QQ);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(pde->dm, QQ, &qq);CHKERRQ(ierr);

  ierr = DMDAGetCoordinateArray(pde->dm, &coors);CHKERRQ(ierr);
  ierr = DMDAGetCorners(pde->dm, &xstart, &ystart, &zstart, &nxe, &nye, &nze);CHKERRQ(ierr);
  for (zi=zstart; zi<zstart+nze; zi++) {
    for (yi=ystart; yi<ystart+nye; yi++) {
      for (xi=xstart; xi<xstart+nxe; xi++) {
        xc = coors[zi][yi][xi].x;
        yc = coors[zi][yi][xi].y;
        zc = coors[zi][yi][xi].z;
        (*defs->initFunc)(defs, xc, yc, zc, &qq[zi][yi][xi]);
      }
    }
  }
  
  ierr = DMDARestoreCoordinateArray(pde->dm, &coors);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(pde->dm, QQ, &qq);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


PetscErrorCode FVNSViewExnerPressureProfile(FVNS* pde)
{
  ProblemDefs *defs=pde->defs;
  FluidReferenceStates  ref    = defs->fluidref; 
  NonDimensionalization nondim = defs->nondim; 
  DMDACoor3d ***coors;
  PetscInt  zi, xstart, ystart, zstart, nxe, nye, nze;
  PetscReal zc;
  PetscScalar exnerp,gamma;
  PetscViewer viewer;
  PetscBool   flg; 
  PetscMPIInt size;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = MPI_Comm_size(pde->comm, &size);CHKERRQ(ierr);
  if (size > 1) PetscFunctionReturn(0);
  ierr = DMDAGetCoordinateArray(pde->dm, &coors);CHKERRQ(ierr);
  ierr = DMDAGetCorners(pde->dm, &xstart, &ystart, &zstart, &nxe, &nye, &nze);CHKERRQ(ierr);
  ierr = VecCreateSeq(PETSC_COMM_SELF,nze,&defs->exnerpres);CHKERRQ(ierr);
  gamma = defs->gamma;

  for (zi=zstart; zi<zstart+nze; zi++) {
    zc = coors[zi][0][0].z;
    exnerp =  - defs->g*(nondim.L*zc) / (1/(gamma-1)*ref.temp);
    VecSetValue(defs->exnerpres,zi,exnerp,INSERT_VALUES);CHKERRQ(ierr);
  }
  ierr = VecAssemblyBegin(defs->exnerpres);CHKERRQ(ierr);
  ierr = VecAssemblyEnd(defs->exnerpres);CHKERRQ(ierr);
  ierr = DMDARestoreCoordinateArray(pde->dm, &coors);CHKERRQ(ierr);

  ierr = PetscOptionsGetViewer(pde->comm,NULL,defs->prefix,"-exner_view",&viewer,NULL,&flg);CHKERRQ(ierr);

  if (flg) {
    ierr = VecView(defs->exnerpres,viewer);CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  }

  PetscFunctionReturn(0);
}

/*
  implementation cloned from 11-Coupled-CNS2D-WDF/CNS2DRHS.jl line 5614
  ComputeRHS_FV2_LF_VISC_conservative()
*/
PetscErrorCode FVNSComputeRHS(FVNS* pde, Vec QQ, Vec RHS)
{
  DMDALocalInfo info;
  Solution ***qq, ***rhs;
  Solution xFluxI, yFluxI, zFluxI, xFluxV, yFluxV, zFluxV;
  PetscReal dx=pde->dx, dy=pde->dy, dz=pde->dz;
  PetscInt xi, yi, zi, xstart, xend, ystart, yend, zstart, zend;
  PetscBool flg, checkSol=PETSC_FALSE, checkRHS=PETSC_FALSE;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PetscOptionsGetBool(NULL,pde->defs->prefix,"-check_sol",&checkSol,&flg);
  if (checkSol) {
    ierr = FVNSCheckSolution(pde, PETSC_TRUE, PETSC_TRUE, QQ);CHKERRQ(ierr);
  }
  /* communicate global solution into local components and extract array */
  ierr = DMGlobalToLocalBegin(pde->dm, QQ, INSERT_VALUES, pde->QQloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(pde->dm, QQ, INSERT_VALUES, pde->QQloc);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(pde->dm, pde->QQloc, &qq);CHKERRQ(ierr);

  /* prepare the solution and gradients for RHS computation */
  ierr = ApplyPhysicalBoundaryConditions(pde, qq);CHKERRQ(ierr);
  ierr = ComputePrimitiveGradients(pde, qq);CHKERRQ(ierr);
  ierr = ReconstructSolutionOnFaces(pde, qq);CHKERRQ(ierr);

  /* get zeroed local array for the RHS vector */
  ierr = VecZeroEntries(RHS);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(pde->dm, RHS, &rhs);CHKERRQ(ierr);

  /* iterate over elements but operate only on the left, front and bottom faces

   * x-direction view:
   *       _________________________________
   *      /          /          /          /|
   *     /          /          /          / |
   *    /__________/__________/__________/  |
   *    |          |          |          |  |
   *    |   xi-1   |    xi    |   xi+1   |  /
   *    |          |          |          | /
   *    |__________|__________|__________|/
                  L^

   * y-direction view:
   *                     ___________
   *                    /          /|
   *                   /   yi+1   / |
   *                  /__________/  |
   *                 /          /|  |
   *                /    yi    / |  /
   *           F > /__________/  | /
   *              /          /|  |/
   *             /   yi-1   / |  /
   *            /__________/  | /
   *            |          |  |/
   *            |          |  /
   *            |          | /
   *            |__________|/

   * z-direction view:
   *                  ___________
   *                 /          /|
   *                /          / |
   *               /__________/  |
   *               |          |  |
   *               |   zi+1   | /|
   *               |          |/ |
   *               |__________|  |
   *               |          |  |
   *               |    zi    | /|
   *               |          |/ |
   *           B > |__________|  |
   *               |          |  |
   *               |   zi-1   |  /
   *               |          | /
   *               |__________|/

  */
  ierr = DMDAGetLocalInfo(pde->dm, &info);CHKERRQ(ierr);
  xstart = info.xs;  xend = info.xs + info.xm;
  ystart = info.ys;  yend = info.ys + info.ym;
  zstart = info.zs;  zend = info.zs + info.zm;
  for (zi=zstart; zi<zend; zi++) {
    for (yi=ystart; yi<yend; yi++) {
      for (xi=xstart; xi<xend; xi++) {
        /* 
          compute inviscid flux contributions to the left/right (x-dir)
          and bottom/top (y-dir) elements surrounding the (xi, yi) face
        */
        ierr = ComputeIFlux(pde, xi, yi, zi, &xFluxI, &yFluxI, &zFluxI);CHKERRQ(ierr);
        /* 
          compute viscous flux contributions to the left/right (x-dir)
          and bottom/top (y-dir) elements surrounding the (xi, yi) face
        */
        ierr = ComputeVFlux(pde, xi, yi, zi, &xFluxV, &yFluxV, &zFluxV);CHKERRQ(ierr);
        /* add left-face flux contribution contribution to left element */
        if (xi != 0 && xi != xstart) {
          rhs[zi][yi][xi-1].rho  += (xFluxV.rho  - xFluxI.rho)/dx;
          rhs[zi][yi][xi-1].rhoU += (xFluxV.rhoU - xFluxI.rhoU)/dx;
          rhs[zi][yi][xi-1].rhoV += (xFluxV.rhoV - xFluxI.rhoV)/dx;
          rhs[zi][yi][xi-1].rhoW += (xFluxV.rhoW - xFluxI.rhoW)/dx;
          rhs[zi][yi][xi-1].rhoE += (xFluxV.rhoE - xFluxI.rhoE)/dx;
        }
        
      
        if (xi == 0 && pde->defs->leftBC == DIRICHLET_RIEMANN_INVAR) {
        /* New Dirichlet BC Modification, uses different flux */ 

        } else {
          /* add left-face flux contribution to the right (central) element */
          rhs[zi][yi][xi].rho  -= (xFluxV.rho  - xFluxI.rho)/dx;
          rhs[zi][yi][xi].rhoU -= (xFluxV.rhoU - xFluxI.rhoU)/dx;
          rhs[zi][yi][xi].rhoV -= (xFluxV.rhoV - xFluxI.rhoV)/dx;
          rhs[zi][yi][xi].rhoW -= (xFluxV.rhoW - xFluxI.rhoW)/dx;
          rhs[zi][yi][xi].rhoE -= (xFluxV.rhoE - xFluxI.rhoE)/dx;
        }

        /* add front-face flux contribution to the bottom element */
        if (yi != 0 && yi != ystart) {
          rhs[zi][yi-1][xi].rho  += (yFluxV.rho  - yFluxI.rho)/dy;
          rhs[zi][yi-1][xi].rhoU += (yFluxV.rhoU - yFluxI.rhoU)/dy;
          rhs[zi][yi-1][xi].rhoV += (yFluxV.rhoV - yFluxI.rhoV)/dy;
          rhs[zi][yi-1][xi].rhoW += (yFluxV.rhoW - yFluxI.rhoW)/dy;
          rhs[zi][yi-1][xi].rhoE += (yFluxV.rhoE - yFluxI.rhoE)/dy;
        }
        if (yi == 0 && pde->defs->frontBC == DIRICHLET_RIEMANN_INVAR) {
        /* New Dirichlet BC Modification, uses different flux */ 

        } else {
          /* add front-face flux contribution to the top (central) element */
          rhs[zi][yi][xi].rho  -= (yFluxV.rho  - yFluxI.rho)/dy;
          rhs[zi][yi][xi].rhoU -= (yFluxV.rhoU - yFluxI.rhoU)/dy;
          rhs[zi][yi][xi].rhoV -= (yFluxV.rhoV - yFluxI.rhoV)/dy;
          rhs[zi][yi][xi].rhoW -= (yFluxV.rhoW - yFluxI.rhoW)/dy;
          rhs[zi][yi][xi].rhoE -= (yFluxV.rhoE - yFluxI.rhoE)/dy;
        }


        /* add bottom-face flux contribution to the front element */
        if (zi != 0 && zi != zstart) {
          rhs[zi-1][yi][xi].rho  += (zFluxV.rho  - zFluxI.rho)/dz;
          rhs[zi-1][yi][xi].rhoU += (zFluxV.rhoU - zFluxI.rhoU)/dz;
          rhs[zi-1][yi][xi].rhoV += (zFluxV.rhoV - zFluxI.rhoV)/dz;
          rhs[zi-1][yi][xi].rhoW += (zFluxV.rhoW - zFluxI.rhoW)/dz;
          rhs[zi-1][yi][xi].rhoE += (zFluxV.rhoE - zFluxI.rhoE)/dz;
        }
        if (zi == 0 && pde->defs->downBC == DIRICHLET_RIEMANN_INVAR) {
        } else {
          /* add bottom-face flux contribution to the back (central) element */
          rhs[zi][yi][xi].rho  -= (zFluxV.rho  - zFluxI.rho)/dz;
          rhs[zi][yi][xi].rhoU -= (zFluxV.rhoU - zFluxI.rhoU)/dz;
          rhs[zi][yi][xi].rhoV -= (zFluxV.rhoV - zFluxI.rhoV)/dz;
          rhs[zi][yi][xi].rhoW -= (zFluxV.rhoW - zFluxI.rhoW)/dz;
          rhs[zi][yi][xi].rhoE -= (zFluxV.rhoE - zFluxI.rhoE)/dz;
        }


        if (xi == info.mx -1 && pde->defs->rightBC == DIRICHLET_RIEMANN_INVAR)
        {
          /* boundary cell in the x-direction has special flux*/

        } else if (xi == xend-1) {
          /* last cell in the x-direction does contributions from the right face */
          ierr = ComputeIFlux(pde, xi+1, yi, zi, &xFluxI, &yFluxI, &zFluxI);CHKERRQ(ierr);
          ierr = ComputeVFlux(pde, xi+1, yi, zi, &xFluxV, &yFluxV, &zFluxV);CHKERRQ(ierr);
          rhs[zi][yi][xi].rho  += (xFluxV.rho  - xFluxI.rho)/dx;
          rhs[zi][yi][xi].rhoU += (xFluxV.rhoU - xFluxI.rhoU)/dx;
          rhs[zi][yi][xi].rhoV += (xFluxV.rhoV - xFluxI.rhoV)/dx;
          rhs[zi][yi][xi].rhoW += (xFluxV.rhoW - xFluxI.rhoW)/dx;
          rhs[zi][yi][xi].rhoE += (xFluxV.rhoE - xFluxI.rhoE)/dx;
        }
        if (yi == info.my -1 && pde->defs->backBC == DIRICHLET_RIEMANN_INVAR)
        {
          /* boundary cell in the y-direction has special flux*/
        } else if (yi == yend-1) {
          /* last cell in the y-direction does contributions from the back face */
          ierr = ComputeIFlux(pde, xi, yi+1, zi, &xFluxI, &yFluxI, &zFluxI);CHKERRQ(ierr);
          ierr = ComputeVFlux(pde, xi, yi+1, zi, &xFluxV, &yFluxV, &zFluxV);CHKERRQ(ierr);
          rhs[zi][yi][xi].rho  += (yFluxV.rho  - yFluxI.rho)/dy;
          rhs[zi][yi][xi].rhoU += (yFluxV.rhoU - yFluxI.rhoU)/dy;
          rhs[zi][yi][xi].rhoV += (yFluxV.rhoV - yFluxI.rhoV)/dy;
          rhs[zi][yi][xi].rhoW += (yFluxV.rhoW - yFluxI.rhoW)/dy;
          rhs[zi][yi][xi].rhoE += (yFluxV.rhoE - yFluxI.rhoE)/dy;
        }
        if (zi == info.mz -1 && pde->defs->upBC == DIRICHLET_RIEMANN_INVAR)
        {
          /* boundary cell in the z-direction has special flux*/
        } else if (zi == zend-1) {
          /* last cell in the z-direction does contributions from the top face */
          ierr = ComputeIFlux(pde, xi, yi, zi+1, &xFluxI, &yFluxI, &zFluxI);CHKERRQ(ierr);
          ierr = ComputeVFlux(pde, xi, yi, zi+1, &xFluxV, &yFluxV, &zFluxV);CHKERRQ(ierr);
          rhs[zi][yi][xi].rho  += (zFluxV.rho  - zFluxI.rho)/dz;
          rhs[zi][yi][xi].rhoU += (zFluxV.rhoU - zFluxI.rhoU)/dz;
          rhs[zi][yi][xi].rhoV += (zFluxV.rhoV - zFluxI.rhoV)/dz;
          rhs[zi][yi][xi].rhoW += (zFluxV.rhoW - zFluxI.rhoW)/dz;
          rhs[zi][yi][xi].rhoE += (zFluxV.rhoE - zFluxI.rhoE)/dz;
        }
        rhs[zi][yi][xi].rhoW -= pde->defs->g*qq[zi][yi][xi].rho;
        rhs[zi][yi][xi].rhoE -= pde->defs->g*qq[zi][yi][xi].rhoW;
      }
    }
  }
  /* restore the solution vector, we're done with it */
  ierr = DMDAVecRestoreArray(pde->dm, pde->QQloc, &qq);CHKERRQ(ierr);
  /* restore RHS array back into local vector and communicate to global */
  ierr = DMDAVecRestoreArray(pde->dm, RHS, &rhs);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(NULL,pde->defs->prefix,"-check_rhs",&checkRHS,&flg);
  if (checkRHS) {
    ierr = FVNSCheckSolution(pde, PETSC_TRUE, PETSC_FALSE, RHS);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

/* only compute for z > bufferz and the output offset to account 
for the RHS vector having the structure of a subvec of QQ with 
IS corresponding to the dofs with z > bufferz */

PetscErrorCode FVNSComputeRHS_TOP(FVNS* pde, Vec QQ, Vec RHS)
{
  DMDALocalInfo info;
  Solution ***qq, ***rhs;
  Solution xFluxI, yFluxI, zFluxI, xFluxV, yFluxV, zFluxV;
  PetscReal dx=pde->dx, dy=pde->dy, dz=pde->dz;
  PetscInt xi, yi, zi, xstart, xend, ystart, yend, zstart, zend,bufferz = pde->multictx.bufferz,zoffset;
  PetscInt       xs,ys,zs,xm,ym,zm,dof;
  PetscBool flg, checkSol=PETSC_FALSE, checkRHS=PETSC_FALSE;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PetscOptionsGetBool(NULL,pde->defs->prefix,"-check_sol",&checkSol,&flg);
  if (checkSol) {
    ierr = FVNSCheckSolution(pde, PETSC_TRUE, PETSC_TRUE, QQ);CHKERRQ(ierr);
  }
  /* communicate global solution into local components and extract array */
  ierr = DMGlobalToLocalBegin(pde->dm, QQ, INSERT_VALUES, pde->QQloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(pde->dm, QQ, INSERT_VALUES, pde->QQloc);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(pde->dm, pde->QQloc, &qq);CHKERRQ(ierr);

  /* prepare the solution and gradients for RHS computation */
  ierr = ApplyPhysicalBoundaryConditions_TOP(pde, qq);CHKERRQ(ierr);
  ierr = ComputePrimitiveGradients_TOP(pde, qq);CHKERRQ(ierr);
  ierr = ReconstructSolutionOnFaces_TOP(pde, qq);CHKERRQ(ierr);

  /* get zeroed local array for the RHS vector */
  ierr = VecZeroEntries(RHS);CHKERRQ(ierr);

  /* use a hack to offset the rhs vector, so that it fits the expected 
  format of the standard code. Remember RHS is a subvec of QQ, so they don't have 
  the same layout anymore */
  //ierr = DMDAVecGetArray(pde->dm, RHS, &rhs);CHKERRQ(ierr);

  ierr = DMDAGetCorners(pde->dm,&xs,&ys,&zs,&xm,&ym,&zm);CHKERRQ(ierr);
  ierr = DMDAGetDof(pde->dm,&dof);CHKERRQ(ierr);

  /* Compute the actual amount of data in RHS */ 
  if (bufferz+1<zs) {
    /* then the entire processor is TOP and RHS is full, no change to zm */
  } else {
    zm = PetscMax(zm+zs-bufferz-1,0);
  }
  /* here we offset the rhs array to the qq array by st the input data matches the output data */
  zoffset = (bufferz >= zs) ? bufferz+1 : zs;
  ierr = VecGetArray3d(RHS,zm,ym,xm*dof,zoffset,ys,xs*dof,(PetscScalar****)&rhs);CHKERRQ(ierr); /* standard call */
  /* as we never write to rhs unless z >= bufferz +1, this will all be okay  */
  /* iterate over elements but operate only on the left, front and bottom faces

   * x-direction view:
   *       _________________________________
   *      /          /          /          /|
   *     /          /          /          / |
   *    /__________/__________/__________/  |
   *    |          |          |          |  |
   *    |   xi-1   |    xi    |   xi+1   |  /
   *    |          |          |          | /
   *    |__________|__________|__________|/
                  L^

   * y-direction view:
   *                     ___________
   *                    /          /|
   *                   /   yi+1   / |
   *                  /__________/  |
   *                 /          /|  |
   *                /    yi    / |  /
   *           F > /__________/  | /
   *              /          /|  |/
   *             /   yi-1   / |  /
   *            /__________/  | /
   *            |          |  |/
   *            |          |  /
   *            |          | /
   *            |__________|/

   * z-direction view:
   *                  ___________
   *                 /          /|
   *                /          / |
   *               /__________/  |
   *               |          |  |
   *               |   zi+1   | /|
   *               |          |/ |
   *               |__________|  |
   *               |          |  |
   *               |    zi    | /|
   *               |          |/ |
   *           B > |__________|  |
   *               |          |  |
   *               |   zi-1   |  /
   *               |          | /
   *               |__________|/

  */
  ierr = DMDAGetLocalInfo(pde->dm, &info);CHKERRQ(ierr);
  xstart = info.xs;  xend = info.xs + info.xm;
  ystart = info.ys;  yend = info.ys + info.ym;
  zstart = (pde->multictx.bufferz >= info.zs) ? pde->multictx.bufferz+1 : info.zs;
  zend = info.zs + info.zm;
  for (zi=zstart; zi<zend; zi++) {
    for (yi=ystart; yi<yend; yi++) {
      for (xi=xstart; xi<xend; xi++) {
        /* 
          compute inviscid flux contributions to the left/right (x-dir)
          and bottom/top (y-dir) elements surrounding the (xi, yi) face
        */
        ierr = ComputeIFlux(pde, xi, yi, zi, &xFluxI, &yFluxI, &zFluxI);CHKERRQ(ierr);
        /* 
          compute viscous flux contributions to the left/right (x-dir)
          and bottom/top (y-dir) elements surrounding the (xi, yi) face
        */
        ierr = ComputeVFlux(pde, xi, yi, zi, &xFluxV, &yFluxV, &zFluxV);CHKERRQ(ierr);
        /* add left-face flux contribution contribution to left element */
        if (xi != 0 && xi != xstart) {
          rhs[zi][yi][xi-1].rho  += (xFluxV.rho  - xFluxI.rho)/dx;
          rhs[zi][yi][xi-1].rhoU += (xFluxV.rhoU - xFluxI.rhoU)/dx;
          rhs[zi][yi][xi-1].rhoV += (xFluxV.rhoV - xFluxI.rhoV)/dx;
          rhs[zi][yi][xi-1].rhoW += (xFluxV.rhoW - xFluxI.rhoW)/dx;
          rhs[zi][yi][xi-1].rhoE += (xFluxV.rhoE - xFluxI.rhoE)/dx;
        }
        
        /* add left-face flux contribution to the right (central) element */
        rhs[zi][yi][xi].rho  -= (xFluxV.rho  - xFluxI.rho)/dx;
        rhs[zi][yi][xi].rhoU -= (xFluxV.rhoU - xFluxI.rhoU)/dx;
        rhs[zi][yi][xi].rhoV -= (xFluxV.rhoV - xFluxI.rhoV)/dx;
        rhs[zi][yi][xi].rhoW -= (xFluxV.rhoW - xFluxI.rhoW)/dx;
        rhs[zi][yi][xi].rhoE -= (xFluxV.rhoE - xFluxI.rhoE)/dx;

        /* add front-face flux contribution to the bottom element */
        if (yi != 0 && yi != ystart) {
          rhs[zi][yi-1][xi].rho  += (yFluxV.rho  - yFluxI.rho)/dy;
          rhs[zi][yi-1][xi].rhoU += (yFluxV.rhoU - yFluxI.rhoU)/dy;
          rhs[zi][yi-1][xi].rhoV += (yFluxV.rhoV - yFluxI.rhoV)/dy;
          rhs[zi][yi-1][xi].rhoW += (yFluxV.rhoW - yFluxI.rhoW)/dy;
          rhs[zi][yi-1][xi].rhoE += (yFluxV.rhoE - yFluxI.rhoE)/dy;
        }
        /* add front-face flux contribution to the top (central) element */
        rhs[zi][yi][xi].rho  -= (yFluxV.rho  - yFluxI.rho)/dy;
        rhs[zi][yi][xi].rhoU -= (yFluxV.rhoU - yFluxI.rhoU)/dy;
        rhs[zi][yi][xi].rhoV -= (yFluxV.rhoV - yFluxI.rhoV)/dy;
        rhs[zi][yi][xi].rhoW -= (yFluxV.rhoW - yFluxI.rhoW)/dy;
        rhs[zi][yi][xi].rhoE -= (yFluxV.rhoE - yFluxI.rhoE)/dy;

        /* add bottom-face flux contribution to the front element */
        if (zi != 0 && zi != zstart) {
          rhs[zi-1][yi][xi].rho  += (zFluxV.rho  - zFluxI.rho)/dz;
          rhs[zi-1][yi][xi].rhoU += (zFluxV.rhoU - zFluxI.rhoU)/dz;
          rhs[zi-1][yi][xi].rhoV += (zFluxV.rhoV - zFluxI.rhoV)/dz;
          rhs[zi-1][yi][xi].rhoW += (zFluxV.rhoW - zFluxI.rhoW)/dz;
          rhs[zi-1][yi][xi].rhoE += (zFluxV.rhoE - zFluxI.rhoE)/dz;
        }
        /* add bottom-face flux contribution to the back (central) element */
        rhs[zi][yi][xi].rho  -= (zFluxV.rho  - zFluxI.rho)/dz;
        rhs[zi][yi][xi].rhoU -= (zFluxV.rhoU - zFluxI.rhoU)/dz;
        rhs[zi][yi][xi].rhoV -= (zFluxV.rhoV - zFluxI.rhoV)/dz;
        rhs[zi][yi][xi].rhoW -= (zFluxV.rhoW - zFluxI.rhoW)/dz;
        rhs[zi][yi][xi].rhoE -= (zFluxV.rhoE - zFluxI.rhoE)/dz;

        /* last cell in the x-direction does contributions from the right face */
        if (xi == xend-1) {
          ierr = ComputeIFlux(pde, xi+1, yi, zi, &xFluxI, &yFluxI, &zFluxI);CHKERRQ(ierr);
          ierr = ComputeVFlux(pde, xi+1, yi, zi, &xFluxV, &yFluxV, &zFluxV);CHKERRQ(ierr);
          rhs[zi][yi][xi].rho  += (xFluxV.rho  - xFluxI.rho)/dx;
          rhs[zi][yi][xi].rhoU += (xFluxV.rhoU - xFluxI.rhoU)/dx;
          rhs[zi][yi][xi].rhoV += (xFluxV.rhoV - xFluxI.rhoV)/dx;
          rhs[zi][yi][xi].rhoW += (xFluxV.rhoW - xFluxI.rhoW)/dx;
          rhs[zi][yi][xi].rhoE += (xFluxV.rhoE - xFluxI.rhoE)/dx;
        }

        /* last cell in the y-direction does contributions from the back face */
        if (yi == yend-1) {
          ierr = ComputeIFlux(pde, xi, yi+1, zi, &xFluxI, &yFluxI, &zFluxI);CHKERRQ(ierr);
          ierr = ComputeVFlux(pde, xi, yi+1, zi, &xFluxV, &yFluxV, &zFluxV);CHKERRQ(ierr);
          rhs[zi][yi][xi].rho  += (yFluxV.rho  - yFluxI.rho)/dy;
          rhs[zi][yi][xi].rhoU += (yFluxV.rhoU - yFluxI.rhoU)/dy;
          rhs[zi][yi][xi].rhoV += (yFluxV.rhoV - yFluxI.rhoV)/dy;
          rhs[zi][yi][xi].rhoW += (yFluxV.rhoW - yFluxI.rhoW)/dy;
          rhs[zi][yi][xi].rhoE += (yFluxV.rhoE - yFluxI.rhoE)/dy;
        }

        /* last cell in the z-direction does contributions from the top face */
        if (zi == zend-1) {
          ierr = ComputeIFlux(pde, xi, yi, zi+1, &xFluxI, &yFluxI, &zFluxI);CHKERRQ(ierr);
          ierr = ComputeVFlux(pde, xi, yi, zi+1, &xFluxV, &yFluxV, &zFluxV);CHKERRQ(ierr);
          rhs[zi][yi][xi].rho  += (zFluxV.rho  - zFluxI.rho)/dz;
          rhs[zi][yi][xi].rhoU += (zFluxV.rhoU - zFluxI.rhoU)/dz;
          rhs[zi][yi][xi].rhoV += (zFluxV.rhoV - zFluxI.rhoV)/dz;
          rhs[zi][yi][xi].rhoW += (zFluxV.rhoW - zFluxI.rhoW)/dz;
          rhs[zi][yi][xi].rhoE += (zFluxV.rhoE - zFluxI.rhoE)/dz;
        }

        rhs[zi][yi][xi].rhoW -= pde->defs->g*qq[zi][yi][xi].rho;
        rhs[zi][yi][xi].rhoE -= pde->defs->g*qq[zi][yi][xi].rhoW;
      }
    }
  }

  /* restore the solution vector, we're done with it */
  ierr = DMDAVecRestoreArray(pde->dm, pde->QQloc, &qq);CHKERRQ(ierr);

  /* restore RHS array back into local vector and communicate to global */
  ierr = VecRestoreArray3d(RHS,zm,ym,xm*dof,zoffset,ys,xs*dof,(PetscScalar****)&rhs);CHKERRQ(ierr); 
  ierr = PetscOptionsGetBool(NULL,pde->defs->prefix,"-check_rhs",&checkRHS,&flg);
  if (checkRHS) {
    ierr = FVNSCheckSolution(pde, PETSC_TRUE, PETSC_FALSE, RHS);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

/* copy of FVNSComputeRHS_TOP with log events added for debugging */

PetscLogEvent TOP_RHS_GL, TOP_RHS_BC, TOP_RHS_GRAD, TOP_RHS_RECON, TOP_RHS_EVAL, TOP_RHS_OFFSET;


PetscErrorCode FVNSComputeRHS_TOP_DEBUG(FVNS* pde, Vec QQ, Vec RHS)
{
  DMDALocalInfo info;
  Solution ***qq, ***rhs;
  Solution xFluxI, yFluxI, zFluxI, xFluxV, yFluxV, zFluxV;
  PetscReal dx=pde->dx, dy=pde->dy, dz=pde->dz;
  PetscInt xi, yi, zi, xstart, xend, ystart, yend, zstart, zend,bufferz = pde->multictx.bufferz,zoffset;
  PetscInt       xs,ys,zs,xm,ym,zm,dof;
  PetscBool flg, checkSol=PETSC_FALSE, checkRHS=PETSC_FALSE;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PetscLogEventRegister("RHS_TOP_BC",TS_CLASSID,&TOP_RHS_BC);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("RHS_TOP_GL",TS_CLASSID,&TOP_RHS_GL);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("RHS_TOP_RECON",TS_CLASSID,&TOP_RHS_RECON);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("RHS_TOP_GRAD",TS_CLASSID,&TOP_RHS_GRAD);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("RHS_TOP_EVAL",TS_CLASSID,&TOP_RHS_EVAL);CHKERRQ(ierr);
  ierr = PetscLogEventRegister("RHS_TOP_OFFSET",TS_CLASSID,&TOP_RHS_OFFSET);CHKERRQ(ierr);

  ierr = PetscOptionsGetBool(NULL,pde->defs->prefix,"-check_sol",&checkSol,&flg);
  if (checkSol) {
    ierr = FVNSCheckSolution(pde, PETSC_TRUE, PETSC_TRUE, QQ);CHKERRQ(ierr);
  }
  ierr = PetscLogEventBegin(TOP_RHS_GL,0,0,0,0);CHKERRQ(ierr);
  /* communicate global solution into local components and extract array */
  ierr = DMGlobalToLocalBegin(pde->dm, QQ, INSERT_VALUES, pde->QQloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(pde->dm, QQ, INSERT_VALUES, pde->QQloc);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(pde->dm, pde->QQloc, &qq);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(TOP_RHS_GL,0,0,0,0);CHKERRQ(ierr);

  /* prepare the solution and gradients for RHS computation */
  ierr = PetscLogEventBegin(TOP_RHS_BC,0,0,0,0);CHKERRQ(ierr);
  ierr = ApplyPhysicalBoundaryConditions_TOP(pde, qq);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(TOP_RHS_BC,0,0,0,0);CHKERRQ(ierr);
    
  ierr = PetscLogEventBegin(TOP_RHS_GRAD,0,0,0,0);CHKERRQ(ierr);
  ierr = ComputePrimitiveGradients_TOP(pde, qq);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(TOP_RHS_GRAD,0,0,0,0);CHKERRQ(ierr);

  ierr = PetscLogEventBegin(TOP_RHS_RECON,0,0,0,0);CHKERRQ(ierr);
  ierr = ReconstructSolutionOnFaces_TOP(pde, qq);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(TOP_RHS_RECON,0,0,0,0);CHKERRQ(ierr);

  /* get zeroed local array for the RHS vector */
  ierr = VecZeroEntries(RHS);CHKERRQ(ierr);

  /* use a hack to offset the rhs vector, so that it fits the expected 
  format of the standard code. Remember RHS is a subvec of QQ, so they don't have 
  the same layout anymore */
  //ierr = DMDAVecGetArray(pde->dm, RHS, &rhs);CHKERRQ(ierr);
  ierr = PetscLogEventBegin(TOP_RHS_OFFSET,0,0,0,0);CHKERRQ(ierr);
  ierr = DMDAGetCorners(pde->dm,&xs,&ys,&zs,&xm,&ym,&zm);CHKERRQ(ierr);
  ierr = DMDAGetDof(pde->dm,&dof);CHKERRQ(ierr);

  /* Compute the actual amount of data in RHS */ 
  if (bufferz+1<zs) {
    /* then the entire processor is TOP and RHS is full, no change to zm */
  } else {
    zm = PetscMax(zm+zs-bufferz-1,0);
  }
  /* here we offset the rhs array to the qq array by st the input data matches the output data */
  zoffset = (bufferz >= zs) ? bufferz+1 : zs;
  ierr = VecGetArray3d(RHS,zm,ym,xm*dof,zoffset,ys,xs*dof,(PetscScalar****)&rhs);CHKERRQ(ierr); /* standard call */
  ierr = PetscLogEventEnd(TOP_RHS_OFFSET,0,0,0,0);CHKERRQ(ierr);
  /* as we never write to rhs unless z >= bufferz +1, this will all be okay  */
  /* iterate over elements but operate only on the left, front and bottom faces

   * x-direction view:
   *       _________________________________
   *      /          /          /          /|
   *     /          /          /          / |
   *    /__________/__________/__________/  |
   *    |          |          |          |  |
   *    |   xi-1   |    xi    |   xi+1   |  /
   *    |          |          |          | /
   *    |__________|__________|__________|/
                  L^

   * y-direction view:
   *                     ___________
   *                    /          /|
   *                   /   yi+1   / |
   *                  /__________/  |
   *                 /          /|  |
   *                /    yi    / |  /
   *           F > /__________/  | /
   *              /          /|  |/
   *             /   yi-1   / |  /
   *            /__________/  | /
   *            |          |  |/
   *            |          |  /
   *            |          | /
   *            |__________|/

   * z-direction view:
   *                  ___________
   *                 /          /|
   *                /          / |
   *               /__________/  |
   *               |          |  |
   *               |   zi+1   | /|
   *               |          |/ |
   *               |__________|  |
   *               |          |  |
   *               |    zi    | /|
   *               |          |/ |
   *           B > |__________|  |
   *               |          |  |
   *               |   zi-1   |  /
   *               |          | /
   *               |__________|/

  */
  ierr = DMDAGetLocalInfo(pde->dm, &info);CHKERRQ(ierr);
  xstart = info.xs;  xend = info.xs + info.xm;
  ystart = info.ys;  yend = info.ys + info.ym;
  zstart = (pde->multictx.bufferz >= info.zs) ? pde->multictx.bufferz+1 : info.zs;
  zend = info.zs + info.zm;

  ierr = PetscLogEventBegin(TOP_RHS_EVAL,0,0,0,0);CHKERRQ(ierr);
  for (zi=zstart; zi<zend; zi++) {
    for (yi=ystart; yi<yend; yi++) {
      for (xi=xstart; xi<xend; xi++) {
        /* 
          compute inviscid flux contributions to the left/right (x-dir)
          and bottom/top (y-dir) elements surrounding the (xi, yi) face
        */
        ierr = ComputeIFlux(pde, xi, yi, zi, &xFluxI, &yFluxI, &zFluxI);CHKERRQ(ierr);
        /* 
          compute viscous flux contributions to the left/right (x-dir)
          and bottom/top (y-dir) elements surrounding the (xi, yi) face
        */
        ierr = ComputeVFlux(pde, xi, yi, zi, &xFluxV, &yFluxV, &zFluxV);CHKERRQ(ierr);
        /* add left-face flux contribution contribution to left element */
        if (xi != 0 && xi != xstart) {
          rhs[zi][yi][xi-1].rho  += (xFluxV.rho  - xFluxI.rho)/dx;
          rhs[zi][yi][xi-1].rhoU += (xFluxV.rhoU - xFluxI.rhoU)/dx;
          rhs[zi][yi][xi-1].rhoV += (xFluxV.rhoV - xFluxI.rhoV)/dx;
          rhs[zi][yi][xi-1].rhoW += (xFluxV.rhoW - xFluxI.rhoW)/dx;
          rhs[zi][yi][xi-1].rhoE += (xFluxV.rhoE - xFluxI.rhoE)/dx;
        }
        
        /* add left-face flux contribution to the right (central) element */
        rhs[zi][yi][xi].rho  -= (xFluxV.rho  - xFluxI.rho)/dx;
        rhs[zi][yi][xi].rhoU -= (xFluxV.rhoU - xFluxI.rhoU)/dx;
        rhs[zi][yi][xi].rhoV -= (xFluxV.rhoV - xFluxI.rhoV)/dx;
        rhs[zi][yi][xi].rhoW -= (xFluxV.rhoW - xFluxI.rhoW)/dx;
        rhs[zi][yi][xi].rhoE -= (xFluxV.rhoE - xFluxI.rhoE)/dx;

        /* add front-face flux contribution to the bottom element */
        if (yi != 0 && yi != ystart) {
          rhs[zi][yi-1][xi].rho  += (yFluxV.rho  - yFluxI.rho)/dy;
          rhs[zi][yi-1][xi].rhoU += (yFluxV.rhoU - yFluxI.rhoU)/dy;
          rhs[zi][yi-1][xi].rhoV += (yFluxV.rhoV - yFluxI.rhoV)/dy;
          rhs[zi][yi-1][xi].rhoW += (yFluxV.rhoW - yFluxI.rhoW)/dy;
          rhs[zi][yi-1][xi].rhoE += (yFluxV.rhoE - yFluxI.rhoE)/dy;
        }
        /* add front-face flux contribution to the top (central) element */
        rhs[zi][yi][xi].rho  -= (yFluxV.rho  - yFluxI.rho)/dy;
        rhs[zi][yi][xi].rhoU -= (yFluxV.rhoU - yFluxI.rhoU)/dy;
        rhs[zi][yi][xi].rhoV -= (yFluxV.rhoV - yFluxI.rhoV)/dy;
        rhs[zi][yi][xi].rhoW -= (yFluxV.rhoW - yFluxI.rhoW)/dy;
        rhs[zi][yi][xi].rhoE -= (yFluxV.rhoE - yFluxI.rhoE)/dy;

        /* add bottom-face flux contribution to the front element */
        if (zi != 0 && zi != zstart) {
          rhs[zi-1][yi][xi].rho  += (zFluxV.rho  - zFluxI.rho)/dz;
          rhs[zi-1][yi][xi].rhoU += (zFluxV.rhoU - zFluxI.rhoU)/dz;
          rhs[zi-1][yi][xi].rhoV += (zFluxV.rhoV - zFluxI.rhoV)/dz;
          rhs[zi-1][yi][xi].rhoW += (zFluxV.rhoW - zFluxI.rhoW)/dz;
          rhs[zi-1][yi][xi].rhoE += (zFluxV.rhoE - zFluxI.rhoE)/dz;
        }
        /* add bottom-face flux contribution to the back (central) element */
        rhs[zi][yi][xi].rho  -= (zFluxV.rho  - zFluxI.rho)/dz;
        rhs[zi][yi][xi].rhoU -= (zFluxV.rhoU - zFluxI.rhoU)/dz;
        rhs[zi][yi][xi].rhoV -= (zFluxV.rhoV - zFluxI.rhoV)/dz;
        rhs[zi][yi][xi].rhoW -= (zFluxV.rhoW - zFluxI.rhoW)/dz;
        rhs[zi][yi][xi].rhoE -= (zFluxV.rhoE - zFluxI.rhoE)/dz;

        /* last cell in the x-direction does contributions from the right face */
        if (xi == xend-1) {
          ierr = ComputeIFlux(pde, xi+1, yi, zi, &xFluxI, &yFluxI, &zFluxI);CHKERRQ(ierr);
          ierr = ComputeVFlux(pde, xi+1, yi, zi, &xFluxV, &yFluxV, &zFluxV);CHKERRQ(ierr);
          rhs[zi][yi][xi].rho  += (xFluxV.rho  - xFluxI.rho)/dx;
          rhs[zi][yi][xi].rhoU += (xFluxV.rhoU - xFluxI.rhoU)/dx;
          rhs[zi][yi][xi].rhoV += (xFluxV.rhoV - xFluxI.rhoV)/dx;
          rhs[zi][yi][xi].rhoW += (xFluxV.rhoW - xFluxI.rhoW)/dx;
          rhs[zi][yi][xi].rhoE += (xFluxV.rhoE - xFluxI.rhoE)/dx;
        }

        /* last cell in the y-direction does contributions from the back face */
        if (yi == yend-1) {
          ierr = ComputeIFlux(pde, xi, yi+1, zi, &xFluxI, &yFluxI, &zFluxI);CHKERRQ(ierr);
          ierr = ComputeVFlux(pde, xi, yi+1, zi, &xFluxV, &yFluxV, &zFluxV);CHKERRQ(ierr);
          rhs[zi][yi][xi].rho  += (yFluxV.rho  - yFluxI.rho)/dy;
          rhs[zi][yi][xi].rhoU += (yFluxV.rhoU - yFluxI.rhoU)/dy;
          rhs[zi][yi][xi].rhoV += (yFluxV.rhoV - yFluxI.rhoV)/dy;
          rhs[zi][yi][xi].rhoW += (yFluxV.rhoW - yFluxI.rhoW)/dy;
          rhs[zi][yi][xi].rhoE += (yFluxV.rhoE - yFluxI.rhoE)/dy;
        }

        /* last cell in the z-direction does contributions from the top face */
        if (zi == zend-1) {
          ierr = ComputeIFlux(pde, xi, yi, zi+1, &xFluxI, &yFluxI, &zFluxI);CHKERRQ(ierr);
          ierr = ComputeVFlux(pde, xi, yi, zi+1, &xFluxV, &yFluxV, &zFluxV);CHKERRQ(ierr);
          rhs[zi][yi][xi].rho  += (zFluxV.rho  - zFluxI.rho)/dz;
          rhs[zi][yi][xi].rhoU += (zFluxV.rhoU - zFluxI.rhoU)/dz;
          rhs[zi][yi][xi].rhoV += (zFluxV.rhoV - zFluxI.rhoV)/dz;
          rhs[zi][yi][xi].rhoW += (zFluxV.rhoW - zFluxI.rhoW)/dz;
          rhs[zi][yi][xi].rhoE += (zFluxV.rhoE - zFluxI.rhoE)/dz;
        }

        rhs[zi][yi][xi].rhoW -= pde->defs->g*qq[zi][yi][xi].rho;
        rhs[zi][yi][xi].rhoE -= pde->defs->g*qq[zi][yi][xi].rhoW;
      }
    }
  }
  ierr = PetscLogEventEnd(TOP_RHS_EVAL,0,0,0,0);CHKERRQ(ierr);

  /* restore the solution vector, we're done with it */
  ierr = DMDAVecRestoreArray(pde->dm, pde->QQloc, &qq);CHKERRQ(ierr);
  ierr = PetscLogEventBegin(TOP_RHS_OFFSET,0,0,0,0);CHKERRQ(ierr);
  /* restore RHS array back into local vector and communicate to global */
  ierr = VecRestoreArray3d(RHS,zm,ym,xm*dof,zoffset,ys,xs*dof,(PetscScalar****)&rhs);CHKERRQ(ierr); 
  ierr = PetscLogEventEnd(TOP_RHS_OFFSET,0,0,0,0);CHKERRQ(ierr);

  ierr = PetscOptionsGetBool(NULL,pde->defs->prefix,"-check_rhs",&checkRHS,&flg);
  if (checkRHS) {
    ierr = FVNSCheckSolution(pde, PETSC_TRUE, PETSC_FALSE, RHS);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

/* only compute for z <= bufferz and the output offset to account 
for the RHS vector having the structure of a subvec of QQ with 
IS corresponding to the dofs with z <= bufferz */

PetscErrorCode FVNSComputeRHS_BOTTOM(FVNS* pde, Vec QQ, Vec RHS)
{
  DMDALocalInfo info;
  Solution ***qq, ***rhs;
  Solution xFluxI, yFluxI, zFluxI, xFluxV, yFluxV, zFluxV;
  PetscReal dx=pde->dx, dy=pde->dy, dz=pde->dz;
  PetscInt xi, yi, zi, xstart, xend, ystart, yend, zstart, zend,bufferz = pde->multictx.bufferz;
  PetscInt       xs,ys,zs,xm,ym,zm,dof;
  PetscBool flg, checkSol=PETSC_FALSE, checkRHS=PETSC_FALSE;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PetscOptionsGetBool(NULL,pde->defs->prefix,"-check_sol",&checkSol,&flg);
  if (checkSol) {
    ierr = FVNSCheckSolution(pde, PETSC_TRUE, PETSC_TRUE, QQ);CHKERRQ(ierr);
  }
  /* communicate global solution into local components and extract array */
  ierr = DMGlobalToLocalBegin(pde->dm, QQ, INSERT_VALUES, pde->QQloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(pde->dm, QQ, INSERT_VALUES, pde->QQloc);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(pde->dm, pde->QQloc, &qq);CHKERRQ(ierr);

  /* prepare the solution and gradients for RHS computation */
  ierr = ApplyPhysicalBoundaryConditions_BOTTOM(pde, qq);CHKERRQ(ierr);
  ierr = ComputePrimitiveGradients_BOTTOM(pde, qq);CHKERRQ(ierr);
  ierr = ReconstructSolutionOnFaces_BOTTOM(pde, qq);CHKERRQ(ierr);

  /* get zeroed local array for the RHS vector */
  ierr = VecZeroEntries(RHS);CHKERRQ(ierr);

  /* use a hack to offset the rhs vector, so that it fits the expected 
  format of the standard code. Remember RHS is a subvec of QQ, so they don't have 
  the same layout anymore */
  //ierr = DMDAVecGetArray(pde->dm, RHS, &rhs);CHKERRQ(ierr);

  ierr = DMDAGetCorners(pde->dm,&xs,&ys,&zs,&xm,&ym,&zm);CHKERRQ(ierr);
  ierr = DMDAGetDof(pde->dm,&dof);CHKERRQ(ierr);

  /* Compute the actual amount of data in RHS */ 

  zm = (zm+zs > bufferz) ? PetscMax(bufferz-zs+1,0) : zm;
  /* here we offset the rhs array tothe qq array by st the rhs[z-bufferz-1][y][x] = qq[z][y][x]. 
  This way the everything will align as expected as RHS is subvec of QQ defined in the same manner */
  ierr = VecGetArray3d(RHS,zm,ym,xm*dof,zs,ys,xs*dof,(PetscScalar****)&rhs);CHKERRQ(ierr); /* standard call */
  /* as we never write to rhs unless z >= bufferz +1, this will all be okay */
  /* iterate over elements but operate only on the left, front and bottom faces

   * x-direction view:
   *       _________________________________
   *      /          /          /          /|
   *     /          /          /          / |
   *    /__________/__________/__________/  |
   *    |          |          |          |  |
   *    |   xi-1   |    xi    |   xi+1   |  /
   *    |          |          |          | /
   *    |__________|__________|__________|/
                  L^

   * y-direction view:
   *                     ___________
   *                    /          /|
   *                   /   yi+1   / |
   *                  /__________/  |
   *                 /          /|  |
   *                /    yi    / |  /
   *           F > /__________/  | /
   *              /          /|  |/
   *             /   yi-1   / |  /
   *            /__________/  | /
   *            |          |  |/
   *            |          |  /
   *            |          | /
   *            |__________|/

   * z-direction view:
   *                  ___________
   *                 /          /|
   *                /          / |
   *               /__________/  |
   *               |          |  |
   *               |   zi+1   | /|
   *               |          |/ |
   *               |__________|  |
   *               |          |  |
   *               |    zi    | /|
   *               |          |/ |
   *           B > |__________|  |
   *               |          |  |
   *               |   zi-1   |  /
   *               |          | /
   *               |__________|/

  */
  ierr = DMDAGetLocalInfo(pde->dm, &info);CHKERRQ(ierr);
  xstart = info.xs;  xend = info.xs + info.xm;
  ystart = info.ys;  yend = info.ys + info.ym;
  zstart = info.zs;  
  zend   = (pde->multictx.bufferz+1 < info.zs+info.zm) ? pde->multictx.bufferz+1 : info.zs+info.zm;
  for (zi=zstart; zi<zend; zi++) {
    for (yi=ystart; yi<yend; yi++) {
      for (xi=xstart; xi<xend; xi++) {
        /* 
          compute inviscid flux contributions to the left/right (x-dir)
          and bottom/top (y-dir) elements surrounding the (xi, yi) face
        */
        ierr = ComputeIFlux(pde, xi, yi, zi, &xFluxI, &yFluxI, &zFluxI);CHKERRQ(ierr);
        /* 
          compute viscous flux contributions to the left/right (x-dir)
          and bottom/top (y-dir) elements surrounding the (xi, yi) face
        */
        ierr = ComputeVFlux(pde, xi, yi, zi, &xFluxV, &yFluxV, &zFluxV);CHKERRQ(ierr);
        /* add left-face flux contribution contribution to left element */
        if (xi != 0 && xi != xstart) {
          rhs[zi][yi][xi-1].rho  += (xFluxV.rho  - xFluxI.rho)/dx;
          rhs[zi][yi][xi-1].rhoU += (xFluxV.rhoU - xFluxI.rhoU)/dx;
          rhs[zi][yi][xi-1].rhoV += (xFluxV.rhoV - xFluxI.rhoV)/dx;
          rhs[zi][yi][xi-1].rhoW += (xFluxV.rhoW - xFluxI.rhoW)/dx;
          rhs[zi][yi][xi-1].rhoE += (xFluxV.rhoE - xFluxI.rhoE)/dx;
        }
        
        /* add left-face flux contribution to the right (central) element */
        rhs[zi][yi][xi].rho  -= (xFluxV.rho  - xFluxI.rho)/dx;
        rhs[zi][yi][xi].rhoU -= (xFluxV.rhoU - xFluxI.rhoU)/dx;
        rhs[zi][yi][xi].rhoV -= (xFluxV.rhoV - xFluxI.rhoV)/dx;
        rhs[zi][yi][xi].rhoW -= (xFluxV.rhoW - xFluxI.rhoW)/dx;
        rhs[zi][yi][xi].rhoE -= (xFluxV.rhoE - xFluxI.rhoE)/dx;

        /* add front-face flux contribution to the bottom element */
        if (yi != 0 && yi != ystart) {
          rhs[zi][yi-1][xi].rho  += (yFluxV.rho  - yFluxI.rho)/dy;
          rhs[zi][yi-1][xi].rhoU += (yFluxV.rhoU - yFluxI.rhoU)/dy;
          rhs[zi][yi-1][xi].rhoV += (yFluxV.rhoV - yFluxI.rhoV)/dy;
          rhs[zi][yi-1][xi].rhoW += (yFluxV.rhoW - yFluxI.rhoW)/dy;
          rhs[zi][yi-1][xi].rhoE += (yFluxV.rhoE - yFluxI.rhoE)/dy;
        }
        /* add front-face flux contribution to the top (central) element */
        rhs[zi][yi][xi].rho  -= (yFluxV.rho  - yFluxI.rho)/dy;
        rhs[zi][yi][xi].rhoU -= (yFluxV.rhoU - yFluxI.rhoU)/dy;
        rhs[zi][yi][xi].rhoV -= (yFluxV.rhoV - yFluxI.rhoV)/dy;
        rhs[zi][yi][xi].rhoW -= (yFluxV.rhoW - yFluxI.rhoW)/dy;
        rhs[zi][yi][xi].rhoE -= (yFluxV.rhoE - yFluxI.rhoE)/dy;

        /* add bottom-face flux contribution to the front element */
        if (zi != 0 && zi != zstart) {
          rhs[zi-1][yi][xi].rho  += (zFluxV.rho  - zFluxI.rho)/dz;
          rhs[zi-1][yi][xi].rhoU += (zFluxV.rhoU - zFluxI.rhoU)/dz;
          rhs[zi-1][yi][xi].rhoV += (zFluxV.rhoV - zFluxI.rhoV)/dz;
          rhs[zi-1][yi][xi].rhoW += (zFluxV.rhoW - zFluxI.rhoW)/dz;
          rhs[zi-1][yi][xi].rhoE += (zFluxV.rhoE - zFluxI.rhoE)/dz;
        }
        /* add bottom-face flux contribution to the back (central) element */
        rhs[zi][yi][xi].rho  -= (zFluxV.rho  - zFluxI.rho)/dz;
        rhs[zi][yi][xi].rhoU -= (zFluxV.rhoU - zFluxI.rhoU)/dz;
        rhs[zi][yi][xi].rhoV -= (zFluxV.rhoV - zFluxI.rhoV)/dz;
        rhs[zi][yi][xi].rhoW -= (zFluxV.rhoW - zFluxI.rhoW)/dz;
        rhs[zi][yi][xi].rhoE -= (zFluxV.rhoE - zFluxI.rhoE)/dz;

        /* last cell in the x-direction does contributions from the right face */
        if (xi == xend-1) {
          ierr = ComputeIFlux(pde, xi+1, yi, zi, &xFluxI, &yFluxI, &zFluxI);CHKERRQ(ierr);
          ierr = ComputeVFlux(pde, xi+1, yi, zi, &xFluxV, &yFluxV, &zFluxV);CHKERRQ(ierr);
          rhs[zi][yi][xi].rho  += (xFluxV.rho  - xFluxI.rho)/dx;
          rhs[zi][yi][xi].rhoU += (xFluxV.rhoU - xFluxI.rhoU)/dx;
          rhs[zi][yi][xi].rhoV += (xFluxV.rhoV - xFluxI.rhoV)/dx;
          rhs[zi][yi][xi].rhoW += (xFluxV.rhoW - xFluxI.rhoW)/dx;
          rhs[zi][yi][xi].rhoE += (xFluxV.rhoE - xFluxI.rhoE)/dx;
        }

        /* last cell in the y-direction does contributions from the back face */
        if (yi == yend-1) {
          ierr = ComputeIFlux(pde, xi, yi+1, zi, &xFluxI, &yFluxI, &zFluxI);CHKERRQ(ierr);
          ierr = ComputeVFlux(pde, xi, yi+1, zi, &xFluxV, &yFluxV, &zFluxV);CHKERRQ(ierr);
          rhs[zi][yi][xi].rho  += (yFluxV.rho  - yFluxI.rho)/dy;
          rhs[zi][yi][xi].rhoU += (yFluxV.rhoU - yFluxI.rhoU)/dy;
          rhs[zi][yi][xi].rhoV += (yFluxV.rhoV - yFluxI.rhoV)/dy;
          rhs[zi][yi][xi].rhoW += (yFluxV.rhoW - yFluxI.rhoW)/dy;
          rhs[zi][yi][xi].rhoE += (yFluxV.rhoE - yFluxI.rhoE)/dy;
        }

        /* last cell in the z-direction does contributions from the top face */
        if (zi == zend-1) {
          ierr = ComputeIFlux(pde, xi, yi, zi+1, &xFluxI, &yFluxI, &zFluxI);CHKERRQ(ierr);
          ierr = ComputeVFlux(pde, xi, yi, zi+1, &xFluxV, &yFluxV, &zFluxV);CHKERRQ(ierr);
          rhs[zi][yi][xi].rho  += (zFluxV.rho  - zFluxI.rho)/dz;
          rhs[zi][yi][xi].rhoU += (zFluxV.rhoU - zFluxI.rhoU)/dz;
          rhs[zi][yi][xi].rhoV += (zFluxV.rhoV - zFluxI.rhoV)/dz;
          rhs[zi][yi][xi].rhoW += (zFluxV.rhoW - zFluxI.rhoW)/dz;
          rhs[zi][yi][xi].rhoE += (zFluxV.rhoE - zFluxI.rhoE)/dz;
        }
        rhs[zi][yi][xi].rhoW -= pde->defs->g*qq[zi][yi][xi].rho;
        rhs[zi][yi][xi].rhoE -= pde->defs->g*qq[zi][yi][xi].rhoW;
      }
    }
  }
  /* restore the solution vector, we're done with it */
  ierr = DMDAVecRestoreArray(pde->dm, pde->QQloc, &qq);CHKERRQ(ierr);
  /* restore RHS array back into local vector and communicate to global */
  ierr = VecRestoreArray3d(RHS,zm,ym,xm*dof,zs,ys,xs*dof,(PetscScalar****)&rhs);CHKERRQ(ierr); /* standard call */
  ierr = PetscOptionsGetBool(NULL,pde->defs->prefix,"-check_rhs",&checkRHS,&flg);
  if (checkRHS) {
    ierr = FVNSCheckSolution(pde, PETSC_TRUE, PETSC_FALSE, RHS);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

PetscErrorCode FVNSCheckBoundary(FVNS* pde, PetscBool checkZero, Vec X)
{
  DMDALocalInfo info;
  Solution** xx;
  PetscMPIInt rank;
  PetscInt i, j;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = MPI_Comm_rank(pde->comm, &rank);CHKERRQ(ierr);
  ierr = DMDAGetLocalInfo(pde->dm, &info);CHKERRQ(ierr);
  ierr = VecGetArray2dRead(
    X, pde->jm, pde->im*info.dof, pde->js, pde->is*info.dof, 
    (PetscScalar***)&xx);CHKERRQ(ierr);
  for (j=pde->js; j<pde->js+pde->jm; j++) {
    for (i=pde->is; i<pde->is+pde->im; i++) {
      if (SolutionCheck(checkZero, &xx[j][i])) {
        ierr = PetscPrintf(
          PETSC_COMM_SELF, "[%d] %s detected bad value at %s boundary idx (%d,%d)\n",
          rank, pde->defs->prefix, BoundaryLocations[pde->defs->couplingSide], i, j);CHKERRQ(ierr);
        ierr = SolutionView(pde->comm, &xx[j][i]);CHKERRQ(ierr);
        ierr = MPI_Abort(pde->comm, 11);CHKERRQ(ierr);
      }
    }
  }
  ierr = VecRestoreArray2dRead(
    X, pde->jm, pde->im*info.dof, pde->js, pde->is*info.dof, 
    (PetscScalar***)&xx);
  ierr = PetscPrintf(PETSC_COMM_SELF, "[%d] %s coupling boundary extracted correctly...\n", rank, pde->defs->prefix);
  PetscFunctionReturn(0);
}

PetscErrorCode FVNSSetFromOptions(FVNS *pde)
{
  PetscBool      flg;
  ProblemDefs    *defs = pde->defs;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PetscOptionsGetInt(
    NULL,defs->prefix,"-nxe",&pde->nxe,&flg);CHKERRQ(ierr); 
  ierr = PetscOptionsGetInt(
    NULL,defs->prefix,"-nye",&pde->nye,&flg);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(
    NULL,defs->prefix,"-nze",&pde->nze,&flg);CHKERRQ(ierr);

  ierr = PetscOptionsGetReal(NULL,defs->prefix,"-dx",&pde->dx,&flg);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,defs->prefix,"-dy",&pde->dy,&flg);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,defs->prefix,"-dz",&pde->dz,&flg);CHKERRQ(ierr);

  ierr = PetscOptionsGetReal(NULL,defs->prefix,"-x_bot",&pde->x_bottomcorner,&flg);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,defs->prefix,"-y_bot",&pde->y_bottomcorner,&flg);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,defs->prefix,"-z_bot",&pde->z_bottomcorner,&flg);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,defs->prefix,"-x_top",&pde->x_upcorner,&flg);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,defs->prefix,"-y_top",&pde->y_upcorner,&flg);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(NULL,defs->prefix,"-z_top",&pde->z_upcorner,&flg);CHKERRQ(ierr);
  /* fix the geometry of the box to use user specified coordinates, turns off the -dx, -dy, -dz options */
  ierr = PetscOptionsGetBool(NULL,defs->prefix,"-fixedgeom",&pde->fixedgeometry,&flg);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode FVNSOptionsView(FVNS* pde)
{
  PetscBool      flg;
  ProblemDefs    *defs = pde->defs;
  PetscViewer    viewer; 
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  flg  = PETSC_FALSE;
  ierr = PetscOptionsGetViewer(pde->comm,NULL,defs->prefix,"-fvns_view",&viewer,NULL,&flg);CHKERRQ(ierr);
  if (flg) {
    ierr = FVNSView(pde,viewer);CHKERRQ(ierr);
    ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

PetscErrorCode FVNSSetNumElements(FVNS* pde, PetscInt nxe, PetscInt nye, PetscInt nze)
{
  PetscFunctionBeginUser;
  pde->nxe = nxe; 
  pde->nye = nye; 
  pde->nze = nze; 
  PetscFunctionReturn(0); 
}

PetscErrorCode FVNSSetMeshSize(FVNS* pde, PetscReal dx, PetscReal dy, PetscReal dz)
{
  PetscFunctionBeginUser;
  pde->dx = dx; 
  pde->dy = dy; 
  pde->dz = dz; 
  PetscFunctionReturn(0); 
}

PetscErrorCode FVNSSetUp(FVNS *newpde)
{

  DMBoundaryType xBCdm, yBCdm, zBCdm;
  DMDALocalInfo info;
  VecType vecType;
  PetscInt nloc, nglob, nstart;
  ProblemDefs *defs = newpde->defs;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  /* set up the DM first */
  if (defs->xPeriodic) xBCdm = DM_BOUNDARY_PERIODIC;
  else xBCdm = DM_BOUNDARY_GHOSTED;
  if (defs->yPeriodic) yBCdm = DM_BOUNDARY_PERIODIC;
  else yBCdm = DM_BOUNDARY_GHOSTED;
  if (defs->zPeriodic) zBCdm = DM_BOUNDARY_PERIODIC;
  else zBCdm = DM_BOUNDARY_GHOSTED;
  ierr = DMDACreate3d(
    newpde->comm, xBCdm, yBCdm, zBCdm, DMDA_STENCIL_BOX, newpde->nxe, newpde->nye, newpde->nze,
    PETSC_DECIDE, PETSC_DECIDE, PETSC_DECIDE, 5, 2, NULL, NULL, NULL, &newpde->dm);CHKERRQ(ierr);
  ierr = DMSetOptionsPrefix(newpde->dm, defs->prefix);CHKERRQ(ierr);
  ierr = DMSetFromOptions(newpde->dm);CHKERRQ(ierr);
  ierr = DMSetUp(newpde->dm);CHKERRQ(ierr);
  /* adjust the top cordinates of the box depending on user inputs */
  if(newpde->fixedgeometry) {
    newpde->dx = (newpde->x_upcorner - newpde->x_bottomcorner)/newpde->nxe;
    newpde->dy = (newpde->y_upcorner - newpde->y_bottomcorner)/newpde->nye;
    newpde->dz = (newpde->z_upcorner - newpde->z_bottomcorner)/newpde->nze;

  } else {
    newpde->x_upcorner = newpde->dx*newpde->nxe +newpde->x_bottomcorner;
    newpde->y_upcorner = newpde->dy*newpde->nye +newpde->y_bottomcorner;
    newpde->z_upcorner = newpde->dz*newpde->nze +newpde->z_bottomcorner;
  }
  /* create cell-centered coordinate */ 
  ierr = DMDASetUniformCoordinates(newpde->dm,
   newpde->x_bottomcorner+0.5*newpde->dx,newpde->x_upcorner-0.5*newpde->dx,
   newpde->y_bottomcorner+0.5*newpde->dy,newpde->y_upcorner-0.5*newpde->dy,
   newpde->z_bottomcorner+0.5*newpde->dz,newpde->z_upcorner-0.5*newpde->dz);CHKERRQ(ierr);

  ierr = DMDASetFieldName(newpde->dm, 0, "rho");CHKERRQ(ierr);
  ierr = DMDASetFieldName(newpde->dm, 1, "rhoU");CHKERRQ(ierr);
  ierr = DMDASetFieldName(newpde->dm, 2, "rhoV");CHKERRQ(ierr);
  ierr = DMDASetFieldName(newpde->dm, 3, "rhoW");CHKERRQ(ierr);
  ierr = DMDASetFieldName(newpde->dm, 4, "rhoE");CHKERRQ(ierr);

  /* create local solution vectors */
  ierr = DMCreateLocalVector(newpde->dm, &newpde->QQloc);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(newpde->dm, &newpde->QF_L);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(newpde->dm, &newpde->QF_R);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(newpde->dm, &newpde->QF_F);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(newpde->dm, &newpde->QF_B);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(newpde->dm, &newpde->QF_D);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(newpde->dm, &newpde->QF_U);CHKERRQ(ierr);

  /* extract work arrays for the reconstructed solution */
  ierr = VecZeroEntries(newpde->QF_L);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(newpde->dm, newpde->QF_L, &newpde->qf_L);CHKERRQ(ierr);
  ierr = VecZeroEntries(newpde->QF_R);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(newpde->dm, newpde->QF_R, &newpde->qf_R);CHKERRQ(ierr);
  ierr = VecZeroEntries(newpde->QF_F);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(newpde->dm, newpde->QF_F, &newpde->qf_F);CHKERRQ(ierr);
  ierr = VecZeroEntries(newpde->QF_B);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(newpde->dm, newpde->QF_B, &newpde->qf_B);CHKERRQ(ierr);
  ierr = VecZeroEntries(newpde->QF_D);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(newpde->dm, newpde->QF_D, &newpde->qf_D);CHKERRQ(ierr);
  ierr = VecZeroEntries(newpde->QF_U);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(newpde->dm, newpde->QF_U, &newpde->qf_U);CHKERRQ(ierr);

  /* create a compatible primitives DM (6 dof instead of 5) */
  ierr = DMDACreateCompatibleDMDA(newpde->dm, 6, &newpde->dmPrim);CHKERRQ(ierr);
  ierr = DMDASetFieldName(newpde->dmPrim, 0, "r");CHKERRQ(ierr);
  ierr = DMDASetFieldName(newpde->dmPrim, 1, "u");CHKERRQ(ierr);
  ierr = DMDASetFieldName(newpde->dmPrim, 2, "v");CHKERRQ(ierr);
  ierr = DMDASetFieldName(newpde->dmPrim, 2, "w");CHKERRQ(ierr);
  ierr = DMDASetFieldName(newpde->dmPrim, 3, "p");CHKERRQ(ierr);
  ierr = DMDASetFieldName(newpde->dmPrim, 4, "e");CHKERRQ(ierr);

  /* create local primitive vectors */
  ierr = DMCreateLocalVector(newpde->dmPrim, &newpde->XG);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(newpde->dmPrim, &newpde->YG);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(newpde->dmPrim, &newpde->ZG);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(newpde->dmPrim, &newpde->PF_L);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(newpde->dmPrim, &newpde->PF_R);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(newpde->dmPrim, &newpde->PF_F);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(newpde->dmPrim, &newpde->PF_B);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(newpde->dmPrim, &newpde->PF_D);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(newpde->dmPrim, &newpde->PF_U);CHKERRQ(ierr);

  /* extract work arrays for the primitive gradients */
  ierr = VecZeroEntries(newpde->XG);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(newpde->dmPrim, newpde->XG, &newpde->xg);CHKERRQ(ierr);
  ierr = VecZeroEntries(newpde->YG);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(newpde->dmPrim, newpde->YG, &newpde->yg);CHKERRQ(ierr);
  ierr = VecZeroEntries(newpde->ZG);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(newpde->dmPrim, newpde->ZG, &newpde->zg);CHKERRQ(ierr);

  /* extract work arrays for reconstructed primitives */
  ierr = VecZeroEntries(newpde->PF_L);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(newpde->dmPrim, newpde->PF_L, &newpde->pf_L);CHKERRQ(ierr);
  ierr = VecZeroEntries(newpde->PF_R);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(newpde->dmPrim, newpde->PF_R, &newpde->pf_R);CHKERRQ(ierr);
  ierr = VecZeroEntries(newpde->PF_F);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(newpde->dmPrim, newpde->PF_F, &newpde->pf_F);CHKERRQ(ierr);
  ierr = VecZeroEntries(newpde->PF_B);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(newpde->dmPrim, newpde->PF_B, &newpde->pf_B);CHKERRQ(ierr);
  ierr = VecZeroEntries(newpde->PF_D);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(newpde->dmPrim, newpde->PF_D, &newpde->pf_D);CHKERRQ(ierr);
  ierr = VecZeroEntries(newpde->PF_U);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(newpde->dmPrim, newpde->PF_U, &newpde->pf_U);CHKERRQ(ierr);

  /* for coupled problems, create an index set for cells on coupling boundary */
  newpde->mi  = 0;         newpde->mj  = 0;
  newpde->im  = 0;         newpde->jm  = 0;
  newpde->is  = 0;         newpde->js  = 0;
  newpde->gim = 0;         newpde->gjm = 0;
  newpde->gis = 0;         newpde->gjs = 0;
  if (defs->couplingSide != NONE) {
    ierr = DMDAGetLocalInfo(newpde->dm, &info);
    if (defs->couplingSide == LEFT || defs->couplingSide == RIGHT) {
      newpde->mi = info.my;  newpde->mj = info.mz;
      if ((defs->couplingSide == LEFT && info.xs == 0) || 
        (defs->couplingSide == RIGHT && info.xs+info.xm == info.mx)) {
        /* process owns cells on the Y-Z coupling face */
        newpde->im  = info.ym;   newpde->jm  = info.zm;
        newpde->is  = info.ys;   newpde->js  = info.zs;
        newpde->gim = info.gym;  newpde->gjm = info.gzm;
        newpde->gis = info.gys;  newpde->gjs = info.gzs;
      }
    } else if (defs->couplingSide == FRONT || defs->couplingSide == BACK) {
      newpde->mi = info.mx;  newpde->mj = info.mz;
      if ((defs->couplingSide == FRONT && info.ys == 0) || 
        (defs->couplingSide == BACK && info.ys+info.ym == info.my)) {
        /* process owns cells on the X-Z coupling face */
        newpde->im  = info.xm;   newpde->jm  = info.zm;
        newpde->is  = info.xs;   newpde->js  = info.zs;
        newpde->gim = info.gxm;  newpde->gjm = info.gzm;
        newpde->gis = info.gxs;  newpde->gjs = info.gzs;
      }
    } else if (defs->couplingSide == DOWN || defs->couplingSide == UP) {
      newpde->mi = info.mx;  newpde->mj = info.my;
      if ((defs->couplingSide == DOWN && info.zs == 0) || 
        (defs->couplingSide == UP && info.zs+info.zm == info.mz)) {
        /* process owns cells on the X-Y coupling face */
        newpde->im  = info.xm;   newpde->jm  = info.ym;
        newpde->is  = info.xs;   newpde->js  = info.ys;
        newpde->gim = info.gxm;  newpde->gjm = info.gym;
        newpde->gis = info.gxs;  newpde->gjs = info.gys;
      }
    }
    /* construct a vector to store own coupling values */
    nloc = newpde->im*newpde->jm*info.dof;
    nglob = newpde->mi*newpde->mj*info.dof;
    ierr = DMGetVecType(newpde->dm, &vecType);CHKERRQ(ierr);
    ierr = VecCreate(newpde->comm, &newpde->couplingOwn);CHKERRQ(ierr);
    ierr = VecSetType(newpde->couplingOwn, vecType);CHKERRQ(ierr);
    ierr = VecSetSizes(newpde->couplingOwn, nloc, nglob);CHKERRQ(ierr);
    ierr = VecSetUp(newpde->couplingOwn);CHKERRQ(ierr);
    ierr = VecDuplicate(newpde->couplingOwn, &newpde->couplingOther);CHKERRQ(ierr);
    ierr = VecGetOwnershipRange(newpde->couplingOwn, &nstart, NULL);CHKERRQ(ierr);
    ierr = ISCreateStride(newpde->comm, nloc, nstart, 1, &newpde->couplingIS);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

PetscErrorCode FVNSCreate(MPI_Comm comm,ProblemDefs *defs, FVNS** pde)
{
  FVNS *newpde;
  PetscInt i;
  size_t   len,filelen;
  PetscBool fileflg = PETSC_TRUE;
  char      *filename,*filedir,*filedir_test;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PetscNew(&newpde);CHKERRQ(ierr);
  newpde->defs = &(*defs);
  newpde->comm = comm;

  /* Set up internal PetscViewer */ 
  /* this file dir manipulation should have better input sanitation */
  ierr = PetscStrlen(defs->prefix,&len);CHKERRQ(ierr);
  filelen = len+13;
  ierr = PetscMalloc1(filelen,&filename);CHKERRQ(ierr);
  ierr = PetscSNPrintf(filename,filelen,"conservation%s",defs->prefix);CHKERRQ(ierr);

  ierr = PetscStrlen(defs->outdir,&len);CHKERRQ(ierr);
  ierr = PetscMalloc1(filelen+len+10,&filedir);CHKERRQ(ierr); 
  ierr = PetscMalloc1(filelen+len+10,&filedir_test);CHKERRQ(ierr);
  ierr = PetscSNPrintf(filedir,filelen+len+10,"%s/%s",defs->outdir,filename);CHKERRQ(ierr); 
  /* check if the csv filename already exists, increment a number at the end until one doesn't */
  i = 0; 
  while(fileflg)
  {
    ierr = PetscSNPrintf(filedir_test,filelen+len+10,"%s%i.csv",filedir,i);CHKERRQ(ierr); 
    ierr = PetscTestFile(filedir_test, 'w', &fileflg);CHKERRQ(ierr);
    i++;
  }

  ierr = PetscViewerCreate(newpde->comm,&newpde->conservation_viewer);CHKERRQ(ierr);
  ierr = PetscViewerSetType(newpde->conservation_viewer,PETSCVIEWERASCII);CHKERRQ(ierr);
  ierr = PetscViewerFileSetMode(newpde->conservation_viewer,FILE_MODE_APPEND);CHKERRQ(ierr);
  ierr = PetscViewerFileSetName(newpde->conservation_viewer,filedir_test);CHKERRQ(ierr);
  ierr = PetscFree(filename);CHKERRQ(ierr);
  ierr = PetscFree(filedir);CHKERRQ(ierr); 
  ierr = PetscFree(filedir_test);CHKERRQ(ierr); 

  /* Default Values */
  newpde->nxe = 8;  newpde->nye = 8;  newpde->nze = 8;
  /* default d[xyx] corresponds to unit cube element */
  newpde->dx = 1.0/((PetscReal)newpde->nxe);
  newpde->dy = 1.0/((PetscReal)newpde->nye);
  newpde->dz = 1.0/((PetscReal)newpde->nze);
  /* default corner points correspond to the unit cube element */
  newpde->x_bottomcorner = 0.0; newpde->x_upcorner = 1.0; 
  newpde->y_bottomcorner = 0.0; newpde->y_upcorner = 1.0; 
  newpde->z_bottomcorner = 0.0; newpde->z_upcorner = 1.0; 
  /* don't fix the geometry by default */
  newpde->fixedgeometry = PETSC_FALSE; 
  /* return out the pointer */
  *pde = newpde;
  PetscFunctionReturn(0);
}

PetscErrorCode FVNSDestroy(FVNS** pde)
{
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  /* restore all the arrays we've extracted during creation */
  ierr = DMDAVecRestoreArray((*pde)->dm, (*pde)->QF_L, &(*pde)->qf_L);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray((*pde)->dm, (*pde)->QF_R, &(*pde)->qf_R);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray((*pde)->dm, (*pde)->QF_D, &(*pde)->qf_D);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray((*pde)->dm, (*pde)->QF_U, &(*pde)->qf_U);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray((*pde)->dm, (*pde)->QF_F, &(*pde)->qf_F);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray((*pde)->dm, (*pde)->QF_B, &(*pde)->qf_B);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray((*pde)->dmPrim, (*pde)->XG, &(*pde)->xg);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray((*pde)->dmPrim, (*pde)->YG, &(*pde)->yg);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray((*pde)->dmPrim, (*pde)->ZG, &(*pde)->zg);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray((*pde)->dmPrim, (*pde)->PF_L, &(*pde)->pf_L);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray((*pde)->dmPrim, (*pde)->PF_R, &(*pde)->pf_R);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray((*pde)->dmPrim, (*pde)->PF_D, &(*pde)->pf_D);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray((*pde)->dmPrim, (*pde)->PF_U, &(*pde)->pf_U);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray((*pde)->dmPrim, (*pde)->PF_F, &(*pde)->pf_F);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray((*pde)->dmPrim, (*pde)->PF_B, &(*pde)->pf_B);CHKERRQ(ierr);

  /* reconstructed solution vectors (local) */
  ierr = VecDestroy(&(*pde)->QQloc);CHKERRQ(ierr);
  ierr = VecDestroy(&(*pde)->QF_L);CHKERRQ(ierr);
  ierr = VecDestroy(&(*pde)->QF_R);CHKERRQ(ierr);
  ierr = VecDestroy(&(*pde)->QF_D);CHKERRQ(ierr);
  ierr = VecDestroy(&(*pde)->QF_U);CHKERRQ(ierr);
  ierr = VecDestroy(&(*pde)->QF_F);CHKERRQ(ierr);
  ierr = VecDestroy(&(*pde)->QF_B);CHKERRQ(ierr);

  /* reconstructed primitives vector (local) */
  ierr = VecDestroy(&(*pde)->XG);CHKERRQ(ierr);
  ierr = VecDestroy(&(*pde)->YG);CHKERRQ(ierr);
  ierr = VecDestroy(&(*pde)->ZG);CHKERRQ(ierr);
  ierr = VecDestroy(&(*pde)->PF_L);CHKERRQ(ierr);
  ierr = VecDestroy(&(*pde)->PF_R);CHKERRQ(ierr);
  ierr = VecDestroy(&(*pde)->PF_D);CHKERRQ(ierr);
  ierr = VecDestroy(&(*pde)->PF_U);CHKERRQ(ierr);
  ierr = VecDestroy(&(*pde)->PF_F);CHKERRQ(ierr);
  ierr = VecDestroy(&(*pde)->PF_B);CHKERRQ(ierr);

  /* coupling boundary vectors */
  if ((*pde)->defs->couplingSide != NONE) {
    ierr = ISDestroy(&(*pde)->couplingIS);CHKERRQ(ierr);
    ierr = VecDestroy(&(*pde)->couplingOwn);CHKERRQ(ierr);
    ierr = VecDestroy(&(*pde)->couplingOther);CHKERRQ(ierr);
  }

  /* DM objects for solution and primitives */
  ierr = DMDestroy(&(*pde)->dmPrim);CHKERRQ(ierr);
  ierr = DMDestroy(&(*pde)->dm);CHKERRQ(ierr);

  /* Viewer Structures */
  ierr = PetscViewerDestroy(&(*pde)->conservation_viewer);CHKERRQ(ierr);

  /* problem definitions and PDE structure */
  ierr = ProblemDefsDestroy(&(*pde)->defs);CHKERRQ(ierr);
  ierr = PetscFree(*pde);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode FVNSView(FVNS* pde, PetscViewer viewer)
{
  PetscMPIInt size;
  PetscBool isASCII;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PetscObjectTypeCompare(
    (PetscObject)viewer, PETSCVIEWERASCII, &isASCII);CHKERRQ(ierr);
  if (!isASCII) PetscFunctionReturn(0);
  ierr = MPI_Comm_size(pde->comm, &size);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(
    viewer, "PDE object: %d MPI process\n", size);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPushTab(viewer);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(
    viewer, "grid size: %d x %d x %d\n", pde->nxe, pde->nye, pde->nze);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(
    viewer, "cell size: dx = %.2e\n", pde->dx);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(
    viewer, "           dy = %.2e\n", pde->dy);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(
    viewer, "           dz = %.2e\n", pde->dz);CHKERRQ(ierr);
  ierr = ProblemDefsView(pde->defs, viewer);CHKERRQ(ierr);
  ierr = DMView(pde->dm, viewer);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPopTab(viewer);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

 PetscErrorCode FVNSMonitorView_Conservation_CSV(TS ts,PetscInt steps,PetscReal time,Vec u,void *mctx)
{
  FVNS           *pde = (FVNS*)mctx;
  Solution       integral; 
  PetscErrorCode ierr;
  PetscBool      isASCII;
  PetscViewer    viewer = pde->conservation_viewer; 

  PetscFunctionBeginUser;
  ierr = PetscObjectTypeCompare(
    (PetscObject)viewer, PETSCVIEWERASCII, &isASCII);CHKERRQ(ierr);
  if (!isASCII) PetscFunctionReturn(0);
  ierr = FVNSIntegrate(pde,u,&integral);CHKERRQ(ierr);
  if(steps == 0) {PetscViewerASCIIPrintf(viewer,"time, rho, rhoU, rhoV, rhoW, rhoE\n");CHKERRQ(ierr);}
  ierr = PetscViewerASCIIPrintf(viewer,"%e,%.16e, %.16e, %.16e, %.16e, %.16e \n", time, integral.rho,integral.rhoU,integral.rhoV,integral.rhoW,integral.rhoE);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode FVNSView_Conservation(FVNS* pde, PetscViewer viewer,Vec X)
{
  PetscBool isASCII;
  PetscErrorCode ierr;
  Solution       integral; 

  PetscFunctionBeginUser;
  ierr = PetscObjectTypeCompare(
    (PetscObject)viewer, PETSCVIEWERASCII, &isASCII);CHKERRQ(ierr);
  if (!isASCII) PetscFunctionReturn(0);
  ierr = FVNSIntegrate(pde,X,&integral);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPrintf(viewer,"Conservative Variable Integrals \n");CHKERRQ(ierr);
  ierr = FVNSView_Solution_Point(pde,viewer,integral);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode FVNSView_Solution_Point(FVNS* pde, PetscViewer viewer,Solution u)
{
  PetscBool isASCII;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PetscObjectTypeCompare(
    (PetscObject)viewer, PETSCVIEWERASCII, &isASCII);CHKERRQ(ierr);
  if (!isASCII) PetscFunctionReturn(0);
  ierr = PetscViewerASCIIPushTab(viewer);CHKERRQ(ierr);
  if(u.rho>=0) {
    ierr = PetscViewerASCIIPrintf(
    viewer, "rho    :  %.16e \n",u.rho);CHKERRQ(ierr);
  } else {
    ierr = PetscViewerASCIIPrintf(
    viewer, "rho    : %.16e \n",u.rho);CHKERRQ(ierr);
  } 
  if(u.rhoU>=0) {
    ierr = PetscViewerASCIIPrintf(
    viewer, "rhoU   :  %.16e \n",u.rhoU);CHKERRQ(ierr);
  } else {
    ierr = PetscViewerASCIIPrintf(
    viewer, "rhoU   : %.16e \n",u.rhoU);CHKERRQ(ierr);
  }
  if(u.rhoW>=0) {
    ierr = PetscViewerASCIIPrintf(
    viewer, "rhoW   :  %.16e \n",u.rhoW);CHKERRQ(ierr);
  } else {
    ierr = PetscViewerASCIIPrintf(
    viewer, "rhoW   : %.16e \n",u.rhoW);CHKERRQ(ierr);
  }
  if(u.rhoV>=0) {
    ierr = PetscViewerASCIIPrintf(
    viewer, "rhoV   :  %.16e \n",u.rhoV);CHKERRQ(ierr);
  } else {  
    ierr = PetscViewerASCIIPrintf(
    viewer, "rhoV   : %.16e \n",u.rhoV);CHKERRQ(ierr);
  }
  if (u.rhoE>=0) {
    ierr = PetscViewerASCIIPrintf(
    viewer, "Energy :  %.16e \n",u.rhoE);CHKERRQ(ierr);
  } else {
    ierr = PetscViewerASCIIPrintf(
    viewer, "Energy : %.16e \n",u.rhoE);CHKERRQ(ierr);
  }
  ierr = PetscViewerASCIIPopTab(viewer);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode FVNSWriteSolutionVTK(FVNS* pde, Vec QQ, const char* dir, PetscInt step)
{
  PetscViewer viewer;
  size_t len;
  char dirBuf[256], baseBuf[256], fileBuf[256];
  PetscBool hasSlash, hasUnderscore;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PetscStrlen(dir, &len);CHKERRQ(ierr);
  if (len) {
    ierr = PetscStrncpy(dirBuf, dir, len+1);CHKERRQ(ierr);
    ierr = PetscStrendswith(dirBuf, "/", &hasSlash);CHKERRQ(ierr);
    if (hasSlash) {
      dirBuf[len] = 0;
    }
  } else {
    ierr = PetscSNPrintf(dirBuf, 256, ".");CHKERRQ(ierr);
  }
  ierr = PetscStrlen(pde->defs->prefix, &len);CHKERRQ(ierr);
  if (len) {
    ierr = PetscStrlen(pde->defs->prefix, &len);CHKERRQ(ierr);
    ierr = PetscStrncpy(baseBuf, pde->defs->prefix, len);CHKERRQ(ierr);
    ierr = PetscStrendswith(baseBuf, "_", &hasUnderscore);CHKERRQ(ierr);
    if (baseBuf[len-1] == '_') {
      baseBuf[len-1] = 0;
    }
  } else {
    ierr = PetscSNPrintf(baseBuf, 256, "sol");CHKERRQ(ierr);
  }
  ierr = PetscSNPrintf(fileBuf, 256, "%s/%s_%04D.vts", dirBuf, baseBuf, step);CHKERRQ(ierr);
  ierr = PetscViewerVTKOpen(pde->comm, fileBuf, FILE_MODE_WRITE, &viewer);
  ierr = VecView(QQ, viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode FVNSExtractCouplingVars(FVNS* pde, Vec QQ)
{
  DMDALocalInfo info;
  PetscInt xi, xstart, xend, yi, ystart, yend, zi, zstart, zend;
  Vec QQloc;
  Solution ***qq, **cbc;
  PetscBool checkCoupling=PETSC_FALSE, flg;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  /* extract the local solution including ghost cells */
  ierr = DMGetLocalVector(pde->dm, &QQloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(pde->dm, QQ, INSERT_VALUES, QQloc);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd(pde->dm, QQ, INSERT_VALUES, QQloc);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(pde->dm, QQloc, &qq);CHKERRQ(ierr);
  /* loop over solution and extract values from coupling boundary */
  ierr = DMDAGetLocalInfo(pde->dm, &info);CHKERRQ(ierr);
  xstart = info.xs;  xend = info.xs + info.xm;
  ystart = info.ys;  yend = info.ys + info.ym;
  zstart = info.zs;  zend = info.zs + info.zm;
  ierr = VecZeroEntries(pde->couplingOwn);CHKERRQ(ierr);
  ierr = VecGetArray2d(
    pde->couplingOwn, pde->jm, pde->im*info.dof, 
    pde->js, pde->is*info.dof, (PetscScalar***)&cbc);CHKERRQ(ierr);
  if (pde->defs->couplingSide == LEFT && info.xs == 0) {
    /* process owns left coupling boundary */
    for (zi=zstart; zi<zend; zi++) {
      for (yi=ystart; yi<yend; yi++) {
        SolutionCopy(&qq[zi][yi][0], &cbc[zi][yi]);
      }
    }
  } else if (pde->defs->couplingSide == RIGHT && info.xs+info.xm == info.mx) {
    /* process owns right coupling boundary */
    for (zi=zstart; zi<zend; zi++) {
      for (yi=ystart; yi<yend; yi++) {
        SolutionCopy(&qq[zi][yi][xend-1], &cbc[zi][yi]);
      }
    }
  } else if (pde->defs->couplingSide == FRONT && info.ys == 0) {
    /* process owns front coupling boundary */
    for (zi=zstart; zi<zend; zi++) {
      for (xi=xstart; xi<xend; xi++) {
        SolutionCopy(&qq[zi][0][xi], &cbc[zi][xi]);
      }
    }
  } else if (pde->defs->couplingSide == BACK && info.ys+info.ym == info.my) {
    /* process owns back coupling boundary */
    for (zi=zstart; zi<zend; zi++) {
      for (xi=xstart; xi<xend; xi++) {
        SolutionCopy(&qq[zi][yend-1][xi], &cbc[zi][xi]);
      }
    }
  } else if (pde->defs->couplingSide == DOWN && info.zs == 0) {
    /* process owns down/bottom coupling boundary */
    for (yi=ystart; yi<yend; yi++) {
      for (xi=xstart; xi<xend; xi++) {
        SolutionCopy(&qq[0][yi][xi], &cbc[yi][xi]);
      }
    }
  } else if (pde->defs->couplingSide == UP && info.zs+info.zm == info.mz) {
    /* process owns down/bottom coupling boundary */
    for (yi=ystart; yi<yend; yi++) {
      for (xi=xstart; xi<xend; xi++) {
        SolutionCopy(&qq[zend-1][yi][xi], &cbc[yi][xi]);
      }
    }
  }
  ierr = VecRestoreArray2d(
    pde->couplingOwn, pde->jm, pde->im*info.dof, 
    pde->js, pde->is*info.dof, (PetscScalar***)&cbc);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(pde->dm, QQloc, &qq);CHKERRQ(ierr);
  ierr = DMRestoreLocalVector(pde->dm, &QQloc);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(NULL,pde->defs->prefix,"-check_coupling",&checkCoupling,&flg);
  if (checkCoupling) {
    ierr = FVNSCheckBoundary(pde, PETSC_FALSE, pde->couplingOwn);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

PetscErrorCode FVNSCheckSolution(FVNS* pde, PetscBool global, PetscBool checkZero, Vec X)
{
  DMDALocalInfo info;
  Solution*** xx;
  PetscMPIInt rank;
  PetscInt xi, xstart, xend, yi, ystart, yend, zi, zstart, zend;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = DMDAGetLocalInfo(pde->dm, &info);CHKERRQ(ierr);
  if (global) {
    xstart = info.xs;  xend = info.xs + info.xm;
    ystart = info.ys;  yend = info.ys + info.ym;
    zstart = info.zs;  zend = info.zs + info.zm;
  } else {
    xstart = info.gxs; xend = info.gxs + info.gxm;
    ystart = info.gys; yend = info.gys + info.gym;
    zstart = info.gzs; zend = info.gzs + info.gzm;
  }
  ierr = MPI_Comm_rank(pde->comm, &rank);CHKERRQ(ierr);
  ierr = DMDAVecGetArrayRead(pde->dm, X, &xx);CHKERRQ(ierr);
  for (zi=zstart; zi<zend; zi++) {
    for (yi=ystart; yi<yend; yi++) {
      for (xi=xstart; xi<xend; xi++) {
        if (SolutionCheck(checkZero, &xx[zi][yi][xi])) {
          ierr = PetscPrintf(PETSC_COMM_SELF, "[%d] %s detected bad value at (%d,%d,%d)\n", rank, pde->defs->prefix, xi, yi, zi);CHKERRQ(ierr);
          ierr = SolutionView(pde->comm, &xx[zi][yi][xi]);CHKERRQ(ierr);
          ierr = MPI_Abort(pde->comm, 11);CHKERRQ(ierr);
        }
      }
    }
  }
  ierr = DMDAVecRestoreArrayRead(pde->dm, X, &xx);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}


/* Compute the Integral of the conservative variables on the DMDA on rank 0. */
PetscErrorCode FVNSIntegrate(FVNS* pde, Vec X, Solution *integral)
{
  DMDALocalInfo info;
  Solution*** xx;
  Solution    integral_wrk; 
  PetscMPIInt rank;
  PetscInt xi, xstart, xend, yi, ystart, yend, zi, zstart, zend;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = DMDAGetLocalInfo(pde->dm, &info);CHKERRQ(ierr);

  xstart = info.xs;  xend = info.xs + info.xm;
  ystart = info.ys;  yend = info.ys + info.ym;
  zstart = info.zs;  zend = info.zs + info.zm;
  integral_wrk.rho = 0.0; integral_wrk.rhoU = 0.0; integral_wrk.rhoV = 0.0; 
  integral_wrk.rhoE = 0.0; integral_wrk.rhoW = 0.0; 

  ierr = DMDAVecGetArrayRead(pde->dm, X, &xx);CHKERRQ(ierr);
  for (zi=zstart; zi<zend; zi++) {
    for (yi=ystart; yi<yend; yi++) {
      for (xi=xstart; xi<xend; xi++) {
        integral_wrk.rho  += xx[zi][yi][xi].rho; 
        integral_wrk.rhoU += xx[zi][yi][xi].rhoU; 
        integral_wrk.rhoV += xx[zi][yi][xi].rhoV;  
        integral_wrk.rhoE += xx[zi][yi][xi].rhoE;  
        integral_wrk.rhoW += xx[zi][yi][xi].rhoW; 
      }
    }
  }
  ierr = DMDAVecRestoreArrayRead(pde->dm, X, &xx);CHKERRQ(ierr);
  integral->rho = 0.0; integral->rhoU = 0.0; integral->rhoV = 0.0; 
  integral->rhoE = 0.0; integral->rhoW = 0.0; 
  ierr = MPI_Reduce(&integral_wrk,integral,5,MPIU_REAL,MPIU_SUM,0,pde->comm);CHKERRMPI(ierr); /* only compute on rank 0 */
  ierr = MPI_Comm_rank(pde->comm, &rank);CHKERRQ(ierr);
  if (rank != 0)  {
      integral->rho = NAN; integral->rhoU = NAN; integral->rhoV = NAN; 
      integral->rhoE = NAN; integral->rhoW = NAN; 
  } else { 
    integral->rho  *= pde->dx*pde->dy*pde->dz;
    integral->rhoU *= pde->dx*pde->dy*pde->dz;  
    integral->rhoW *= pde->dx*pde->dy*pde->dz;
    integral->rhoV *= pde->dx*pde->dy*pde->dz;
    integral->rhoE *= pde->dx*pde->dy*pde->dz; 
  }
  PetscFunctionReturn(0);
}