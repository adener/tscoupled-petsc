#include "fvns3d.h"

PetscErrorCode FormRHS(TS ts, PetscReal t, Vec QQ, Vec RHS, void* ctx)
{
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = FVNSComputeRHS((FVNS*)ctx, QQ, RHS);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

PetscErrorCode PostStep(TS ts)
{
  FVNS* pde;
  Vec QQ;
  PetscInt step;
  char buf[256];
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = TSGetApplicationContext(ts, &pde);CHKERRQ(ierr);
  ierr = TSGetSolution(ts, &QQ);CHKERRQ(ierr);
  ierr = TSGetStepNumber(ts, &step);CHKERRQ(ierr);
  ierr = PetscSNPrintf(buf, 256, "out/3D/%s", ProblemTypes[pde->defs->ptype]);CHKERRQ(ierr);
  ierr = FVNSWriteSolutionVTK(pde, QQ, buf, step);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

static PetscErrorCode TSMonitorGlvis(TS ts,PetscInt steps,PetscReal time,Vec u,void *mctx)
{
  PetscErrorCode ierr;
  PetscViewer    viewer = (PetscViewer)mctx;

  PetscFunctionBeginUser;
  ierr = VecView(u,viewer);CHKERRQ(ierr); 
  PetscFunctionReturn(0);
}


int main(int argc,char **argv)
{
  ProblemDefs *defs;
  FVNS *pde;
  Vec QQ, RHS;
  TS ts;
  PetscInt max_steps=20000, nxe=16, nye=16, nze=16;
  PetscReal dt=1e-3, max_time=5.0;
  PetscBool plot = PETSC_FALSE, flg,plot_glvis = PETSC_FALSE; 
  char buf[256];
  PetscErrorCode ierr;
  PetscViewer    viewer;
  
  PetscFunctionBeginUser;
  ierr = PetscInitialize(&argc,&argv,0,0); if (ierr) return ierr;
  ierr = PetscOptionsGetBool(NULL,NULL,"-plot",&plot,&flg);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(NULL,NULL,"-plot_glvis",&plot_glvis,&flg);CHKERRQ(ierr);


  /* create problem definitions and PDE solver */
  ierr = ProblemDefsCreate(NULL,NULL, PTYPE_TGV, NONE, &defs);CHKERRQ(ierr);
  ierr = FVNSCreate(PETSC_COMM_WORLD, defs, &pde);CHKERRQ(ierr);
  ierr = FVNSSetNumElements(pde,nxe,nye,nze);CHKERRQ(ierr);
  ierr = FVNSSetFromOptions(pde);CHKERRQ(ierr); 
  ierr = FVNSSetUp(pde);CHKERRQ(ierr);
  ierr = FVNSView(pde, PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);

  /* create and configure the time stepper */
  ierr = TSCreate(pde->comm, &ts);CHKERRQ(ierr);
  ierr = TSSetDM(ts, pde->dm);CHKERRQ(ierr);
  ierr = TSSetProblemType(ts, TS_LINEAR);CHKERRQ(ierr);
  ierr = TSSetType(ts, TSRK);CHKERRQ(ierr);
  ierr = TSSetTimeStep(ts, dt);CHKERRQ(ierr);
  ierr = TSSetMaxTime(ts, max_time);CHKERRQ(ierr);
  ierr = TSSetMaxSteps(ts, max_steps);CHKERRQ(ierr);
  ierr = TSSetExactFinalTime(ts, TS_EXACTFINALTIME_STEPOVER);CHKERRQ(ierr);
  ierr = TSSetFromOptions(ts);CHKERRQ(ierr);
  ierr = TSSetUp(ts);CHKERRQ(ierr);
  
  /* initialize and solve the problem */
  ierr = DMCreateGlobalVector(pde->dm, &RHS);CHKERRQ(ierr);
  ierr = TSSetRHSFunction(ts, RHS, FormRHS, (void*)pde);CHKERRQ(ierr);
  ierr = DMCreateGlobalVector(pde->dm, &QQ);CHKERRQ(ierr);
  ierr = FVNSComputeInitialCondition(pde, QQ);CHKERRQ(ierr);
  ierr = FVNSViewExnerPressureProfile(pde);CHKERRQ(ierr);

  if (plot) {
    ierr = PetscSNPrintf(buf, 256, "out/3D/%s", ProblemTypes[defs->ptype]);CHKERRQ(ierr);
    ierr = FVNSWriteSolutionVTK(pde, QQ, buf, 0);CHKERRQ(ierr);
    ierr = TSSetApplicationContext(ts, (void*)pde);CHKERRQ(ierr);
    ierr = TSSetPostStep(ts, PostStep);CHKERRQ(ierr);
  }
  if (plot_glvis) {
    ierr = TSSetApplicationContext(ts, (void*)pde);CHKERRQ(ierr);
    ierr = PetscViewerGLVisOpen(PETSC_COMM_WORLD,PETSC_VIEWER_GLVIS_SOCKET,"localhost",PETSC_DECIDE,&viewer);CHKERRQ(ierr);
    ierr = TSMonitorSet(ts,TSMonitorGlvis,(void*)viewer,NULL);CHKERRQ(ierr);
  }
  ierr = TSSolve(ts, QQ);CHKERRQ(ierr);
  
  /* clean up and exit */
  ierr = VecDestroy(&QQ);CHKERRQ(ierr);
  ierr = VecDestroy(&RHS);CHKERRQ(ierr);
  ierr = FVNSDestroy(&pde);CHKERRQ(ierr);
  if (plot_glvis) {
    ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  }
  ierr = TSDestroy(&ts);CHKERRQ(ierr);
  ierr = PetscFinalize();
  return ierr;
}
